﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tourist.ViewModels.PaymentViewModel
{
    public class UserVModel
    {
        private string bornDay;
        public UserVModel()
        {
            Name = "";
            Meli = "";
            BornDate = "";
            PhoneNumber = "";
            FatherName = "";
            Day = "";
            Month = "";
            Years = "";
        }
     
        [Required(ErrorMessage = "لطفا نام خود را وارد کنید")]
        public string Name { get; set; }
        [Required(ErrorMessage = "لطفا کدملی خود را وارد کنید")]
        [StringLength(10, ErrorMessage = "کدملی باید کمتر از 10 رقم باشد")]
        [Remote("CheckMeli", "Payment")]
        public string Meli { get; set; }
        [Required(ErrorMessage = "لطفا تاریخ تولد خود را وارد کنید")]
        public string BornDate { get
            {
                if(!string.IsNullOrEmpty(Day))
                {
                    bornDay = Years + "/" + Month + "/" + Day;
                }
                return bornDay;
            }
            set
            {
             
                    bornDay = value;
             

            } }
        [Required(ErrorMessage = "لطفا شماره تماس خود را وارد کنید")]
        [RegularExpression(@"(^09[0-9]{9}|۰۹[۰-۹]{9}$)", ErrorMessage = "فرمت وارد شده صحیح نمی باشد")]
        public string PhoneNumber { get; set; }
        
        [Required(ErrorMessage = "لطفا نام پدر خود را وارد کنید")]
        public string FatherName { get; set; }

        [Required(ErrorMessage ="لطفا روز تولد را انتخاب کنید")]
        public string Day { get; set; }
        [Required(ErrorMessage = "لطفا ماه تولد را انتخاب کنید")]
        public string Month { get; set; }
        [Required(ErrorMessage = "لطفا سال تولد را انتخاب کنید")]
        public string Years { get; set; }

        public List<string> ListDay { get; set; }



    }
}