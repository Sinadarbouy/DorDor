﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tourist.ViewModels.PaymentViewModel
{
    public class PurchaseVModel
    {
        private string description;
      //private int transferPrice;
        private string transfer;
        private int quantity;
        //private int placeprice;
   
        private int id;
        private int cost;
        private string destination;
        private string origin;
        private string enddate;
        private string startdate;
        private string time;
        private string timeend;
        private string exteraOption;
        private string place;
        private int discountId;
        private string codeDiscount;
        private string typeTour;
      
        public PurchaseVModel(int? quantity)
        {
            discountId = 0;
     
            if (!quantity.HasValue)
                quantity = 1;
            Users = new List<UserVModel>();
            for (int i = 0; i < quantity; i++)
                Users.Add(new UserVModel());

        }
        public List<UserVModel> Users { get; set; }
        public int TempId
        {
            get;set;
        }
        public string TypeTour
        {
            get
            {
                if(typeTour!=null)
                {
                    return typeTour;
                }
                return "";
            }
            set
            {
                if (value != null)
                {
                    typeTour = value;
                }else
                {
                    typeTour = "";
                }
            }
        }
        public int DiscountId
        {
            get
            {
                return discountId;
            }
            set
            {
            
                if(value>0)
                {
                    discountId = value;
                }
                else
                {
                    discountId = 0;
                }
            }
        }
        public string CodeDiscount
        {
            get
            {
                if(codeDiscount==null)
                {
                    return "";
                }
                return codeDiscount;

            }
            set
            {
                if(value!=null)
                {
                    codeDiscount = value;
                }else
                {
                    codeDiscount = "";
                }
            }
        }
        public string Description {
            get
            {
                return description;
            }
            set
            {
                if (value != null)
                    description = value;
                else
                    description = "";
                    
            }
        }
        //public int? TransferPrice {
        //    get
        //    {
        //        return transferPrice;
        //    }
        //    set
        //    {
        //        if (value != null)
        //        {
        //            transferPrice = value.Value;
        //        }
        //        else
        //            transferPrice = 0;
        //    }
        //}
        public string Transfer {
            get
            {
                return transfer;
            }
            set
            {
                if (value != null)
                {
                    transfer = value;
                }
                else
                    transfer="";
            }
        }
        public int? Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                if (value.HasValue)
                {
                    quantity = value.Value;
                }
                else
                    quantity=1;
            }
        }
        //public int? Placeprice {
        //    get
        //    {
        //        return placeprice;
        //    }
        //    set
        //    {
        //        if (value.HasValue)
        //        {
        //            placeprice = value.Value;
        //        }
        //        else
        //            placeprice = 0;
              
        //    }
        //}
        public int? Id
        {
            get
            {
                return id;
            }
            set
            {
                if (value.HasValue)
                {
                    id = value.Value;
                }
                else
                    throw new Exception("id is null");

            }
        }
        public int? Cost
        {
            get
            {
                return cost;
            }
            set
            {
                if (value.HasValue)
                {
                    cost = value.Value;
                }
                else
                    throw new Exception("cost is null");
            }
        }

        public string Destination {
            get
            {
                return destination;
            }
            set
            {
                if (value != null)
                {
                    destination = value;
                }
                else
                    destination = "";
            }
        }
        public string Origin {
            get
            {
                return origin;
            }
            set
            {
                if (value != null)
                {
                    origin = value;
                }
                else
                    origin = "";
            }
        }
        public string Enddate
        {
            get
            {
                return enddate;
            }
            set
            {
                if (value != null)
                {
                    enddate = value;
                }
                else
                    throw new Exception("فیلد زمان حرکت مقدار ندارد");

            }
        }
        public string Startdate {
            get
            {
                return startdate;
            }

            set
            {
                if (value != null)
                {
                    startdate = value;
                }
                else
                    throw new Exception("فیلد تاریخ حرکت مقدار ندارد");
            }
        }
        public string Time {
            get
            {
                return time;
            }
            set
            {
                if (value != null)
                {
                    time = value;
                }
                else
                    throw new Exception("فیلد ساعت حرکت مقدار ندارد");

            }
        }
        public string Timeend {
            get
            {
                return timeend;
            }
            set
            {
                if (value != null)
                {
                    timeend = value;
                }
                else
                    timeend = "";
            }
        }
        public string ExteraOption {
            get
            {
                return exteraOption;
            }
            set
            {
                if (value != null)
                {
                    exteraOption = value;
                }
                else
                    exteraOption = "";
            }
        }
        public string Place
        {
            get
            {
                return place;
            }
            set
            {
                if(value!=null)
                {
                    place = value;
                }
                else
                {
                    place = "";
                }
            }
        }

    }
}