﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Tourist.Routing
{
    public class SubdomainRoute: RouteBase
    {
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            if (httpContext.Request == null || httpContext.Request.Url == null)
            {
                return null;
            }
            var host = httpContext.Request.Url.Host;
            var index = host.IndexOf(".");
            var index2 = host.IndexOf('.', host.IndexOf('.') + 1);
            string[] segments = httpContext.Request.Url.PathAndQuery.TrimStart('/').Split('/');

            if (index < 0)
            {
                return null;
            }
           
            if (segments.Length > 0)
            {

                if (segments[0] != "" && segments[0].ToLower() != "home")
                {
                    return null;
                }
              
            }
            var subdomain = host.Substring(0, index);
            string[] blacklist = { "www", "tourips", "dor-dor", "mail" };

            if (subdomain.ToLower() != "www")
            {
                if (blacklist.Contains(subdomain.ToLower()))
                {
                    return null;
                }
            }
            else {
                var subdomain2 = host.Substring(index + 1, index2 - index - 1);
                if (blacklist.Contains(subdomain2.ToLower()))
                {
                    return null;
                }
            }
            string controller = (segments.Length > 0) ? segments[0] : "Home";
            string action = (segments.Length > 1) ? segments[1] : "Index";

            var routeData = new RouteData(this, new MvcRouteHandler());
            routeData.Values.Add("controller", "Agency"); //Goes to the relevant Controller  class
            routeData.Values.Add("action", "Index"); //Goes to the relevant action method on the specified Controller
            if (subdomain.ToLower() != "www")
            {
            routeData.Values.Add("name", subdomain); //pass subdomain as argument to action method
            }
            else {
                var subdomain2 = host.Substring(index + 1, index2 - index - 1);
                routeData.Values.Add("name", subdomain2); //pass subdomain as argument to action method
            }
            return routeData;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            //Implement your formating Url formating here
            return null;
        }
    }
}