﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tourist.CommonMethods
{
    public static class DoubleExtensions
    {
        public static string ReminderDigits(this Double number, int count)
        {
            return (number - Math.Truncate(number)).ToString().Substring(2, count);
        }
    }
}