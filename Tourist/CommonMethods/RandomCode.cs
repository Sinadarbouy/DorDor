﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tourist.CommonMethods
{
    public static class RandomCode
    {
        private static string[] letters = new string[27] { "A", "B", "C", "D", "E", "F", "G", "H", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        private static string[] numbers = new string[9] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        private static string[] lettersANDnumbers = new string[36] { "A", "1", "B", "C", "2", "D", "E", "9", "F", "G", "H", "H", "3" , "I", "J", "K", "L", "M", "7", "N", "O", "4", "P", "Q", "8", "R", "S", "T", "U", "5", "V", "W", "X", "Y", "6", "Z" };
        private static Random r = new Random();

        public static string make(int max,bool isletter) {

            if (isletter)
            {
                string[] randomLetters = new string[max];
                for (int i = 0; i < max; i++)
                {
                    randomLetters[i] = letters[r.Next(0, letters.Length - 1)];
                }
                return string.Join("", randomLetters);
            }
            else {
                string[] randomLettersNumbers = new string[max];
                for (int i = 0; i < max; i++)
                {
                    randomLettersNumbers[i] = lettersANDnumbers[r.Next(0, lettersANDnumbers.Length - 1)];
                }
                return string.Join("", randomLettersNumbers);
            }

        }
    }
}