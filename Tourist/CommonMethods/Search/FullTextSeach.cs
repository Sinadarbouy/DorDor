﻿using CommonMethod;
using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Tourist.Models;

namespace CommonMethod.Search
{
    public class FullTextSearch:IDisposable
    {
        DbTourist sourc;
        public FullTextSearch()
        {
          
                sourc = new DbTourist();
           
       
        }

      
        public List<Tour> Search(string type, string day, string searchTerm)
        {

            
            try
            {
                day = day.Trim();
                type = type.Trim();
                if (type == "چه نوع توری؟مثلا طبیعت گردی".Trim())
                    type = "طبیعت گردی";
                if (day == "چند روزه؟".Trim())
                    day = "0";


                List<Tour> listTour = new List<Tour>();
                if (string.IsNullOrEmpty(searchTerm))
                {
                    if (type == "0".Trim() && day.Trim() == "0".Trim())
                    {
                        listTour = sourc.Tours.ToList();

                    }

                    else if (day == "0".Trim())
                    {
                        listTour = sourc.Tours.Where(x => x.Type.Contains(type.Trim())).ToList();


                    }
                    else if (type == "0".Trim())
                    {
                        List<Tour> list = sourc.Tours.ToList();
                        listTour = list.Where(c => BoolDay(day, c)).ToList();
                      
                    }
                    else
                    {

                        List<Tour> list = sourc.Tours.Where(c => c.Type.Contains(type.Trim())).ToList();
                        listTour = list.Where(c => BoolDay(day, c)).ToList();
                    }
                }


                else
                {
                    if (type == "0".Trim() && day == "0".Trim())
                    {
                        listTour = FullSearch(searchTerm).ToList();

                    }
                    else if (type == "0".Trim())
                    {
                        List<Tour> list = FullSearch(searchTerm).ToList();
                        listTour=list.Where(c => BoolDay(day,c)).ToList();



                    }

                    else if (day == "0".Trim())
                    {
                        listTour = FullSearch(searchTerm, c => c.Type.Contains(type)).ToList();

                    }

                    else
                    {

                        List<Tour> list = FullSearch(searchTerm, c => c.Type.Contains(type)).ToList();
                        listTour=list.Where(c => BoolDay(day,c)).ToList();

                    }
                }
              //  Dispose();
                if (listTour == null)
                    listTour = new List<Tour>();
                return listTour.Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).ToList();
            }
            catch(Exception ex)
            {

                Dispose();
               string message= ex.Message;
                return new List<Tour>();
            }
           
           

        }
      
            /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private IQueryable<Tour> FullSearch(string searchTerm, Expression<Func<Tour,bool>>expression=null)
        {
            try
            {
              
                string sqlString = "SELECT * FROM [4618_sina].[Tour] where Freetext((Type,Title,Destination,Attractives,Meal_Description),@term)";
                IQueryable<Tour> list;
                if (expression!=null)
                    list = sourc.Tours.SqlQuery(sqlString,new SqlParameter("term",searchTerm)).AsQueryable().Where(expression);
                else
                    list= sourc.Tours.SqlQuery(sqlString, new SqlParameter("term", searchTerm)).AsQueryable();


                return list;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public bool BoolDay(string day,Tour tour)
        {
            DayCounts dateCount = new DayCounts(tour);
            float days = float.Parse(day);
            
            if (days <= 1.5f)
                return float.Parse(dateCount.GetDay()) == days;

                return float.Parse(dateCount.GetDay()) >= days;
        }
      
        public  void Dispose()
        {
            if(sourc!=null)
            {
                sourc.Dispose();
                sourc = null;
            }
           
            
        }

       

    }

}