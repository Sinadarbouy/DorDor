﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

using Tourist.Models;

namespace Models
{
    public class DayCounts
    {
        DateTime time1;
        DateTime time2;
        DateTime date1;
        DateTime date2;
        public DayCounts(Tour item)
        {
            try
            {
                  if (!string.IsNullOrWhiteSpace(item.Arrival_Time)&&item.Arrival_Time.Trim().Substring(0, 2) == "24")
               {
               item.Arrival_Time = "00:00";
               item.End_Date= item.End_Date.Value.AddDays(1);
               }
               
                if (!string.IsNullOrWhiteSpace(item.Start_time)&&item.Start_time.Trim().Substring(0, 2) == "24")
                {
                item.Start_time = "00:00";
                item.Start_Date= item.Start_Date.Value.AddDays(1);
                }
                  
            
         
            time1 = DateTime.ParseExact(Convert.ToDateTime(item.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
            time2 = DateTime.ParseExact(Convert.ToDateTime(item.Arrival_Time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);

            date1 = new DateTime(item.Start_Date.Value.Year, item.Start_Date.Value.Month, item.Start_Date.Value.Day, time1.Hour, time1.Minute, 00);
            date2 = new DateTime(item.End_Date.Value.Year, item.End_Date.Value.Month, item.End_Date.Value.Day, time2.Hour, time2.Minute, 00);
            
            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }
            
        }


        public string GetDay()
        {
            try
            {
                TimeSpan timeSpan = date2 - date1;

                int a = (int)timeSpan.TotalDays;

                Double b = timeSpan.TotalHours;
                string day;

                if (a == 0)
                    day = "1";

                else
                  if (b >= 27 && b <= 30)

                {
                    day = a.ToString() + "." + "5";
                }

                else
                {
                    day = a.ToString();
                }

                return day;
            }
          
               catch (Exception ex)
            {
                string message = ex.Message;
                return "0";
            }
        }
    }
}
