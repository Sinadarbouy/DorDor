﻿using Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Tourist.Models;

namespace Tourist.Controllers
{
    public class AgencyController : Controller
    {
        DbTourist DB = new DbTourist();
        // GET: Agency
       
        public ActionResult Index(string name)
        {
            try
            {
                //db.Tours.ToList().Where(x=>(x.Start_Date.Value.Date == DateTime.Now.Date)? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay: x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x=>x.Start_Date).Take(8)

                TempData["cm"] = name;
                     Tuple<List<Tour>, Profile_Tour, List<Tour>, List<Leader>> tuple = new Tuple<List<Tour>, Profile_Tour, List<Tour>, List<Leader>>(
                    DB.Tours.Where(x => x.Profile_Tour.Name_Group == name).ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).Take(8).ToList()
                    ,
                    DB.Profile_Tour.Where(x => x.Name_Group == name).FirstOrDefault(),
                    DB.Tours.Where(x => x.Start_Date < DateTime.Now && x.Profile_Tour.Name_Group == name).OrderBy(x => x.Start_Date).Take(4).ToList(),
                    DB.Leaders.Where(x => x.Profile_Tour.Name_Group == name).ToList()
                        );
                TempData["name"] = name;
                Session["stateuser"] = "1";
            if (!string.IsNullOrEmpty(name))
            {
                Session["NameAgency"] = name;
            }
            else
            {
                Session["NameAgency"] = "safhe agency";
            }
            return View(tuple);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult Archive(string name)
        {
            try
            {
                TempData["name"] = name;
                TempData["Tour_count"] = DB.Tours.Where(x => x.Start_Date >= DateTime.Now && x.Profile_Tour.Name_Group == name).Count().ToString();
                return View(DB.Tours.Where(x =>x.Profile_Tour.Name_Group == name && x.Start_Date < DateTime.Now).OrderByDescending(x=>x.Start_Date));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult Single_Tour(int Id)
        {
            try
            {
                Session["stateuser"] = "1";
                if (DB.Temps.Where(x => x.Tour_Id == Id).Count() != 0)
                {
                    TempData["temp"] = DB.Temps.Where(x => x.Tour_Id == Id).Sum(x => x.quantity);
                }
                else
                {
                    TempData["temp"] = 0;
                }
                TempData["name"] = DB.Tours.Find(Id).Profile_Tour.Name_Group;
                DateTime dateTimenow = DateTime.Now;
                //var lst = DB.Temps.Where(x => x.ExpireTime < dateTimenow).ToList().GroupBy(x => x.Tour_Id).Select(x=>new { quantity = x.Sum(y => y.quantity), x.Key});
                //foreach (var item in lst)
                //{
                //    DB.Tours.Find(item.Key).Remainder += item.quantity;
                //}
                //var lstexpire = DB.Temps.Where(x => x.ExpireTime < dateTimenow).ToList();
                //DB.Temps.RemoveRange(lstexpire);
                DB.SaveChanges();
                if (DB.Tours.Find(Id) != null)
                {
                   
                    return View(DB.Tours.Find(Id));
                }
                else
                {
                    return RedirectToAction("Error", "Home");
                }
            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }
           
        }

        public ActionResult All_Tour(string name)
        {
            TempData["name"] = name;
            TempData["ArchiveTour_count"] = DB.Tours.Where(x => x.Profile_Tour.Name_Group == name).Count().ToString();
            Session["stateuser"] = "1";
            return View(DB.Tours.Where(x => x.Profile_Tour.Name_Group == name).ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date));
        }

        public ActionResult Invoice(int id)
        {
            Payment_user _User = DB.Payment_user.Find(id);
            return View(DB.Payment_user.Where(x => x.groupid == _User.groupid).ToList());
        }

        public ActionResult Baby(int Id)
        {
            try
            {
                RepTemp repTemp = new RepTemp();
                repTemp.Check(Id);
                TempData["temp"] = repTemp.Quantity(Id);

                TempData["name"] = DB.Tours.Find(Id).Profile_Tour.Name_Group;

                Random random = new Random((int)(DateTime.Now.Ticks % 1000000));
                TempData["TempId"] = random.Next(100, int.MaxValue);

                //DateTime dateTimenow = DateTime.Now;
                //var lst = DB.Temps.Where(x => x.ExpireTime < dateTimenow).ToList().GroupBy(x => x.Tour_Id).Select(x => new { quantity = x.Sum(y => y.quantity), x.Key });
                //foreach (var item in lst)
                //{
                //    DB.Tours.Find(item.Key).Remainder += item.quantity;
                //}
                //var lstexpire = DB.Temps.Where(x => x.ExpireTime < dateTimenow).ToList();
                //DB.Temps.RemoveRange(lstexpire);
                //DB.SaveChanges();
                if (DB.Tours.Find(Id) != null)
                {
                    return View(DB.Tours.Find(Id));
                }
                else
                {
                    return RedirectToAction("Error", "Home");
                }
            }
            catch (Exception)
            {

                return RedirectToAction("Error", "Home");
            }
        }
        [WebMethod]
        public JsonResult ProfileMessage(string Name, string Email, string Phone, string Body, int profileID)
        {
            ProfileMessage profileMessage = new ProfileMessage() {
                Name = Name,
                Email = Email,
                Date = DateTime.Now,
                Profile_id = profileID,
                Message = Body,
                Tel = Phone
            };
            DB.ProfileMessages.Add(profileMessage);
            DB.SaveChanges();
            return Json("ok");
            //return RedirectToAction("index","Agency",new {name = NameAgency });
        }

    }
}