﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;
using Tourist.CommonMethods;

namespace Tourist.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        DbTourist db = new DbTourist();
        public ActionResult Add_profile_cafe_gallery()
        {
            ViewBag.profile = db.Profile_cafe_gallery.ToList();
            ViewBag.type = db.Types.ToList();
            ViewBag.kind_1 = new List<string> { "فیلم", "موسیقی", "ورزشی" };
            ViewBag.kind_2 = new List<string> { "گل", "کتاب", "نقاشی" };
            return View();
        }

        public ActionResult AllRequest()
        {
            
            return View(db.Requests);
        }
        public ActionResult Add_profile_Tour()
        {
            return View();
        }
        [ValidateInput(false)]
        public ActionResult AddProfileTourConfirm(Profile_Tour model)
        {
            db.Profile_Tour.Add(model);
            db.SaveChanges();
            return RedirectToAction("Add_profile_Tour", "profile");
        }
        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod()]
        [HttpGet]
        public string profilebyid(int type_id) {
            var p = db.Profile_cafe_gallery.Where(x => x.Type_id == type_id).Select(x => new { name = x.Name, id = x.id });
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(p);
            return json;
        }

       
        public ActionResult AddProfileConfirm(Profile_cafe_gallery model,cross_profile_classification model1, HttpPostedFileBase img1, HttpPostedFileBase img2,
            string type,string state_tagsinput)
        {
            byte[] image = null;
            if (img1 != null)
            {
                image = new byte[img1.ContentLength];
                img1.InputStream.Read(image, 0, image.Length);
            }

            byte[] logo = null;
            if (img2 != null)
            {

                logo = new byte[img2.ContentLength];
                img2.InputStream.Read(logo, 0, logo.Length);
            }
            if(type == "gallery")
            {
                model.Type_id = 2;
            }
            if (type == "cafe")
            {
                model.Type_id = 1;
            }
            string[] class_id = state_tagsinput.Split(',');
    
            model.Logo = logo;
            model.Image = image;
            db.Profile_cafe_gallery.Add(model);
            foreach (var item in class_id)
            {
                model1.classification_id = Convert.ToInt32(item);
                model1.profile_id = model.id;
                db.cross_profile_classification.Add(model1);
                db.SaveChanges();
            }
            db.SaveChanges();
            return RedirectToAction("Add_profile_cafe_gallery");
        }
    }
}