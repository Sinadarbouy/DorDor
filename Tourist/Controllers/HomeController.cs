﻿using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Tourist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using Tourist.ir;
using Fluentx.Mvc;
using Payments;
using Repositories;

namespace IdentitySample.Controllers
{

    public class HomeController : Controller
    {
        
        DbTourist db = new DbTourist();
        public ActionResult Indexghabl()
        {
            var rolemanager =
                HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            List<string> roles = new List<string> {
                "Admin","User","Purchaser","PreAdmin"
            };

            roles.ForEach(x =>
            {
                if (rolemanager.RoleExists(x) == false)
                {
                    IdentityRole role = new IdentityRole(x);
                    rolemanager.Create(role);
                }
            });
            //x=> x.Start_Date.Value.Date >= DateTime.Now.Date &&(x.Start_Date.Value.Date == DateTime.Now.Date)? DateTime.ParseExact(x.Start_time, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay:).Take(8).OrderBy(x=>x.Start_Date));
            TempData["compare"] = db.Tours.Where(x => x.Start_Date >= DateTime.Now).ToList();
            Session["stateuser"] = "0";
            return View(db.Tours.Where(x=>x.Profile_id != 80 && x.Profile_id != 43).ToList().Where(x=>(x.Start_Date.Value.Date == DateTime.Now.Date)? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay: x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x=>x.Start_Date).Take(8));
        }
        public ActionResult Index()
        {
            Session["stateuser"] = "0";
            Tuple<List<Tour>, List<Tour>, List<Profile_Tour>> tuple = new Tuple<List<Tour>, List<Tour>, List<Profile_Tour>>(
                db.Tours.Where(x => x.Profile_id != 80 && x.Profile_id != 43).ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).Take(8).ToList(),
                new List<Tour>(),
                db.Profile_Tour.Where(x => x.WorkWithUs == true).ToList()
            );
            return View(tuple);
        }

        //public void returnimage() {
        //    RepImage repImage = new RepImage(this);
        //    repImage.Transfer();
        //}



        public string Check(string option) {
            Analyze model = db.Analyzes.First();
            switch (option)
            {
                case "Submit_click":
                    model.Submit_click += 1;
                    db.SaveChanges();
                    return "Submit_click";
                case "compare_Click":
                    model.Compare_Click += 1;
                    db.SaveChanges();
                    return "compare_Click";
                case "Search_Click":
                    model.Search_Click += 1;
                    db.SaveChanges();
                    return "Search_Click";
                case "Type_Staying":
                    model.Type_Staying += 1;
                    db.SaveChanges();
                    return "Type_Staying";
                case "Type_Transport":
                    model.Type_Transport += 1;
                    db.SaveChanges();
                    return "Type_Transport";
                case "search_index_click":
                    model.search_index_click += 1;
                    db.SaveChanges();
                    return "search_index_click";
                case "compare_index_click":
                    model.compare_index_click += 1;
                    db.SaveChanges();
                    return "compare_index_click";
            }
            return "ok";
        }

        public ActionResult pay()
        {
            string resNum = CreateResnum().ToString();
            Int32 amount = 1000;
            Tourist.ir.shaparak.sep.PaymentIFBinding initPayment = new Tourist.ir.shaparak.sep.PaymentIFBinding();
            string token = initPayment.RequestToken("11221931", resNum, amount, 0, 0, 0, 0, 0, 0, "mamad", "", 0);
            if (!string.IsNullOrEmpty(token))
            {
                //save
                //info user and product and token and  unique resnumber(ReservationNumber)

                Dictionary<string, object> query = new Dictionary<string, object>();
                query.Add("Token", token);
                query.Add("RedirectURL", "http://" + Request.Url.Authority + "/Home/Verify");
                return this.RedirectAndPost("https://sep.shaparak.ir/payment.aspx", query);
            }

            return View();
        }
        
        private int CreateResnum()
        {
            return (int)(DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
        }
     

        public ActionResult Map()
        {
            return View(db.events);
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }

        public string price() {
        int tedad = db.Tours.Count();
        int one= db.Tours.Sum(x => x.Tourist_bus).Value;
        int one1 = db.Tours.Where(x => x.Tourist_bus != null).Count();
            double b = one / one1;
            return b.ToString();
        }
    }
}
