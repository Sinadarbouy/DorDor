﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using IdentitySample.Models;
using Microsoft.AspNet.Identity.Owin;
using Tourist.Models;
using Tourist.CommonMethods;
using System.Data.Entity.Validation;
using System.Net.Mail;
using System.Globalization;
using System.IO;
using System.Text;
using static Tourist.Controllers.AccountController;
using System.Data.Entity;
using Repositories;

namespace Tourist.Controllers
{
    public class DashboardController : Controller
    {
        DbTourist db = new DbTourist();
        // GET: Dashboard
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            Tuple<List<Tour>, List<Tour>, List<Payment_user>> tuple;
            int income;
            int incomlastweek;
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour modelpro = db.Profile_Tour.Where(x => x.User_id == userid).FirstOrDefault();
            if (user.UserName == "admin@admin.com")
            {
                List<Payment_user> payment_Users = db.Payment_user.Where(x => x.commit_pay == true).ToList();
                if (!payment_Users.Any())
                {
                    income = 0;
                }
                else
                {
                    income = payment_Users.Sum(x => x.pay);
                }
                DateTime lastweek = DateTime.Now.AddDays(-7);
                List<Payment_user> payment_Users_lastweek = payment_Users.Where(x => x.DateReq <= DateTime.Now && x.DateReq >= lastweek).ToList();
                if (!payment_Users.Any())
                {
                    incomlastweek = 0;
                }
                else
                {
                    incomlastweek = payment_Users_lastweek.Sum(x => x.pay);
                }
                TempData["LastWeekCount"] = incomlastweek.ToString("N0");
                TempData["AllIncome"] = income.ToString("N0");
                TempData["CustomerCount"] = payment_Users.Count();
                List<Tour> tours = db.Tours.ToList();
                TempData["TourCount"] = tours.Count;
                tuple = new Tuple<List<Tour>, List<Tour>, List<Payment_user>>(
                tours.Where(x => x.End_Date < DateTime.Now).OrderBy(x => x.Start_Date).Take(3).ToList()
                ,
                tours.Where(x => x.Start_Date >= DateTime.Now).OrderBy(x => x.Start_Date).Take(3).ToList()
                ,
                payment_Users.Where(x => (x.commit_pay == true || x.commit_pay == false)).OrderByDescending(x => x.DateReq).Take(5).ToList()
                );
                ViewBag.profilename =modelpro.Name_Group;
            }
            else
            {
                List<Payment_user> payment_User = db.Payment_user.Where(x => x.Tour.Profile_Tour.User_id == userid && x.commit_pay == true).ToList();
                if (!payment_User.Any())
                {
                    income = 0;
                }
                else
                {
                    income = payment_User.Sum(x => x.pay);
                }
                DateTime lastweek = DateTime.Now.AddDays(-7);
                //db.Payment_user.Where(x => x.Tour.Profile_Tour.User_id == userid && x.DateReq <= DateTime.Now && x.DateReq >= lastweek && x.commit_pay == true).Count() == 0
                List<Payment_user> payment_Users_lastweek = payment_User.Where(x=>x.DateReq <= DateTime.Now && x.DateReq >= lastweek).ToList();
                if (!payment_User.Any())
                {
                    incomlastweek = 0;
                }
                else
                {
                    incomlastweek = payment_Users_lastweek.Sum(x => x.pay);
                }
                TempData["LastWeekCount"] = incomlastweek.ToString("N0");
                TempData["AllIncome"] = income.ToString("N0");
                TempData["CustomerCount"] = payment_User.Count();
                List<Tour> tours_profile = db.Tours.Where(x => x.Profile_Tour.User_id == userid).ToList();
                TempData["TourCount"] = tours_profile.Count;
                tuple = new Tuple<List<Tour>, List<Tour>, List<Payment_user>>(
                    tours_profile.Where(x => x.End_Date < DateTime.Now).OrderBy(x => x.Start_Date).Take(3).ToList()
                   ,
                    tours_profile.Where(x=> x.Start_Date >= DateTime.Now).OrderBy(x => x.Start_Date).Take(3).ToList()
                   ,
                    payment_User.Where(x=> (x.commit_pay == true || x.commit_pay == false)).OrderByDescending(x => x.DateReq).Take(5).ToList()
                   );
                ViewBag.profilename = modelpro.Name_Group;
            }

            return View(tuple);
        }
        [Authorize(Roles = "Admin")]
        [ValidateInput(false)]
        public ActionResult AddTourConfirm(TourPlus model, string deadline, string deadlinetime,
            string state_tagsinput, int id_profile, List<string> Food_Name, List<int> Food_Price, List<string> Place_Name, List<int> Place_Price)
        {
            try
            {
                Repositories.RepImage repImage = new Repositories.RepImage(this, db);
                repImage.FileSave("~/Content/Images/", model.beforeimage, model.tour);

                if (Food_Name != null)
                {
                    for (int i = 0; i < Food_Name.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Food_Name[i]))
                        {
                            Food_tour food_Tour = new Food_tour();
                            food_Tour.Name = Food_Name[i];
                            food_Tour.Price = Food_Price[i];
                            model.tour.Food_tour.Add(food_Tour);
                        }

                    }
                }
                if (Place_Name != null)
                {
                    for (int i = 0; i < Place_Name.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Place_Name[i]))
                        {
                            Place_tour place_Tour = new Place_tour();
                            place_Tour.Name = Place_Name[i];
                            place_Tour.Price = Place_Price[i];
                            model.tour.Place_tour.Add(place_Tour);
                        }

                    }
                }
                TimeSpan timeSpan = DateTime.ParseExact(Convert.ToDateTime(deadlinetime).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay;
                DateTime dateTime = CalendarMngr.PersianToJulian(model.deadline_fa).Date;
                DateTime date = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                model.tour.deadline = date;
                model.tour.Start_Date = CalendarMngr.PersianToJulian(model.start_date_fa).Date;
                model.tour.End_Date = CalendarMngr.PersianToJulian(model.end_date_fa).Date;
                model.tour.Profile_id = id_profile;
                model.tour.Remainder = model.tour.Capacity;
                db.Tours.Add(model.tour);
                if (state_tagsinput != "")
                {
                    string[] class_id = state_tagsinput.Split(',');
                    foreach (var item in class_id)
                    {
                        Tour_Cross_Facilities model1 = new Tour_Cross_Facilities();
                        model1.Facilities_id = Convert.ToInt32(item);
                        model1.Tour_id = model.tour.Id;
                        db.Tour_Cross_Facilities.Add(model1);
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
                TempData["cm"] = "تور ثبت شد";
                TempData["msg"] = "تور ثبت شد";
            }
            catch (Exception ex)
            {
                TempData["cm"] = "تور ثبت نشد";
                TempData["msg"] = "تور ثبت نشد";
                return RedirectToAction("AddTour", "Dashboard", new
                {
                    model = model
                });
                throw;
            }
            return RedirectToAction("Index", "Dashboard");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AddTour(TourPlus model)
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            if (user.UserName == "admin@admin.com")
            {
                ViewBag.profile = db.Profile_Tour.ToList();
                List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
                ViewBag.profilename = modelpro[0].Name_Group;
                TempData["name"] = modelpro[0].Name_Group;
                TempData["cm"] = "اضافه کردن تور";
            }
            else
            {
                ViewBag.profile = db.Profile_Tour.Where(x => x.User_id == userid);
                List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
                ViewBag.profilename = modelpro[0].Name_Group;
                TempData["name"] = modelpro[0].Name_Group;
                TempData["cm"] = "اضافه کردن تور";
            }

            return View(model);
        }
        public ActionResult Balance()
        {
            int Available = 0;
            var userid = User.Identity.GetUserId();
            Profile_Tour modelpro = db.Profile_Tour.Where(x => x.User_id == userid).FirstOrDefault();
            ViewBag.profilename = modelpro.Name_Group;
            List<Payment_user> payment_Users = db.Payment_user.Where(x => x.Tour.Profile_Tour.User_id == userid && x.commit_pay == true).ToList();
            List<RequestBalance> lstrequestBalances = db.RequestBalances.Where(x => x.Profile_Tour.User_id == userid && x.Confirmation == 1).ToList();
            if (payment_Users.Any())
            {
                if (payment_Users.Sum(x => x.pay) == 0)
                {
                    Available = 0;
                }
                else
                {
                    int RequestBalances = 0;
                    TimeSpan time = new TimeSpan(7, 0, 0);
                    Available = payment_Users.Where(x=>x.DateReq.Value.Date.AddDays(1)+time < DateTime.Now).Sum(x => x.pay);
                    if (lstrequestBalances.Any()) {
                      RequestBalances = lstrequestBalances.Sum(x => x.Amount).Value;
                    }
                    //Available = db.Profile_Tour.Where(x => x.User_id == userid).ToList().First().Available_money.Value - RequestBalances;
                    Available = Available - RequestBalances;
                }   
            }
            if (modelpro.Available_money != null)
            {
                TempData["AllIncome"] = modelpro.Available_money.Value.ToString("N0");
            }
            else
            {
                TempData["AllIncome"] = 0.ToString();
            }
            if (!lstrequestBalances.Any())
            {
                TempData["received"] = 0.ToString();
            }
            else
            {
                TempData["received"] = lstrequestBalances.Sum(x => x.Amount).Value.ToString("N0");
            }
            TempData["Available"] = Available.ToString("N0");
            TempData["RequestCount"] =lstrequestBalances.Count();
            List<RequestBalance> allrequestBalances = db.RequestBalances.Where(x => x.Profile_Tour.User_id == userid).ToList();
            TempData["Request"] = allrequestBalances.OrderByDescending(x => x.Date).Take(15).ToList();
            TempData["cm"] = "12";
            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult BalanceConfirm(RequestBalance model)
        {
            try
            {
                int Available = 0;
                var userid = User.Identity.GetUserId();
                ViewBag.profile = db.Profile_Tour.Where(x => x.User_id == userid);
                List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
                ViewBag.profilename = modelpro[0].Name_Group;
                List<Payment_user> payment_Users = db.Payment_user.Where(x => x.Tour.Profile_Tour.User_id == userid && x.commit_pay == true).ToList();
                List<RequestBalance> lstrequestBalances = db.RequestBalances.Where(x => x.Profile_Tour.User_id == userid && x.Confirmation == 1).ToList();
                if (payment_Users.Any())
                {
                    if (payment_Users.Sum(x => x.pay) == 0)
                    {
                        Available = 0;
                    }
                    else
                    {
                        int RequestBalances = 0;
                        TimeSpan time = new TimeSpan(7, 0, 0);
                        Available = payment_Users.Where(x => x.DateReq.Value.Date.AddDays(1) + time < DateTime.Now).Sum(x => x.pay);
                        if (lstrequestBalances.Any())
                        {
                            RequestBalances = lstrequestBalances.Sum(x => x.Amount).Value;
                        }
                        //Available = db.Profile_Tour.Where(x => x.User_id == userid).ToList().First().Available_money.Value - RequestBalances;
                        Available = Available - RequestBalances;
                    }
                }
                if (Available < model.Amount)
                {
                    TempData["msg"] = "موجودی کافی نیست";
                    TempData["error"] = "موجودی کافی نیست";
                    return RedirectToAction("Balance", "Dashboard");
                }
                model.Decription = "درخواست شما ثبت و درحال بررسی است";
                model.Date = DateTime.Now;
                model.Confirmation = 3;
                model.Profile_id = db.Profile_Tour.Where(x => x.User_id == userid).ToList().First().ID;
                model.Available = db.Profile_Tour.Where(x => x.User_id == userid).ToList().First().Available_money;
                db.RequestBalances.Add(model);
                db.SaveChanges();
                TempData["cm"] = "درخواست ثبت شد";
                TempData["success"] = "درخواست شما ثبت و درحال بررسی است";
            }
            catch (Exception)
            {
                TempData["error"] = "درخواست ثبت نشد لطفا دوباره تلاش کنید";
                return RedirectToAction("Balance", "Dashboard");
            }

            return RedirectToAction("Balance", "Dashboard");
        }
        public ActionResult Test()
        {
            List<Payment_user> model = db.Payment_user.Where(x => x.commit_pay == true).ToList();
            //foreach (var item in model)
            //{
            //    if (item.Tour != null) {
            //        item.Profile_Id = item.Tour.Id;
            //    }
            //}

            
            return View(model);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult EditTour(int id)
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            if (user.UserName == "admin@admin.com")
            {
                ViewBag.profile = db.Profile_Tour.ToList();
            }
            else
            {
                ViewBag.profile = db.Profile_Tour.Where(x => x.User_id == userid);
            }
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            TempData["name"] = modelpro[0].Name_Group;
            TempData["cm"] = "ویرایش تور";
            //Tuple<Tour, TourPlus> tuple = new Tuple<Tour, TourPlus>(
            //    db.Tours.Find(id)
            //    ,
            //    new TourPlus()
            //    );
            TourPlus plus = new TourPlus { tour = db.Tours.Find(id) };
            return View(plus);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Link_Page()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid).OrderByDescending(x => x.Start_Date).ToList();

            }
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            TempData["start_date"] = "";
            TempData["end_date"] = "";
            return View(tours);
        }
        public ActionResult profiletour () {
            return View(db.Profile_Tour.ToList().OrderByDescending(x=>x.ID).Take(10));
        }
       

        public ActionResult RejectRequest(string Description, int idReject)
        {
            RequestBalance RejectRequest = db.RequestBalances.Find(idReject);
            RejectRequest.Decription = Description;
            RejectRequest.Confirmation = 0;
            db.SaveChanges();
            return RedirectToAction("ReportBalance");
        }
        public ActionResult finishRequest(int id)
        {
            RequestBalance finishtRequest = db.RequestBalances.Find(id);
            finishtRequest.Profile_Tour.Available_money = finishtRequest.Profile_Tour.Available_money - finishtRequest.Amount;
            finishtRequest.Decription = "پرداخت شد";
            finishtRequest.Confirmation = 1;
            db.SaveChanges();
            return RedirectToAction("ReportBalance");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ReportTour()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid).OrderByDescending(x => x.Start_Date).ToList();
            }
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            TempData["start_date"] = "";
            TempData["end_date"] = "";
            return View(tours);
        }
        public ActionResult Filter(string start_date, string end_date)
        {
            DateTime startdate = CalendarMngr.PersianToJulian(start_date).Date;
            DateTime enddate = CalendarMngr.PersianToJulian(end_date).Date;
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.Where(x => x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid && x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            var modelpro = db.Profile_Tour.Where(x => x.User_id == userid).FirstOrDefault();
            ViewBag.profilename = modelpro.Name_Group;
            TempData["start_date"] = start_date;
            TempData["end_date"] = end_date;
            return View("ReportTour", tours);
        }

        public string getpaymentuser(int id)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Payment_user.Where(x => x.Tour_id == id && x.commit_pay == true).Select(x => new { x.name, x.ssn,x.Father, x.Born_Date_fa, x.mobie, x.transfer, x.ExteraOption, x.pay, x.Description, x.refId, Type = x.Tour.Type }));
            return json;
        }
        public string gettype(int id)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Tours.Find(id).Type);
            return json;
        }
        public string getTour(string userid)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Tours.Where(x => x.Profile_Tour.User_id == userid).Select(x => new { x.Id, x.Title, x.Start_Date }));
            return json;
        }
        public string getTourbyid(int id)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Tours.Where(x => x.Id == id).Select(x => new { x.Capacity, x.Tourist_bus, x.Vip_bus, x.Agency_car, x.personal_car, x.Type, x.Remainder }));
            return json;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult EditTourAdmin()
        {

            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid).OrderByDescending(x => x.Start_Date).ToList();
            }
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            TempData["start_date"] = "";
            TempData["end_date"] = "";
            return View(tours);
        }

        public ActionResult EditTourConfirm(int id
            , TourPlus model, List<string> Food_Name,
            List<int> Food_Price, List<string> Place_Name,
            List<int> Place_Price

            , List<HttpPostedFileBase> img1
            , string state_tagsinput, string id_profile, string deadlinetime
            )
        {
            RepImage repImage = new RepImage(this, db);
            try
            {

                var item = db.Tours.First(x => x.Id == id);

                if (img1 != null)
                {
                    repImage.FileSave("~/Content/Images/", img1, item);
                    //foreach (HttpPostedFileBase postedFile in img1)
                    //{
                    //    byte[] image = null;
                    //    if (postedFile != null)
                    //    {
                    //        image = new byte[postedFile.ContentLength];
                    //        postedFile.InputStream.Read(image, 0, image.Length);
                    //        image img = new image();
                    //        img.image1 = image;
                    //        item.images.Add(img);
                    //    }
                    //}
                }
                if (Food_Name != null)
                {
                    for (int i = 0; i < Food_Name.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Food_Name[i]))
                        {
                            Food_tour food_Tour = new Food_tour();
                            food_Tour.Name = Food_Name[i];
                            food_Tour.Price = Food_Price[i];
                            item.Food_tour.Add(food_Tour);
                        }

                    }
                }
                if (Place_Name != null)
                {
                    for (int i = 0; i < Place_Name.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Place_Name[i]))
                        {
                            Place_tour place_Tour = new Place_tour();
                            place_Tour.Name = Place_Name[i];
                            place_Tour.Price = Place_Price[i];
                            item.Place_tour.Add(place_Tour);
                        }

                    }
                }
                item.Profile_id = int.Parse(id_profile);
                item.Type = model.tour.Type;
                item.Title = model.tour.Title;
                item.Origin = model.tour.Origin;
                item.Destination = model.tour.Destination;
                item.Start_Date = CalendarMngr.PersianToJulian(model.start_date_fa).Date;
                item.End_Date = CalendarMngr.PersianToJulian(model.end_date_fa).Date;
                item.Start_time = model.tour.Start_time;
                item.Arrival_Time = model.tour.Arrival_Time;
                item.Duration_Time = model.tour.Duration_Time;
                item.Place_Movement = model.tour.Place_Movement;
                item.Capacity = model.tour.Capacity;
                item.Age = model.tour.Age;
                item.Difficulty = model.tour.Difficulty;
                item.Tourist_bus = model.tour.Tourist_bus;
                item.Vip_bus = model.tour.Vip_bus;
                item.Agency_car = model.tour.Agency_car;
                item.personal_car = model.tour.personal_car;
                item.basic_place = model.tour.basic_place;//
                item.staying_place = model.tour.staying_place;
                item.staying_place2 = model.tour.staying_place2;
                item.staying_place3 = model.tour.staying_place3;//
                item.staying_price = model.tour.staying_price;
                item.staying_price2 = model.tour.staying_price2;
                item.staying_price3 = model.tour.staying_price3;//
                item.Breakfast = model.tour.Breakfast;
                item.Lunch = model.tour.Lunch;
                item.Dinner = model.tour.Dinner;
                item.Meal_Description = model.tour.Meal_Description;
                item.insuranceType = model.tour.insuranceType;
                item.facilityType = model.tour.facilityType;
                item.Travel_plan = model.tour.Travel_plan;
                item.Attractives = model.tour.Attractives;
                item.Afternoon = model.tour.Afternoon;
                item.Leader = model.tour.Leader;
                item.LeaderTel = model.tour.LeaderTel;

                item.needed_equipment = model.tour.needed_equipment;
                if (deadlinetime != null && deadlinetime != "" && model.deadline_fa != null && model.deadline_fa != "")
                {
                    TimeSpan timeSpan = DateTime.ParseExact(Convert.ToDateTime(deadlinetime).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay;
                    DateTime dateTime = CalendarMngr.PersianToJulian(model.deadline_fa).Date;
                    DateTime date = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                    item.deadline = date;
                }

                db.SaveChanges();
                if (state_tagsinput != "")
                {
                    string[] class_id = state_tagsinput.Split(',');
                    foreach (var i in class_id)
                    {
                        Tour_Cross_Facilities model1 = new Tour_Cross_Facilities();
                        model1.Facilities_id = Convert.ToInt32(i);
                        model1.Tour_id = id;
                        db.Tour_Cross_Facilities.Add(model1);
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
                TempData["cm"] = "تور ویرایش شد";
                TempData["msg"] = "تور ویرایش شد";

            }
            catch (Exception)
            {
                TempData["msg"] = "تور ویرایش نشد";
                TempData["cm"] = "تور ویرایش نشد";
                return RedirectToAction("Edittour", "Dashboard", new { id = model.tour.Id });

                throw;
            }


            return RedirectToAction("Index", "Dashboard");
        }
        public ActionResult FilterEdit(string start_date, string end_date)
        {
            DateTime startdate = CalendarMngr.PersianToJulian(start_date).Date;
            DateTime enddate = CalendarMngr.PersianToJulian(end_date).Date;
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.Where(x => x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid && x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            var modelpro = db.Profile_Tour.Where(x => x.User_id == userid).First();
            ViewBag.profilename = modelpro.Name_Group;
            TempData["start_date"] = start_date;
            TempData["end_date"] = end_date;
            return View("EditTourAdmin", tours);
        }
        public string FilterEditjson(string start_date, string end_date)
        {
            DateTime startdate = CalendarMngr.PersianToJulian(start_date).Date;
            DateTime enddate = CalendarMngr.PersianToJulian(end_date).Date;
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            List<Tour> tours;
            if (user.UserName == "admin@admin.com")
            {
                tours = db.Tours.Where(x => x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            else
            {
                tours = db.Tours.Where(x => x.Profile_Tour.User_id == userid && x.Start_Date >= startdate && x.Start_Date <= enddate).OrderByDescending(x => x.Start_Date).ToList();
            }
            TempData["start_date"] = start_date;
            TempData["end_date"] = end_date;
            return Newtonsoft.Json.JsonConvert.SerializeObject(tours.OrderBy(x => x.Start_Date).Select(x => new { x.Id, x.Title, date = CalendarMngr.JulianToPersian(x.Start_Date.Value) }));
        }

        public string DeleteTour(int id)
        {
            List<image> modelimages = db.images.Where(x => x.Tour_id == id).ToList();
            RepImage repImage = new RepImage(this,db);
            repImage.Delete(modelimages);
            List<Tour_Cross_Facilities> modelcross = db.Tour_Cross_Facilities.Where(x => x.Tour_id == id).ToList();
            db.Tour_Cross_Facilities.RemoveRange(modelcross);
            //List<Payment_user> lstPayment = db.Payment_user.Where(x => x.Tour_id == id).ToList();
            //db.Payment_user.RemoveRange(lstPayment);
            Tour removetour = db.Tours.Find(id);
            List<Temp> lsttemps = db.Temps.Where(x => x.Tour_Id == id).ToList();
            List<Food_tour> lstfood = db.Food_tour.Where(x => x.Tour_id == id).ToList();
            List<Place_tour> lstplace = db.Place_tour.Where(x => x.Tour_id == id).ToList();
            db.Food_tour.RemoveRange(lstfood);
            db.Place_tour.RemoveRange(lstplace);
            db.Temps.RemoveRange(lsttemps);
            db.Tours.Remove(removetour);
            db.SaveChanges();
            return Newtonsoft.Json.JsonConvert.SerializeObject("ook");
        }
        public string Deleteimage(int id)
        {
            RepImage repImage = new RepImage(this);
            repImage.Delete(id, true);
            return Newtonsoft.Json.JsonConvert.SerializeObject("ook");
        }
        public string GozareshDaramad()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            DateTime lastweek = DateTime.Now.AddDays(-7);
            List<Payment_user> m1;
            if (user.UserName == "admin@admin.com")
            {
                m1 = db.Payment_user.Where(x => x.DateReq <= DateTime.Now && x.DateReq >= lastweek && x.commit_pay == true).ToList().GroupBy(x => x.DateReq.Value.Date).Select(x => new Payment_user { name = CalendarMngr.JulianToPersian(x.First().DateReq.Value).ToString().Substring(5), pay = x.Sum(c => c.pay) }).ToList();
            }
            else
            {
                m1 = db.Payment_user.Where(x => x.DateReq <= DateTime.Now && x.DateReq >= lastweek && x.commit_pay == true && x.Tour.Profile_Tour.User_id == userid).ToList().GroupBy(x=>x.DateReq.Value.Date).Select(x => new Payment_user { name = CalendarMngr.JulianToPersian(x.First().DateReq.Value).ToString().Substring(5), pay = x.Sum(c => c.pay) }).ToList();
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(m1.Select(x => new { x.name, x.pay }));
            return json;
        }

        public ActionResult Message()
        {
            var userid = User.Identity.GetUserId();
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            return View(db.Tours.Where(x => x.Profile_Tour.User_id == userid).OrderByDescending(x => x.Start_Date).ToList());

        }
        [Authorize(Roles = "PreAdmin , Admin")]
        public ActionResult pricing()
        {
            try
            {
                var userid = User.Identity.GetUserId();
                var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
                ViewBag.profilename = modelpro[0].Name_Group;

                if (usermngr.IsInRole(userid, "Admin"))
                {
                    TempData["Role"] = "Admin";
                }
                else if (usermngr.IsInRole(userid, "PreAdmin"))
                {
                    TempData["Role"] = "PreAdmin";
                }

                return View();
            }
            catch (Exception)
            {

                return RedirectToAction("Login", "Account");
            }


        }


        public ActionResult Discount()
        {
            return View();
        }

        public string getUserCount(int id)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Payment_user.Where(x => x.Tour_id == id).Count());
            return json;
        }
        public ActionResult MessageConfirm(int id, string body, string subject)
        {
            try
            {
                foreach (var item in db.Payment_user.Where(x => x.Tour_id == id).ToList())
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(new MailAddress(item.email));
                    msg.From = new MailAddress("mail@dor-dor.ir");
                    msg.Subject = subject;
                    msg.Body = body;
                    msg.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient("mail.dor-dor.ir", 25);
                    smtp.Credentials = new System.Net.NetworkCredential("mail@dor-dor.ir", "Sina-1376");
                    //smtp.UseDefaultCredentials = true;
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.EnableSsl = false;
                    smtp.Send(msg);
                }
                TempData["msg"] = "پیام ها ارسال شد";
            }
            catch
            {
                TempData["msg"] = "پیام ها ارسال نشد";
                return RedirectToAction("Message", "Dashboard");
            }
            //var token2 = usermngr.GeneratePasswordResetToken(user.Id);
            //var token3 = usermngr.GeneratePasswordResetToken(user.Id);

            return RedirectToAction("Message", "Dashboard");
        }

        public ActionResult LittleEdit(int id, string Remainder, string capacity, string priceVip, string priceTourist, string pricePersonal, string PriceAgency)
        {
            try
            {
                foreach (var item in db.Tours.Where(x => x.Id == id))
                {

                    if (capacity != "" && capacity != null && capacity != "null")
                    {
                        item.Capacity = int.Parse(capacity);
                    }
                    if (Remainder != "" && Remainder != null && Remainder != "null")
                    {
                        item.Remainder = int.Parse(Remainder);
                    }
                    else
                    {
                        item.Remainder = null;
                    }
                    if (priceVip != "" && priceVip != null && priceVip != "null")
                    {
                        item.Vip_bus = int.Parse(priceVip);
                    }
                    else
                    {
                        item.Vip_bus = null;
                    }
                    if (priceTourist != "" && priceTourist != null && priceTourist != "null")
                    {
                        item.Tourist_bus = int.Parse(priceTourist);
                    }
                    else
                    {
                        item.Tourist_bus = null;
                    }
                    if (pricePersonal != "" && pricePersonal != null)
                    {
                        item.personal_car = int.Parse(pricePersonal);
                    }
                    else
                    {
                        item.personal_car = null;
                    }
                    if (PriceAgency != "" && PriceAgency != null)
                    {
                        item.Agency_car = int.Parse(PriceAgency);
                    }
                    else
                    {
                        item.Agency_car = null;
                    }
                }
                db.SaveChanges();
                TempData["cm"] = "تغییرات تور ایجاد شد";
                TempData["success"] = "تغییرات تور ایجاد شد";
            }
            catch (Exception)
            {
                TempData["error"] = "تغییرات ایجاد نشد دوباره امتحان کنید";
            }
            
            return RedirectToAction("EditTourAdmin", "Dashboard");
        }
        public ActionResult ReportBalance()
        {
            Tuple<List<RequestBalance>, List<RequestBalance>> tupleBalances = new Tuple<List<RequestBalance>, List<RequestBalance>>(
                db.RequestBalances.Where(x => x.Confirmation == 3).OrderByDescending(x => x.Date).ToList(),
                db.RequestBalances.Where(x => x.Confirmation != 3).OrderByDescending(x => x.Date).ToList()
            );
            return View(tupleBalances);
        }
        public ActionResult AddUser()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);

            if (user.UserName == "admin@admin.com")
            {
                TempData["tour"] = db.Tours.ToList();
            }
            else
            {
                TempData["tour"] = db.Tours.Where(x => x.Profile_Tour.User_id == userid).ToList();
            }
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;

            TempData["msg"] = "";
            return View();
        }

        public ActionResult AddUserConfirm(Payment_user model)
        {
            model.Born_Date = CalendarMngr.PersianToJulian(model.Born_Date_fa).Date;
            model.termpolicy = true;
            model.DateReq = DateTime.Now;
            model.commit_pay = true;
            model.Description = "تور" + model.Tour_id;
            db.Payment_user.Add(model);
            db.SaveChanges();
            TempData["msg"] = "مشتری اضافه شد";
            return RedirectToAction("Index", "Dashboard");
        }

        public string Check(string meli)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Payment_user.Count());
            return json;
        }
      
        public new ActionResult Profile()
        {
            return View();
        }
        public ActionResult CreateProfileConfirm(Profile_Tour model, List<string> Name_Member, List<string> Tel_Member, List<HttpPostedFileBase> Img_Member, HttpPostedFileBase logo_img, HttpPostedFileBase img, List<string> Title_Member, List<string> Description_Member)
        {
            int counter = 0;

            try
            {
                byte[] image1 = null;
                if (logo_img != null)
                {
                    image1 = new byte[logo_img.ContentLength];
                    logo_img.InputStream.Read(image1, 0, image1.Length);
                }
                byte[] image2 = null;
                if (img != null)
                {
                    image2 = new byte[img.ContentLength];
                    img.InputStream.Read(image2, 0, image2.Length);
                }
                model.Logo = image1;
                model.image = image2;
                db.Profile_Tour.Add(model);
                db.SaveChanges();
                foreach (HttpPostedFileBase postedFile in Img_Member)
                {

                    byte[] image = null;
                    if (postedFile != null)
                    {
                        image = new byte[postedFile.ContentLength];
                        postedFile.InputStream.Read(image, 0, image.Length);
                        Leader leader = new Leader();
                        leader.image = image;
                        leader.Tel = Tel_Member[counter];
                        leader.Description = Description_Member[counter];
                        leader.Title = Title_Member[counter];
                        leader.Name = Name_Member[counter];
                        leader.profile_id = model.ID;
                        db.Leaders.Add(leader);
                        db.SaveChanges();

                    }
                    counter++;
                }



                TempData["cm"] = "پروفایل ثبت شد";
            }
            catch (Exception)
            {
                TempData["cm"] = "پروفایل ثبت نشد";
                return RedirectToAction("CreateProfile", "Dashboard");

                throw;
            }

            return RedirectToAction("Index", "Dashboard");
        }
        public ActionResult EditProfile()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            ViewBag.profile = db.Profile_Tour.Where(x => x.User_id == userid);
            List<Profile_Tour> modelpro = db.Profile_Tour.Where(x => x.User_id == userid).ToList();
            ViewBag.profilename = modelpro[0].Name_Group;
            TempData["name"] = modelpro[0].Name_Group;
            TempData["cm"] = "ویرایش پروفایل";
            return View(modelpro[0]);
        }
        public ActionResult editProfileConfirm(Profile_Tour model, List<string> Name_Member, List<string> Tel_Member, List<HttpPostedFileBase> Img_Member, List<HttpPostedFileBase> logo_img, List<string> Title_Member, List<string> Description_Member, List<string> old_Name_Member, List<string> old_Tel_Member, List<HttpPostedFileBase> old_Img_Member)
        {
            int counter = 0;
            int counterLeader = 0;

            try
            {
                var userid = User.Identity.GetUserId();
                var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                Profile_Tour profile_Tourmodel = db.Profile_Tour.Where(x => x.User_id == userid).Single();
                byte[] image1 = null;
                if (logo_img[0] != null)
                {
                    image1 = new byte[logo_img[0].ContentLength];
                    logo_img[0].InputStream.Read(image1, 0, image1.Length);
                    profile_Tourmodel.Logo = image1;
                }


                foreach (var item in db.Leaders.Where(x => x.Profile_Tour.User_id == userid))
                {
                    item.Name = old_Name_Member[counterLeader];
                    item.Tel = old_Tel_Member[counterLeader];
                    if (old_Img_Member[counterLeader] != null)
                    {
                        byte[] image2 = null;
                        image2 = new byte[old_Img_Member[counterLeader].ContentLength];
                        old_Img_Member[counterLeader].InputStream.Read(image2, 0, image2.Length);
                        item.image = image2;
                    }
                    counterLeader++;
                }
                profile_Tourmodel.instagram = model.instagram;
                profile_Tourmodel.Manager = model.Manager;
                profile_Tourmodel.Map_lat = model.Map_lat;
                profile_Tourmodel.Map_len = model.Map_len;
                profile_Tourmodel.Name_Agency = model.Name_Agency;
                profile_Tourmodel.Name_Group = model.Name_Group.ToLower().Trim().Replace(" ","");
                profile_Tourmodel.Name_Leader = model.Name_Leader;
                profile_Tourmodel.Service = model.Service;
                profile_Tourmodel.Address = model.Address;
                profile_Tourmodel.Telegram = model.Telegram;
                profile_Tourmodel.Tel_Agency = model.Tel_Agency;
                profile_Tourmodel.Tel_Leader = model.Tel_Leader;
                profile_Tourmodel.WhatsApp = model.WhatsApp;
                profile_Tourmodel.Description = model.Description;

                db.SaveChanges();
                if (Img_Member != null) {
                    foreach (HttpPostedFileBase postedFile in Img_Member)
                    {

                        byte[] image = null;
                        if (postedFile != null)
                        {
                            image = new byte[postedFile.ContentLength];
                            postedFile.InputStream.Read(image, 0, image.Length);
                            Leader leader = new Leader();
                            leader.image = image;
                            leader.Tel = Tel_Member[counter];
                            leader.Description = Description_Member[counter];
                            leader.Title = Title_Member[counter];
                            leader.Name = Name_Member[counter];
                            leader.profile_id = profile_Tourmodel.ID;
                            db.Leaders.Add(leader);
                            db.SaveChanges();
                        }
                        counter++;
                    }
                }
                TempData["cm"] = "پروفایل تغییر کرد";
            }
            catch (Exception)
            {
                TempData["cm"] = "پروفایل تغییر نکرد";
                TempData["error"] = "متاسفانه پروفایل تغییر نکرد";
                return RedirectToAction("EditProfile", "Dashboard");
            }
            TempData["success"] = "پروفایل با موفقیت تغییر کرد";
            return RedirectToAction("EditProfile", "Dashboard");
        }

        public string deleteLeader(int id)
        {
            Leader leader = db.Leaders.Find(id);
            db.Leaders.Remove(leader);
            db.SaveChanges();
            return "ok";
        }

        [HttpPost]
        public ActionResult PdfReport(List<Payment_user> users, string title, string column1, string column2)
        {
            HTMLToPDF.PdfReport pdfReport = new HTMLToPDF.PdfReport();
            //pdfReport.DateHeader = DateTime.Now.ToString();
            pdfReport.Header = title;
            pdfReport.FontPath = "~/Models/Utilities/ReportToPDF/";
            pdfReport.Column1 = column1;// "اضافه";
            pdfReport.Column2 = column2;//"توضیحات";

            List<HTMLToPDF.Cells> cells = new List<HTMLToPDF.Cells>();
            if (users == null)
            {
                users = new List<Payment_user>();
                users.Add(new Payment_user());

            }
            if (title == null)
            {
                title = "preview";
            }
            for (int i = 0; i < users.Count; i++)
            {
                cells.Add(new HTMLToPDF.Cells()
                {
                    Name = users[i].name,
                    Id = (i + 1).ToString(),
                    NCode = users[i].ssn,
                    Amount = users[i].pay.ToString(),
                    DateGenerate = users[i].Born_Date_fa,
                    PhoneNumber = users[i].mobie,
                    Transition_no = users[i].refId,
                    Col2 = users[i].ExteraOption,
                    Col1 = users[i].transfer,
                    Description = users[i].Description,
                    FatherName = users[i].Father
                });
            }
            pdfReport.Rows = cells;


            byte[] ms = pdfReport.Generic();

            return Content(Convert.ToBase64String(ms));


        }


        public ActionResult ReportCode()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
            ViewBag.profilename = profile_Tour.Name_Group;
            TempData["ProfileID"] = profile_Tour.ID;
            return View();
        }


        public ActionResult MakeCode(Discount model)
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
            ViewBag.profilename = profile_Tour.Name_Group;
            TempData["ProfileID"] = profile_Tour.ID;
            return View(model);
        }
        public string gettourDiscount() {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            string json;
            if (user.UserName == "admin@admin.com")
            {
                 json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Tours.ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).Select(x => new { x.Id, x.Title, date = CalendarMngr.JulianToPersian(x.Start_Date.Value) }));
            }
            else {
                Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(profile_Tour.Tours.ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).Select(x => new { x.Id, x.Title, date = CalendarMngr.JulianToPersian(x.Start_Date.Value) }));
            }
          
            return json;
        }
        public string getalltour()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            string json;
            if (user.UserName == "admin@admin.com")
            {
                json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Tours.ToList().OrderBy(x => x.Start_Date).Select(x => new { x.Id, x.Title, date = CalendarMngr.JulianToPersian(x.Start_Date.Value) }));
            }
            else
            {
                Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
                json = Newtonsoft.Json.JsonConvert.SerializeObject(profile_Tour.Tours.ToList().OrderBy(x => x.Start_Date).Select(x => new { x.Id, x.Title, date = CalendarMngr.JulianToPersian(x.Start_Date.Value) }));
            }

            return json;
        }
        public string checkUrl(string url)
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
            var profileTours = db.Profile_Tour.ToList().Select(x => x.Name_Group);
            url = url.Replace(" ","");
            string json;
            if (profile_Tour.Name_Group.ToLower().Trim() != url.ToLower().Trim())
            {
                if (profileTours.Contains(url.Trim()))
                {
                    json = "1";
                }
                else
                {
                    json = "0";
                }
            }
            else
            {
                json = "0";
            }
            json = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return json;
        }
        [HttpPost]
        public ActionResult MakeCode(Discount model, string start_date, string end_date)
        {
            try
            {
                Profile_Tour profile_Tour = db.Profile_Tour.Find(model.ProfileID);
                ViewBag.profilename = profile_Tour.Name_Group;
                TempData["ProfileID"] = model.ProfileID;
                if (db.Discounts.Where(x => x.CodeDiscount.Trim().ToLower() == model.CodeDiscount.Trim().ToLower() && x.Exp >= DateTime.Now).Count() != 0)
                {
                    ModelState.AddModelError("CodeDiscount", "کد تخفیف وارد شده تکراری است");
                    TempData["error"] = "کد تخفیف واردشده تکراری است";
                    return View();
                }
                if (model.TourID == 0)
                {
                    model.IsProfile = true;
                }
                else {
                    model.IsProfile = false;
                }
                try
                {
                    model.Exp = CalendarMngr.PersianToJulian(end_date);
                    model.Start = CalendarMngr.PersianToJulian(start_date);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("start_date", "تاریخ حتما باید وارد شود");
                    return View();
                }
                
                model.Remainder = model.Quantity;
                db.Discounts.Add(model);
                db.SaveChanges();
                TempData["success"] = "کد تخفیف با موفقیف ثبت شد";
                return View();
            }
            catch (Exception)
            {
                TempData["error"] = "کد تخفیف ثبت نشد لطفا دوباره تلاش کنید";
                return View();
            }


        }
        public ActionResult MakeRandomCode()
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
            ViewBag.profilename = profile_Tour.Name_Group;
            TempData["ProfileID"] = profile_Tour.ID;
            return View();
        }
        [HttpPost]
        public ActionResult MakeRandomCode(Discount model, string start_date, string end_date, string length, string number ,bool Letters)
        {

            try
            {
                Profile_Tour profile_Tour = db.Profile_Tour.Find(model.ProfileID);
                ViewBag.profilename = profile_Tour.Name_Group;
                TempData["ProfileID"] = model.ProfileID;
                int _length;
                int _number;
                bool iserror = false;
                if (!int.TryParse(length, out _length))
                {
                    ModelState.AddModelError("length", "تعداد کد تخفیف باید عدد باشد");
                    iserror = true;
                }
                if (!int.TryParse(number, out _number))
                {
                    ModelState.AddModelError("number", "تعداد کد تخفیف باید عدد باشد");
                    iserror = true;
                    
                }
                try
                {
                    model.Exp = CalendarMngr.PersianToJulian(end_date);
                    model.Start = CalendarMngr.PersianToJulian(start_date);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("start_date", "تاریخ حتما باید وارد شود");
                    iserror = true;
                }
                if (iserror)
                {
                    return View();
                }
                else {
                    TempData["error"] = null;
                }
                if (model.TourID == 0)
                {
                    model.IsProfile = true;
                }
                else
                {
                    model.IsProfile = false;
                }

                for (int i = 0; i < _number; i++)
                {
                    model.CodeDiscount = RandomCode.make(_length, Letters);
                    model.Remainder = model.Quantity;
                    db.Discounts.Add(model);
                    db.SaveChanges();
                }
                
                TempData["success"] = "کد تخفیف با موفقیف ثبت شد";
                return View();
            }
            catch (Exception)
            {
                TempData["error"] = "کد تخفیف ثبت نشد لطفا دوباره تلاش کنید";
                return View();
            }


        }

        public ActionResult EditDiscount(int ID) {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
            TempData["ProfileID"] = profile_Tour.ID;
            ViewBag.tour = profile_Tour.Tours.ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date);
            Discount model = db.Discounts.Find(ID);
            ViewBag.profilename = profile_Tour.Name_Group;
            return View(model);
            
        }
        [HttpPost]
        public ActionResult EditDiscount(Discount model, string start_date, string end_date)
        {
            try
            {
                var userid = User.Identity.GetUserId();
                Profile_Tour profile_Tour = db.Profile_Tour.Where(x => x.User_id == userid).First();
                TempData["ProfileID"] = profile_Tour.ID;
                ViewBag.profilename = profile_Tour.Name_Group;
                model.Exp = CalendarMngr.PersianToJulian(end_date);
                model.Start = CalendarMngr.PersianToJulian(start_date);
                if (model.TourID == 0)
                {
                    model.IsProfile = true;
                }
                else
                {
                    model.IsProfile = false;
                }
                model.Remainder = model.Quantity;
                if (db.Discounts.Where(x => x.ID != model.ID && x.CodeDiscount == model.CodeDiscount.Trim().ToLower() && x.ProfileID == model.ProfileID).Count() != 0)
                {
                    ModelState.AddModelError("CodeDiscount", "کد تخفیف وارد شده تکراری است");
                    TempData["error"] = "کد تخفیف وارد شده تکراری است";
                    return View(model);
                }
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                TempData["success"] = "کد تخفیف با موفقیت ویرایش شد";
                return RedirectToAction("ReportCode");
            }
            catch (Exception)
            {
                TempData["error"] = "ویرایش کد تخفیف انجام نشد لطفا دوباره تلاش کنید";
                return View(model);
            }
            

        }
        [HttpPost]
        public JsonResult DeleteCode(int id)
        {
            Discount discount = db.Discounts.Find(id);
            db.Discounts.Remove(discount);
            db.SaveChanges();
            TempData["success"] = "کد تخفیف با موفقیت حذف شد";
            return Json("کد تخفیف با موفقیت حذف شد");
        }


        public string getDiscountcode(int id)
        {
            var userid = User.Identity.GetUserId();
            var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            var user = usermngr.FindById(userid);
            string json;
            if (user.UserName == "admin@admin.com")
            {
                if (id == 0)
                {
                   return json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Discounts.OrderByDescending(x=>x.Exp).ToList().Select(x => new { x.ID, x.CodeDiscount, x.Discountprice, x.Remainder, Exp = CalendarMngr.JulianToPersian(x.Exp.Value) }));
                }
                else
                {
                   return json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Discounts.Where(x =>x.TourID.Value == id).OrderByDescending(x => x.Exp).ToList().Select(x => new { x.ID, x.CodeDiscount, x.Discountprice, x.Remainder, Exp = CalendarMngr.JulianToPersian(x.Exp.Value) }));
                }
            }
            if (id == 0) {
                 json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Discounts.Where(x =>x.Profile_Tour.User_id == userid).OrderByDescending(x => x.Exp).ToList().Select(x => new { x.ID, x.CodeDiscount, x.Discountprice, x.Remainder, Exp = CalendarMngr.JulianToPersian(x.Exp.Value) }));
            }
            else
            {
             json = Newtonsoft.Json.JsonConvert.SerializeObject(db.Discounts.Where(x=> x.Profile_Tour.User_id == userid && x.TourID.Value == id).OrderByDescending(x => x.Exp).ToList().Select(x => new {x.ID, x.CodeDiscount, x.Discountprice, x.Remainder, Exp = CalendarMngr.JulianToPersian(x.Exp.Value) }));
            }
            return json;
        }

        public JsonResult DeleteFacility(int? id)
        {

            try
            {
                if (id.HasValue)
                {
                    Tour_Cross_Facilities tour_Cross_Facilities = db.Tour_Cross_Facilities.First(c => c.id == id);
                    if (tour_Cross_Facilities != null)
                    {
                        db.Tour_Cross_Facilities.Remove(tour_Cross_Facilities);
                        if (db.SaveChanges() > 0)
                            return Json("success");
                    }

                }
            }
            catch
            {

            }

            return Json("");
        }

        public JsonResult RemoveFood(int? id)
        {

            if (id.HasValue)
            {
                try
                {
                    Food_tour food_Tour = db.Food_tour.First(c => c.ID == id);
                    {
                        if (food_Tour != null)
                        {
                            db.Food_tour.Remove(food_Tour);
                            if (db.SaveChanges() > 0)
                            {
                                return Json("success");
                            }

                        }

                    }
                }
                catch
                {

                }



            }
            return Json("false");

        }
        public JsonResult RemovePalce(int? id)
        {
            if (id.HasValue)
            {
                try
                {
                    Place_tour place_Tour = db.Place_tour.First(c => c.ID == id);
                    if (place_Tour != null)
                    {
                        db.Place_tour.Remove(place_Tour);
                        if (db.SaveChanges() > 0)
                        {
                            return Json("success");
                        }
                    }
                }
                catch
                {

                }
            }
            return Json("false");

        }

    }
}
