﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;
using Tourist.CommonMethods;
using System.Globalization;
using System.Threading;
using System.Web.Services;
using Tourist.Utilities.Discount;
using Compress;

namespace Tourist.Controllers
{

    public class EventsController : Controller
    {
        private string _MerchantID = "0b18fd1a-49aa-11e8-b173-005056a205be";
        private string _urlpay = "https://www.zarinpal.com/pg/StartPay/";
        private string _CallbackURL = "http://tourips.com/payment/endp/";
        public delegate void PArgs(object sender, PayArgs e);
        public event PArgs OnPaymentAction;
        private int _Amount;
        private int id;
        // GET: Events
        DbTourist db = new DbTourist();
        public ActionResult AddEvent()
        {
            return View();
        }

        public ActionResult AddTour()
        {
            ViewBag.profile = db.Profile_Tour.ToList();
            return View();
        }


        public string cost(string tedad,string cost,string cost2)
        {
            int ghaymat = int.Parse(cost) + int.Parse(cost2);
            int ghaymat_tedad = ghaymat * int.Parse(tedad);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(ghaymat_tedad);
            return json;
        }
        public string costBaby(string tedad_baby,string tedad_hamrah, string cost,string babyprice,string Fellowprice)
        {
            if (tedad_baby == "") {
                tedad_baby = "0";
            }
            if (babyprice == "") {
                babyprice = "0";
            }
            if (tedad_hamrah == "")
            {
                babyprice = "0";
            }
            if (Fellowprice == "")
            {
                Fellowprice = "0";
            }
            int baby = int.Parse(tedad_baby) * int.Parse(babyprice);
            int Fellow = int.Parse(tedad_hamrah) * int.Parse(Fellowprice);
            int total = baby+Fellow + int.Parse(cost);
           
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(total);
            return json;
        }
        public string AddRequest(Request model, string name, string tel , int id,string tedad,int cost,string placeprice,string transferPrice)
        {
            model.Name = name;
            model.Tel = tel;
            model.Tour_id = id;
            model.quantity = int.Parse(tedad);
            model.cost = cost;
            model.placeprice = int.Parse(placeprice);
            model.transferPrice = int.Parse(transferPrice);
            model.date = DateTime.Now;
            db.Requests.Add(model);
            db.SaveChanges();
            var json = Newtonsoft.Json.JsonConvert.SerializeObject("succsess");
            return json;
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod()]
        [HttpGet]
        public string eventbykind(string kind_price)
        {
            var p = db.events.Where(x => x.price_kind == kind_price).Select(x => new { title = x.title, id = x.id , price_kind = x.price_kind , poster = x.poster });
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(p);
            
            return json;
        }
        [ValidateInput(false)]
        public ActionResult AddEventConfirm(@event model, HttpPostedFileBase img1,
            string start_date , string end_date  , string price_kind,
            int id_profile,string kind_event,string title,string ingenious,
            string description , string summary, int price , int reserved)
        {
            

            byte[] image = null;
            if (img1 != null)
            {
                image = new byte[img1.ContentLength];
                img1.InputStream.Read(image, 0, image.Length);
            }

            model.start_date = CalendarMngr.PersianToJulian(start_date).Date;
            model.end_date = CalendarMngr.PersianToJulian(end_date).Date;
            model.price_kind = price_kind;
            model.profile_id = id_profile;
            model.kind = kind_event;
            model.title = title;
            model.ingenious = ingenious;
           
            model.description = description;
            model.summary = summary;
            model.price = price;
            model.reserved = reserved;
            model.poster = image;

            db.events.Add(model);
            db.SaveChanges();
            return RedirectToAction("Add_profile_cafe_gallery","profile");
        }
        [HttpGet]
        public ActionResult All_Event(int kind)
        {
            ViewBag.countcafe = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1).ToList().Count();
            ViewBag.countgallery = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1).ToList().Count();
            ViewBag.kind = kind;
            return View(db.events.Where(x => x.Profile_cafe_gallery.Type_id == kind).ToList());
        }
        public ActionResult All_Event(string pricekind)
        {
            ViewBag.countcafe = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1).ToList().Count();
            ViewBag.countgallery = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1).ToList().Count();
            
            ViewBag.kind = 0 ;
            //return RedirectToAction("All_Event","Events", db.events.Where(x => x.price_kind == kind).ToList());
            return View(db.events.Where(x => x.price_kind == pricekind).ToList());

        }
        
        public ActionResult Single_Event(int Id)
        {
            return View(db.events.Find(Id));
        }
        public ActionResult Single_Tour(int Id)
        {
            if (db.Temps.Where(x => x.Tour_Id == Id).Count() != 0)
            {
                TempData["temp"] = db.Temps.Where(x => x.Tour_Id == Id).Sum(x => x.quantity);
            }
            else {
                TempData["temp"] = 0;
            }
            DateTime dateTimenow = DateTime.Now;
            //var lst = db.Temps.Where(x => x.ExpireTime < dateTimenow).ToList().GroupBy(x => x.Tour_Id).Select(x => new { quantity = x.Sum(y => y.quantity), x.Key });
            //foreach (var item in lst)
            //{
            //    db.Tours.Find(item.Key).Remainder += item.quantity;
            //}
            //var lstexpire = db.Temps.Where(x => x.ExpireTime < dateTimenow).ToList();
            //db.Temps.RemoveRange(lstexpire);
            db.SaveChanges();
            if (db.Tours.Find(Id) != null)
            {

                return View(db.Tours.Find(Id));
            }
            else {
                return RedirectToAction("Error","Home");
            }
        }
        public ActionResult Single_Tour2(int Id)
        {
            if (db.Temps.Where(x => x.Tour_Id == Id).Count() != 0)
            {
                TempData["temp"] = db.Temps.Where(x => x.Tour_Id == Id).Sum(x => x.quantity);
            }
            else
            {
                TempData["temp"] = 0;
            }
            DateTime dateTimenow = DateTime.Now;
            //var lst = db.Temps.Where(x => x.ExpireTime < dateTimenow).ToList().GroupBy(x => x.Tour_Id).Select(x => new { quantity = x.Sum(y => y.quantity), x.Key });
            //foreach (var item in lst)
            //{
            //    db.Tours.Find(item.Key).Remainder += item.quantity;
            //}
            //var lstexpire = db.Temps.Where(x => x.ExpireTime < dateTimenow).ToList();
            //db.Temps.RemoveRange(lstexpire);
            db.SaveChanges();
            if (db.Tours.Find(Id) != null)
            {

                return View(db.Tours.Find(Id));
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
        }
        [HttpPost]
        public ActionResult SearchTour(string type,string day,string what) {

            CommonMethod.Search.FullTextSearch fullSearch = new CommonMethod.Search.FullTextSearch();
            List<Tour> model = fullSearch.Search(type, day, what);

            return View("All_Tour",model);
        }
        public ActionResult SearchTourbytype(string type)
        {
            List<Tour> model = db.Tours.Where(x => x.Type == type).ToList().Where(x => (x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).ToList();
            return View("All_Tour", model);
        }
        public ActionResult Search(List<@event> model)
        {
            return View(model);
        }
        public List<@event> result;
        public ActionResult Search_Gallery(@event model, string category_kind, string end_date, string start_date, string category_price, int category_time)
        {
            DateTime startdate = CalendarMngr.PersianToJulian(start_date).Date;
            DateTime enddate = CalendarMngr.PersianToJulian(end_date).Date;
            if (category_kind == "0")
            {

                if (category_time == 1)
                {
                    if (category_price == "0")
                    {
                     result =  db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                    else
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                }
                else if (category_time == 2)
                {
                    if (category_price == "0")
                    {
                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                    else
                    {

                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                }
                else if (category_time == 0)
                {
                    if (category_price == "0")
                    {
                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                    else
                    {
                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                }
            }
            else {

                if (category_time == 1)
                {
                    if (category_price == "0")
                    {
                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                    else
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                }
                else if (category_time == 2)
                {
                    if (category_price == "0")
                    {
                         result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                    else
                    {

                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                }
                else if (category_time == 0)
                {
                    if (category_price == "0")
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                    else
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 2 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                }
            }
            return RedirectToAction("Search","Events", result);
        }

        public ActionResult Search_cafe(@event model, string category_kind, string end_date, string start_date, string category_price, int category_time)
        {
            DateTime startdate = CalendarMngr.PersianToJulian(start_date).Date;
            DateTime enddate = CalendarMngr.PersianToJulian(end_date).Date;
            if (category_kind == "0")
            {

                if (category_time == 1)
                {
                    if (category_price == "0")
                    {
                         result = db.events.Where(x =>x.Profile_cafe_gallery.Type_id == 1 && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                    else
                    {
                      result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                }
                else if (category_time == 2)
                {
                    if (category_price == "0")
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                    else
                    {

                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                }
                else if (category_time == 0)
                {
                    if (category_price == "0")
                    {
                      result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                    else
                    {
                      result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                }
            }
            else
            {

                if (category_time == 1)
                {
                    if (category_price == "0")
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                    else
                    {
                      result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours < 12).ToList();
                    }
                }
                else if (category_time == 2)
                {
                    if (category_price == "0")
                    {
                       result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                    else
                    {

                       result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date && x.time_end_mor.Value.Hours > 12).ToList();
                    }
                }
                else if (category_time == 0)
                {
                    if (category_price == "0")
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                    else
                    {
                        result = db.events.Where(x => x.Profile_cafe_gallery.Type_id == 1 && x.kind == category_kind && x.price_kind == category_price && startdate <= x.start_date && enddate <= x.end_date).ToList();
                    }
                }
            }




            return RedirectToAction("Search", "Events", result);
        }



        public List<Tour> lsttour;


        public ActionResult All_Tour(List<Tour> model)
        {
            //lsttour = db.Tours.Where(x=>x.Start_Date >= DateTime.Now).ToList();
            //TempData["Tour"] = lsttour;
            Session["stateuser"] = "0";
            if (model != null) {

            } else {
            
            model = db.Tours.Where(x=> x.Profile_id != 80 && x.Profile_id != 43).ToList().Where(x =>(x.Start_Date.Value.Date == DateTime.Now.Date) ? DateTime.ParseExact(Convert.ToDateTime(x.Start_time).TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay >= DateTime.Now.TimeOfDay : x.Start_Date.Value.Date >= DateTime.Now.Date).OrderBy(x => x.Start_Date).ToList();
            }
            return View(model);
        }
        
        public ActionResult Archive_tour()
        {
            ViewBag.ArchiveTour_count = db.Tours.ToList().Count();
            ViewBag.Tour_count = db.Tours.Where(x => x.Start_Date >= DateTime.Now).Count();
            //lsttour = db.Tours.(x=>x.Start_Date < DateTime.Now).ToList();
            //TempData["Tour"] = lstWheretour;
            //ViewBag.Tour_count = db.Tours.ToList().Count();
            return View(db.Tours.Where(x => x.Start_Date < DateTime.Now).OrderByDescending(x=>x.Start_Date));
        }



        public ActionResult Search_Tour(Tour model, string category, string end_date, string start_date,string destination,string city)
        {
            DateTime startdate = new DateTime();
            DateTime enddate = new DateTime();
            if (start_date != null && start_date != "")
            {
                startdate = CalendarMngr.PersianToJulian(start_date).Date;
            }
            else {
                startdate = DateTime.Now;
            }

            if (end_date != null && end_date != "")
            {
                enddate = CalendarMngr.PersianToJulian(end_date).Date;
            }
            else {
                enddate = DateTime.Now.AddYears(1);
            }

            if(category == "0"){
                if (destination == "") {
                    lsttour = db.Tours.Where(x => x.Start_Date >= startdate && x.End_Date <= enddate && x.Origin.Contains(city)).OrderBy(x=>x.Start_Date).ToList();
                    TempData["Tour"] = lsttour;
                }else{
                    lsttour = db.Tours.Where(x => x.Start_Date >= startdate && x.End_Date <= enddate && x.Origin.Contains(city) && ( x.Destination.Contains(destination) || x.Title.Contains(destination)) ).OrderBy(x => x.Start_Date).ToList();
                    TempData["Tour"] = lsttour;
                }
            }else{
                if (destination == "") {
                    lsttour = db.Tours.Where(x => x.Start_Date >= startdate && x.End_Date <= enddate && x.Type == category && x.Origin.Contains(city)).OrderBy(x => x.Start_Date).ToList();
                    TempData["Tour"] = lsttour;
                }else{
                    lsttour = db.Tours.Where(x => x.Start_Date >= startdate && x.End_Date <= enddate && x.Type == category && x.Origin.Contains(city) && (x.Destination.Contains(destination) || x.Title.Contains(destination))).OrderBy(x => x.Start_Date).ToList();
                    TempData["Tour"] = lsttour;
                }
            }
            return View("All_Tour");
        }

        public ActionResult All_Tourby(string orderby, List<Tour> model)
        {

            if (orderby == "price")
            {
                
                TempData["Tour"] = db.Tours.Where(x => x.Start_Date >= DateTime.Now).OrderBy(x => x.Tourist_bus).ToList();
            }
            else if (orderby == "capacity")
            {
                TempData["Tour"] = db.Tours.Where(x => x.Start_Date >= DateTime.Now).OrderBy(x => x.Capacity).ToList();
            }
            else if (orderby == "time")
            {
                TempData["Tour"] = db.Tours.Where(x => x.Start_Date >= DateTime.Now).OrderBy(x => x.Duration_Time).ToList();
            }
            else if (orderby == "date")
            {
                TempData["Tour"] = db.Tours.Where(x => x.Start_Date >= DateTime.Now).OrderBy(x => x.Start_Date).ToList();
            }
            return View("All_Tour");
        }


        public ActionResult compare_Tour()
        {
            return View(db.Tours.Where(x=>x.Start_Date >= DateTime.Now).ToList());
        }
        public ActionResult compare_Tours(string First_Tour,string Second_Tour)
        {
            try
            {
                int firsi_id = int.Parse(First_Tour);
                int second_id = int.Parse(Second_Tour);
                TempData["first"] = db.Tours.Where(x => x.Id == firsi_id).ToList();
                TempData["second"] = db.Tours.Where(x => x.Id == second_id).ToList();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
           


            return View("compare_Tour", db.Tours.Where(x => x.Start_Date >= DateTime.Now).ToList());
        }

        public ActionResult Laws()
        {
            
            return View();
        }


        [WebMethod]
        public JsonResult AddComment(Comment cm) {
            cm.confirm_cm = false;
            cm.datetime = DateTime.Now;
            db.Comments.Add(cm);
            db.SaveChanges();
            //if (cm.Tour_id == 178) {
            //    return RedirectToAction("MainPage", "Home");
            //}
            //return RedirectToAction("SingleTour", "Events", new { id = cm.Tour_id });
            return Json(cm.Body);
        }
        public ActionResult ConfirmComments(List<Comment> models) {
            if (models == null) {
                models = db.Comments.Where(x=>x.confirm_cm != true).ToList();
            }
            return View(models);
        }

        public ActionResult CheckCm(int id,string Leader, string Place, string Hashtag) {
            foreach (var item in db.Comments.Where(x=>x.ID == id))
            {
                if (Leader != null)
                {
                    item.Leader = true;
                }
                else {
                    item.Leader = false;
                }
                if (Place != null)
                {
                    item.Place = true;
                }
                else
                {
                    item.Place = false;
                }
                item.tag = Hashtag;
                item.confirm_cm = true;
            }
            db.SaveChanges();
            return RedirectToAction("ConfirmComments", "Events");
            
        }
        public ActionResult SingleTour(int id)
        {
            try
            {
                Tour TourModel = db.Tours.Find(id);

                if (TourModel.Type == "مادروکودک")
                {
                    return RedirectToAction("Baby", "Agency", new { id });
                }

                Repositories.RepTemp repTemp = new Repositories.RepTemp();

                repTemp.Check(id);

                TempData["temp"] = repTemp.Quantity(id);
                Random random = new Random((int)(DateTime.Now.Ticks % 1000000));
                TempData["TempId"] = random.Next(100, int.MaxValue);
                ViewBag.cm = db.Comments.Where(x => x.Tour_id == id && x.confirm_cm == true).OrderByDescending(x => x.datetime).ToList();
                ViewBag.tour = TourModel;

                if (TourModel != null)
                {
                    bool ModDiscount = true;

                    if (Session["stateuser"] != null)
                    {
                        if (Session["stateuser"] == "0")
                        {
                            TempData["Brand"] = "توریپس";
                            TempData["name"] = null;
                        }
                        if (Session["stateuser"] == "1")
                        {
                            TempData["Brand"] = TourModel.Profile_Tour.Name_Agency;
                            TempData["name"] = TourModel.Profile_Tour.Name_Group;
                            ModDiscount = false;
                        }
                    }
                    else
                    {
                        TempData["Brand"] = "توریپس";
                        TempData["name"] = null;

                    }
                    ManageDiscount manageDiscount = new ManageDiscount(db);
                    TempData["ProfileDiscount"] = manageDiscount.CheckDiscount(TourModel.Profile_id.Value, ModDiscount);
                    return View();
                }
                else
                {
                    return RedirectToAction("error", "home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("error", "home");
            }

            

        }



        [HttpPost]
        public ActionResult SingleTour(Payment_user payment_model)
        {

            Tour purchase_tour = db.Tours.Find(payment_model.Tour_id);
            Repositories.RepTemp repTemp = new Repositories.RepTemp();
            //repTemp.SetStep1(payment_model.Tour_id.Value, payment_model.quantity,Request.Url.ToString());

            if (purchase_tour.Remainder < payment_model.quantity)
            {
                TempData["color"] = "style=background-color:#e3c0c0";
                if (purchase_tour.Type == "مادروکودک")
                {
                    return RedirectToAction("Baby", "Agency", new { id = payment_model.Tour_id });
                }
                else
                {
                    return RedirectToAction("Singletour", "Events", new { id = payment_model.Tour_id });
                }
            }

            Code_meli code_Meli = new Code_meli();
            string ssn = CalendarMngr.ConvertNumbersToEnglish(payment_model.ssn);
            if (!code_Meli.chek(ssn))
            {
                ModelState.AddModelError("ssn", "کدملی اشتباه است");
                Session["stateuser"] = "0";
                if (db.Temps.Where(x => x.Tour_Id == payment_model.Tour_id).Count() != 0)
                {
                    TempData["temp"] = db.Temps.Where(x => x.Tour_Id == payment_model.Tour.Id).Sum(x => x.quantity);
                }
                else
                {
                    TempData["temp"] = 0;
                }
                /////////////////////////////////////////////////////////////////////////////temp

                repTemp.Check(payment_model.Tour_id.Value);


                ViewBag.cm = db.Comments.Where(x => x.Tour_id == payment_model.Tour_id && x.confirm_cm == true).OrderByDescending(x => x.datetime).ToList();
                ViewBag.tour = db.Tours.Find(payment_model.Tour_id);
                if (db.Tours.Find(payment_model.Tour_id) != null)
                {
                    if (Session["stateuser"].ToString() == "0")
                    {
                        TempData["Brand"] = "توریپس";
                        TempData["name"] = null;
                    }
                    if (Session["stateuser"].ToString() == "1")
                    {
                        TempData["Brand"] = db.Tours.Find(payment_model.Tour_id).Profile_Tour.Name_Agency;
                        TempData["name"] = db.Tours.Find(payment_model.Tour_id).Profile_Tour.Name_Group;
                    }
                }
                return View(payment_model);
            }

            //if (db.Temps.Where(x => x.pay == payment_model.pay && x.Step == 1 && x.quantity == payment_model.quantity).Count() == 0)
            //{
            //    if (purchase_tour.Remainder < payment_model.quantity)
            //    {
            //        TempData["color"] = "style=background-color:#e3c0c0";
            //        return RedirectToAction("Single_tour", "Events", new { id = payment_model.Tour_id});
            //    }
            //}

            Guid g = Guid.NewGuid();
            payment_model.termpolicy = true;
            payment_model.Description = $"تور {payment_model.Tour_id}";
            payment_model.DateReq = DateTime.Now;
            payment_model.groupid = g;
            payment_model.Born_Date = CalendarMngr.PersianToJulian(payment_model.Born_Date_fa).Date;
            db.Payment_user.Add(payment_model);

            db.SaveChanges();


            //db.Temps.Where(x => x.pay == payment_model.pay && x.Step == 1 && x.quantity == payment_model.quantity).OrderByDescending(x => x.datetime).First().pay_user_id = payment_model.Id;
            //db.SaveChanges();

            int id2 = payment_model.Id + 7;
            Guid guid = payment_model.groupid.Value;
            try
            {
                Zarinpal.PaymentGatewayImplementationServicePortTypeClient request1 = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
                string autohority = string.Empty;

                int Status = request1.PaymentRequest(_MerchantID, payment_model.pay, payment_model.Description, payment_model.email, payment_model.mobie, _CallbackURL + "?id=" + id2 + "&&group=" + guid, out autohority);
                if (Status > 0)
                {
                    new Thread(() =>
                    {
                        CheckPaymentStatus(autohority);
                    }).Start();

                    return Redirect(_urlpay + autohority);
                }
                else
                {
                    return RedirectToAction("error", "Home");
                }

            }
            catch (Exception)
            {
                return View(payment_model);
            }

        }
        public string GetMoney(string Transfer, string Place, string food, int quantity, int id, bool? isMain, string codeDiscount)
        {
            Tour model = db.Tours.Find(id);
            int money = 0;
            //Transfer.Contains("وسیله نقلیه توریستی")
            if (Transfer.Contains("توریستی"))
            {
                money += model.Tourist_bus.Value;
            }
            else
            if (Transfer.Contains("اتوبوس وی ای پی"))
            {
                money += model.Vip_bus.Value;
            }
            else if (Transfer.Contains("ماشین آژانس"))
            {
                money += model.Agency_car.Value;
            }
            else if (Transfer.Contains("ماشین شخصی"))
            {
                money += model.personal_car.Value;
            }

            if (Place != "" && Place != "1")
            {
                money += model.Place_tour.Where(x => x.Name.Replace("+", " ") == Place && x.Tour_id == id).Single().Price.Value;
            }
            if (food != "" && food != "1")
            {
                money += model.Food_tour.Where(x => x.Name.Replace("+", " ") == food && x.Tour_id == id).Single().Price.Value;
            }
            if (isMain.HasValue)
            {
                //money = CheckDiscount(money, model.Profile_id.Value, isMain.Value);
            }
            float ototal = quantity * money;

            ManageDiscount manageDiscount = new ManageDiscount(db, isMain);

            ototal = manageDiscount.CalcDiscount(ototal, codeDiscount, quantity, model);

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { per = ototal / quantity, total = ototal });
        }
        public string GetMoneyAgency(int momAndBaby, int extraBaby, int extraMom, bool? isMain, Tour tour)
        {
            int total = 0;
            if (momAndBaby > 0)
            {
                if (tour.Tourist_bus.HasValue)
                    total += (momAndBaby * tour.Tourist_bus.Value);
                else
                    throw new Exception("Is null momand and baby in database Tourist_buss");
            }
            if (extraBaby > 0)
            {
                if (tour.Agency_car.HasValue)
                    total += (extraBaby * tour.Agency_car.Value);
                else
                    throw new Exception("is null extra babay Ajency car");

            }
            if (extraMom > 0)
            {
                if (tour.personal_car.HasValue)
                {
                    total += extraMom * tour.personal_car.Value;
                }
                else
                {
                    throw new Exception();
                }
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { total = total });
        }
        //چک کردن اینکه تور تخفیف دارد یا نه
        public int CheckDiscount(int money, int profileID, bool isMain)
        {
            ProfileDiscount profileDiscount = db.ProfileDiscounts.Where(x => x.Profile_ID == profileID).FirstOrDefault();//پیدا کردن پروفایل
            if (isMain)//تخفیف برای صفحه اصلی
            {
                if (profileDiscount.Exp_main == null)//تاریخ انقضا دارد تخفیف یا نه
                {
                    if (profileDiscount.Exp_main >= DateTime.Now)//از زمان تخفیف نگذشته یاشد
                    {
                        return money;//اگر گذشته باشد تخفیف اعمال نشود
                    }
                }
                else
                {
                    return money = money * profileDiscount.Discount_main.Value / 100; //تخفیف اعمال شود
                }
            }//تخفیف برای صفحه آژانس
            else
            {
                if (profileDiscount.Exp_Agency != null)//تاریخ انقضا دارد تخفیف یا نه
                {
                    if (profileDiscount.Exp_Agency >= DateTime.Now)//از زمان تخفیف نگذشته یاشد
                    {
                        return money;//اگر گذشته باشد تخفیف اعمال نشود
                    }
                }
                else
                {
                    return money = money * (100 - profileDiscount.Discount_Agency.Value) / 100; ; //تخفیف اعمال شود
                }
            }
            return money;//تخفیف اعمال نشود
        }

        private void CheckPaymentStatus(string autohority)
        {

            Zarinpal.PaymentGatewayImplementationServicePortTypeClient request = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
            long refID = -1;
            bool stopit = false;
            long curtick = DateTime.Now.Ticks / TimeSpan.TicksPerSecond;
            while (true)
            {
                if (stopit)
                    break;
                int verf = -21;
                try
                {
                    verf = request.PaymentVerification(_MerchantID, autohority, _Amount, out refID);
                    db.Payment_user.Find(id).refId = request.PaymentVerification(_MerchantID, autohority, _Amount, out refID).ToString();
                }
                catch
                {

                }
                if (verf > 0)
                {
                    stopit = true;
                    if (OnPaymentAction != null)
                    {
                        OnPaymentAction(this, new PayArgs(verf, autohority, refID));
                    }
                }
                else
                {
                    if (!stopit && verf != -21)
                    {
                        stopit = true;
                        if (OnPaymentAction != null)
                        {
                            OnPaymentAction(this, new PayArgs(verf, autohority, refID));
                        }
                    }
                }
                long curtime = DateTime.Now.Ticks / TimeSpan.TicksPerSecond;
                if ((curtime - curtick) > 1850) // 30 * 60 +- 50
                {
                    if (!stopit)
                    {
                        OnPaymentAction(this, new PayArgs(-22, autohority, refID));
                        stopit = true;
                    }
                }
            }

        }

        public string URL
        {
            get { return _urlpay; }
        }

        public class PayArgs
        {
            private int _Status;
            private string _Autohority;
            private long _RefID;
            public PayArgs(int Status, string Autohority, long RefID)
            {
                _Status = Status;
                _Autohority = Autohority;
                _RefID = RefID;
            }
            public int Status
            {
                get { return _Status; }
            }
            public string Autohority
            {
                get { return _Autohority; }
            }
            public long RefID
            {
                get { return _RefID; }
            }
            private string GetFromLastSlash(string text)
            {
                int where = text.LastIndexOf('/');
                return text.Substring(where);
            }
        }



        public ActionResult AgencySingletour(int Id) {
            Session["stateuser"] = "1";


            Tour tour = db.Tours.Find(Id);
            if (!string.IsNullOrEmpty(tour.Profile_Tour.Name_Group))
            {
                Session["NameAgency"] = tour.Profile_Tour.Name_Group.ToString();
            }
            else {
                Session["NameAgency"] = "";
            }
           return RedirectToAction("singletour", "events", new { Id = Id });
        }
        public ActionResult touripSingletour(int Id)
        {
            Session["stateuser"] = "0";
            Session["NameAgency"] = "MA";
            return RedirectToAction("singletour", "events", new { Id = Id });
        }
    }
}