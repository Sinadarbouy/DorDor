﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;
using Tourist.CommonMethods;
using System.Globalization;
using System.Net;

using Fluentx.Mvc;
using Tourist.ViewModels.PaymentViewModel;
using Payments;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using IdentitySample.Models;
using Microsoft.AspNet.Identity.Owin;
using Tourist.Utilities.Filter;
using Repositories;
using Tourist.Utilities.Discount;
using Newtonsoft.Json;

namespace Tourist.Controllers
{
    public class PaymentController : Controller
    {
        private string _MerchantID = "0b18fd1a-49aa-11e8-b173-005056a205be";
        private string _urlpay = "https://www.zarinpal.com/pg/StartPay/";
        public delegate void PArgs(object sender, PayArgs e);
        public event PArgs OnPaymentAction;
        private int _Amount;
        private string _CallbackURL = "http://tourips.com/payment/endp/";
        private string _CallbackURLAgency = "http://tourips.com/payment/endp/";
        private int id;
        private enum package:int
        {
            simple = 2000000,
            VIP = 800
        }
        DbTourist db = new DbTourist();
        ManageDiscount manageDiscount = null;
        public PaymentController()
        {
            manageDiscount = new ManageDiscount(db);
        }


        // GET: Payment
        public ActionResult endp(int id, string Status, string Authority, Guid group)
        {
            RepTemp repTemp = new RepTemp();
            Payment_user model_pay;

            try
            {
                model_pay = db.Payment_user.Find(id - 7);
                if (!repTemp.CheckQuntity(model_pay))
                {
                    TempData["error"] = "ظرفیت به اتمام رسید خطای 20 دقیقه";
                    return RedirectToAction("error", "Home");
                }


                if (model_pay.State == null)
                {
                    if (Session["stateuser"] != null)
                    {
                        if (Session["stateuser"].ToString() == "0")
                        {
                            TempData["Brand"] = "توریپس";
                            TempData["name"] = null;
                        }
                        if (Session["stateuser"].ToString() == "1")
                        {
                            TempData["Brand"] = model_pay.Tour.Profile_Tour.Name_Agency;
                            TempData["name"] = model_pay.Tour.Profile_Tour.Name_Group;
                        }
                    }
                    else
                    {
                        TempData["Brand"] = "توریپس";
                        TempData["name"] = null;
                    }
                }
                else
                {
                    if (model_pay.State == "0")
                    {
                        TempData["Brand"] = "توریپس";
                        TempData["name"] = null;
                    }
                    if (model_pay.State == "1")
                    {
                        TempData["Brand"] = model_pay.Tour.Profile_Tour.Name_Agency;
                        TempData["name"] = model_pay.Tour.Profile_Tour.Name_Group;
                    }
                }
            }
            catch (Exception)
            {
                return RedirectToAction("error", "Home");
            }
            if (model_pay.Authority == null)
            {
                int countsms = 0;
                bool isError = true;
                foreach (var item in db.Payment_user.Where(x => x.groupid == group))
                {
                    item.refId = Authority.TrimStart('0');
                    item.Authority = Authority;
                    var a = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                    if (model_pay.State == null)
                    {
                        if (Session["stateuser"] != null)
                        {
                            if (Session["stateuser"].ToString() == "0")
                            {
                                item.State = "0";
                                model_pay.State = "0";
                            }
                            if (Session["stateuser"].ToString() == "1")
                            {
                                item.State = "1";
                                model_pay.State = "1";
                            }
                        }
                        else
                        {
                            TempData["Brand"] = "توریپس";
                            TempData["name"] = null;
                        }
                    }
                    else
                    {
                        if (model_pay.State == "0")
                        {
                            item.State = "0";
                        }
                        if (model_pay.State == "1")
                        {
                            item.State = "1";
                        }
                    }
                    if (Status.ToLower() == "nok")
                    {
                        //var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        //foreach (var item1 in lst)
                        //{

                        //    db.Tours.Find(item1.Tour_Id).Remainder += item1.quantity;
                        //}
                        //db.Temps.RemoveRange(lst);

                        item.commit_pay = false;
                        TempData["status"] = "ناموفق";

                    }
                    else if (Status.ToLower() == "ok")
                    {
                        //var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        //db.Temps.RemoveRange(lst);
                        item.commit_pay = true;
                        isError = false;
                        //model_pay.Tour.Remainder--;
                        if (item.Tour.Profile_Tour.Available_money != null)
                            item.Tour.Profile_Tour.Available_money += model_pay.pay;
                        else
                            item.Tour.Profile_Tour.Available_money = model_pay.pay;
                        TempData["status"] = "موفق";

                        //ersal sms
                        //SMS.Send sms = new SMS.Send();
                        //long[] rec = null;
                        //byte[] status = null;
                        //string[] Recipients = new string[] { item.mobie };
                        //string[] Recipientsagency = new string[] { item.Tour.LeaderTel };
                        //string siteagency = $"http://{item.Tour.Profile_Tour.Name_Group}.tourips.com";
                        //string site = "http://www.tourips.com";
                        //if(countsms == 0)//یک بار برای آزانس
                        //{
                        //string bodyagency = $"تور {item.Tour.Title} \n تاریخ حرکت:{CommonMethods.CalendarMngr.JulianToPersian(item.Tour.Start_Date.Value)} \n تعداد بلیط:{item.quantity} \n خریدار:{item.name} \n شماره تماس:09178273067 \n ظرفیت باقی مانده:{item.Tour.Remainder}";
                        //int retval = sms.SendSms("9174542538", "4398", Recipients, "20002222400065", bodyagency, false, "", ref rec, ref status);
                        //}
                        //End ersal sms

                        //ersal sms User

                        //string body = "";
                        //if (item.State == "1")
                        //{
                        //    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{item.quantity} \n مبلغ:{item.pay.ToString("N0")}تومان \n کدرهگیری:{item.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {site}";
                        //}
                        //else
                        //{
                        //    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{item.quantity} \n مبلغ:{item.pay.ToString("N0")}تومان \n کدرهگیری:{item.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {siteagency}";
                        //}
                        //int retval1 = sms.SendSms("9174542538", "4398", Recipientsagency, "20002222400065", body, false, "", ref rec, ref status);


                        //end ersal sms User

                    }
                    countsms++;//فقط یک sms برای اژانس فرستاده شود
                }
                repTemp.Commit(model_pay, isError);
            }
            else
            {
                if (model_pay.Authority == Authority && model_pay.groupid == group)
                {

                    if (model_pay.commit_pay == true)
                    {
                        TempData["status"] = "موفق";
                    }
                    else
                    {
                        TempData["status"] = "ناموفق";
                    }
                }
                else
                {
                    return RedirectToAction("error", "Home");
                }
            }
            db.SaveChanges();
            TempData["Authority"] = Authority;
            return View(model_pay);
        }
        [FilterLog]
        public ActionResult endp2(string State, string StateCode, string ResNum, string MID, string RefNum, string CID, string TRACENO, string RRN)
        {

            int countsms = 0;
            Payment_user model_pay;
            Payments.ModelPayment modelPayment = null;
            Payment payVerify = new Payment(this);
            Guid group;
            PaymentLog payLog;
            string[] Recipients;
            Profile_Tour profile_Tour;
            RepTemp repTemp = new RepTemp();

            /////////////////////////////////////////////////log
            try
            {
                payLog = db.PaymentLogs.Where(c => c.ResNUM == ResNum).First();
            }
            catch
            {
                payLog = new PaymentLog();
            }

            //////////////////////////////////////////////////////
            
            try
            {
                group = Guid.Parse(ResNum);
                List<Payment_user> payment_Users = db.Payment_user.Where(c => c.groupid == group).ToList();
                model_pay = payment_Users.First();
                Recipients = new string[1];
                profile_Tour = model_pay.Tour.Profile_Tour;
                //////////////////////////////////////////////////////////verify 
                if (!repTemp.CheckQuntity(model_pay))
                {
                    TempData["error"] = "ظرفیت به اتمام رسید خطای 20 دقیقه در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
                    return RedirectToAction("error", "Home");
                }
                if (manageDiscount.IsQuantity(model_pay))
                {
                    TempData["error"] = "ظرفیت کد تخفیف به اتمام رسید خطای 20 دقیقه در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";

                    return RedirectToAction("error", "Home");
                }
                modelPayment = payVerify.Verify(PaymentWith.Sep, MID, RefNum, ResNum, State, TRACENO, "");
                /////////////////////////////////////////////////////////////

                ///////log
                payLog.description += "isError : " + modelPayment.isError.ToString() + "\t";
                payLog.description += "Description : " + modelPayment.Description + "\r\n";
                payLog.description += "msgError: " + modelPayment.errorMsg + "\r\n";
                if (modelPayment.isError)
                    db.SaveChanges();
                ///////

                bool userState = DetectBrand(model_pay.State);
                if (userState)
                {
                    TempData["Brand"] = model_pay.Tour.Profile_Tour.Name_Agency;
                    TempData["name"] = model_pay.Tour.Profile_Tour.Name_Group;
                }
                else
                {
                    TempData["Brand"] = "توریپس";
                    TempData["name"] = null;
                }

                if (model_pay.Authority == null)
                {
                    repTemp.Commit(model_pay, modelPayment.isError);

                    if (modelPayment.isError)
                        TempData["status"] = modelPayment.Description;
                    else
                        TempData["status"] = "موفق";

                    ////////////////////////////////////////////////////////////Discount
                    manageDiscount.DiscountCommit(payment_Users.First(), modelPayment.isError);
                    int available_money = 0;
                    foreach (var item in payment_Users)
                    {

                        item.refId = TRACENO;//code paygiri
                        item.Authority = RefNum;// Auth user

                        if (item.State == null)
                            item.State = userState ? "1" : "0";
                        if (modelPayment.isError)
                        {
                            item.commit_pay = false;
                        }
                        else
                        {
                            item.commit_pay = true;
                            //model_pay.Tour.Remainder--;
                            available_money += model_pay.pay;//ezafe shodn pol be ajancy
                            if (countsms == 0) {
                            Recipients[countsms] = item.mobie.ConvertNumbersToEnglish();//shomare haye ersal sms
                            }
                            countsms++;
                        }
                    }
                    if (modelPayment.isError) {
                        return View(model_pay);
                    }
                    if (profile_Tour.Available_money == null)
                        profile_Tour.Available_money = available_money;
                    else
                        profile_Tour.Available_money += available_money;
                }
                else
                {
                    if (model_pay.Authority == RefNum && model_pay.groupid == group)
                    {
                        if (model_pay.commit_pay == true)
                        {
                            TempData["status"] = "موفق";
                            return View(model_pay);
                        }
                        else
                        {
                            TempData["status"] = "ناموفق";
                            return View(model_pay);
                        }
                    }
                    else
                    {
                        return RedirectToAction("error", "Home");
                    }
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ///////////////////////////log
                payLog.description += "\r\n" + ex.Message;
                db.SaveChanges();
                //////////////////////////

                return RedirectToAction("error", "Home");
            }
            try
            {
                SendSms(model_pay,Recipients);
            }
            catch (Exception ex)
            {
                ////////////////////////log
                payLog.description += "\r\n" + ex.Message;
                db.SaveChanges();
                //////////////////////////
            }

            TempData["Authority"] = RefNum;
            return View(model_pay);
        }
        private bool DetectBrand(string state)
        {
            var sessionState = Session["stateuser"];
            if (state != null)
                if (state == "1")
                    return true;

            if (sessionState != null)
                if (sessionState.ToString() == "1")
                    return true;
            return false;
        }
        private void SendSms(Payment_user model_pay, string[] Recipients)
        {

            //ersal sms
            SMS.Send sms = new SMS.Send();
            long[] rec = null;
            byte[] status = null;
            string siteagency = $"http://{model_pay.Tour.Profile_Tour.Name_Group}.tourips.com";
            string site = "http://www.tourips.com";
            if (model_pay.Tour.LeaderTel != null)
            {

                string[] Recipientsagency = new string[] { model_pay.Tour.LeaderTel.ConvertNumbersToEnglish() };

                string bodyagency = $"{model_pay.Tour.Title} \n تاریخ حرکت:{CommonMethods.CalendarMngr.JulianToPersian(model_pay.Tour.Start_Date.Value)} \n تعداد بلیط:{model_pay.quantity} \n خریدار:{model_pay.name} \n شماره تماس:{model_pay.mobie} \n ظرفیت باقی مانده:{model_pay.Tour.Remainder}";
                int retval = sms.SendSms("9174542538", "4398", Recipientsagency, "20002222400065", bodyagency, false, "", ref rec, ref status);

            }

            //End ersal sms agency

            //ersal sms User
            string body = "";
            if (model_pay.State == "0")
            {
                if (model_pay.Tour.Type == "مادروکودک")
                {
                    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{model_pay.quantity} \n مبلغ:{(model_pay.pay).ToString("N0")}تومان \n کدرهگیری:{model_pay.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {site}";
                }
                else
                {
                    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{model_pay.quantity} \n مبلغ:{(model_pay.pay * model_pay.quantity).Value.ToString("N0")}تومان \n کدرهگیری:{model_pay.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {site}";
                }
            }
            else
            {
                if (model_pay.Tour.Type == "مادروکودک")
                {
                    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{model_pay.quantity} \n مبلغ:{(model_pay.pay).ToString("N0")}تومان \n کدرهگیری:{model_pay.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {siteagency}";

                }
                else
                {
                    body = $"ثبت نام شما با موفقیت انجام شد \n تعداد بلیط:{model_pay.quantity} \n مبلغ:{(model_pay.pay * model_pay.quantity).Value.ToString("N0")}تومان \n کدرهگیری:{model_pay.refId} \n سفر خوب و هیجان انگیزی براتون ارزو میکنیم \n {siteagency}";
                }
            }
            SMS.Send sms2 = new SMS.Send();
            int retval1 = sms2.SendSms("9174542538", "4398", Recipients, "20002222400065", body, false, "", ref rec, ref status);
            //end ersal sms User



        }


        public string CheckDiscount(string discountCode, int? id)
        {
            try
            {
                
                return manageDiscount.CheckDiscount(discountCode, id);

            }
            catch
            {

            }

            return JsonConvert.SerializeObject(new { success = "false" });
        }

        [HttpPost]
        public ActionResult GetInfo([Bind(Include = "ExteraOption,id,quantity,transfer,state,place,codeDiscount,TempId")]
        string ExteraOption, int? id, int? quantity, string transfer, string state, string place, string codeDiscount, int? TempId)
        {

            if (!(id.HasValue || quantity.HasValue))
                return RedirectToAction("error", "Home");

            PurchaseVModel model = new PurchaseVModel(quantity);
            Repositories.RepTemp repTemp = new Repositories.RepTemp();
            Tour tour;
            try
            {
                tour = db.Tours.Find(id);

                string money = "";
                if (tour.Type != "مادروکودک")
                {
                    money = new EventsController().GetMoney(transfer, place, ExteraOption, quantity.Value, id.Value, Session["stateuser"] == "0" ? true : false, codeDiscount);
                    model.TypeTour = "nor";
                }
                else
                {

                    int exb;
                    int exm;
                    int.TryParse(transfer, out exb);
                    int.TryParse(place, out exm);

                    model.TypeTour = "mom";
                    money = new EventsController().GetMoneyAgency(1, exb, exm, Session["stateuser"] == "0" ? true : false, tour);
                }


                JToken token = JObject.Parse(money);
                int pay = (int)token.SelectToken("total");



                if (id != 0)
                {
                    if ((db.Tours.Find(id).Remainder - repTemp.Quantity(id.Value)) < quantity)
                    {
                        TempData["color"] = "style=background-color:#e3c0c0";

                        if (tour.Type == "مادروکودک")
                        {
                            return RedirectToAction("Baby", "Agency", new { id = id });
                        }
                        else
                        {
                            return RedirectToAction("Singletour", "Events", new { id = id });
                        }
                    }
                }
                tour = db.Tours.Find(id.Value);

                //------------------------------------------------------------------------manageDiscount set temp with null paymat_user
                manageDiscount.cleanTemp();
                model.DiscountId = manageDiscount.SetTemp(codeDiscount, quantity.Value, tour);
                //---------------------------------------------------------------------------------------------------------------

                ViewBag.tour = tour;
                model.Description = "ok";
                TempData["titile"] = tour.Title;
                model.Id = id;
                model.Cost = pay;
                model.Destination = tour.Destination;
                model.Origin = tour.Origin;
                model.Enddate = CalendarMngr.JulianToPersian(tour.End_Date.Value.Date);
                model.Startdate = CalendarMngr.JulianToPersian(tour.Start_Date.Value.Date);
                model.Time = tour.Start_time.ToString();
                model.Timeend = tour.Arrival_Time.ToString();
                model.Quantity = quantity;
                model.CodeDiscount = codeDiscount;
                //model.Placeprice = placeprice;
                //model.TransferPrice = transferPrice;
                model.Transfer = transfer;
                model.ExteraOption = ExteraOption;
                model.Place = place;

                if (Session["stateuser"] != null)
                {
                    if (Session["stateuser"] == "0")
                    {
                        TempData["Brand"] = "توریپس";
                        TempData["name"] = null;
                    }
                    if (Session["stateuser"] == "1")
                    {
                        TempData["Brand"] = tour.Profile_Tour.Name_Agency;
                        TempData["name"] = tour.Profile_Tour.Name_Group;
                    }
                }
                else
                {
                    TempData["Brand"] = "توریپس";
                    TempData["name"] = null;

                }


                //TempData["place"] = place;
                //----------------------------------------------------------------------------temp clean and set step1

                model.TempId = repTemp.SetStep1(tour.Id, quantity, TempId);
                //------------------------------------------------------------------------------

                db.SaveChanges();
                return View(model);
            }
            catch
            {
                return RedirectToAction("error", "Home");
            }

        }

       

        public JsonResult RevivalDiscount(int? id)
        {
            if (id == null)
                return Json("nan");
            try
            {

                TempDiscount tempDiscount = db.TempDiscounts.Where(c => c.id == id).First();
                tempDiscount.time_start = DateTime.Now;
                db.SaveChanges();
                return Json("ok");
            }
            catch
            {
                return Json("nan");
            }
        }
        [HttpPost]
        public ActionResult GetInfoed([Bind(Include = "Cost,ExteraOption,Quantity,termpolicy,Tour_id,Transfer,place,discountId,TempId,codeDiscount")]string termpolice
   , string ExteraOption, int? Quantity, int? Tour_id, string Transfer, List<UserVModel> Users, string place, string discountId, int? TempId, string codeDiscount)
        {

            Payment pay = new Payment(this);
            if (!(TempId.HasValue || Tour_id.HasValue || Quantity.HasValue))
                return RedirectToAction("error", "Home");
            Tour tour = db.Tours.Find(Tour_id);
            RepTemp repTemp = new RepTemp();



            ////////////////////////
            ////initial vriable
            List<Payment_user> payment_user = new List<Payment_user>();
            int discount_id = int.Parse(discountId);
            Guid groupId = Guid.NewGuid();

            string state = "0";
            string extra = "";
            string money = "";


            if (Session["stateuser"] != null)
            {
                if (Session["stateuser"].ToString() == "0")
                {
                    state = "0";
                }
                if (Session["stateuser"].ToString() == "1")
                {
                    state = "1";
                }
            }

            if (tour.Type != "مادروکودک")
            {
                money = new EventsController().GetMoney(Transfer, place, ExteraOption, Quantity.Value, tour.Id, state == "0" ? true : false, codeDiscount);
                if (!string.IsNullOrEmpty(ExteraOption))
                {
                    extra += $"-غذا={ExteraOption}-";
                }
                if (!string.IsNullOrEmpty(place))
                {
                    extra += $"-اقامت={place}-";
                }


            }
            else
            {

                int exb;
                int exm;
                int.TryParse(Transfer, out exb);
                int.TryParse(place, out exm);

                money = new EventsController().GetMoneyAgency(1, exb, exm, state == "0" ? true : false, tour);
                extra = ExteraOption;
                Transfer = tour.Tourist_bus.Value.ToString() + "مادر کودک";
            }

            JToken token = JObject.Parse(money);
            int cost = (int)token.SelectToken("total");

            int c = cost / Users.Count();
            foreach (var item in Users)
            {
                db.Payment_user.Add(new Payment_user
                {
                    name = item.Name,
                    mobie = item.PhoneNumber,
                    ssn = item.Meli,
                    Born_Date_fa = item.BornDate,
                    Father = item.FatherName,
                    transfer = Transfer,
                    quantity = Quantity,
                    ExteraOption = extra,
                    Discount_ID = discount_id,
                    Tour_id = tour.Id,
                    pay = c,
                    termpolicy = true,
                    Description = $"تور {Tour_id}",
                    DateReq = DateTime.Now,
                    State = state,
                    groupid = groupId,
                    Born_Date = CalendarMngr.PersianToJulian(item.BornDate).Date,
                    Profile_Id = tour.Profile_id
                });

            }

            if (!repTemp.Exist(TempId.Value))
            {
                if ((tour.Remainder - repTemp.Quantity(Tour_id.Value)) < Quantity)
                {
                    TempData["color"] = "style=background-color:#e3c0c0";
                    return RedirectToAction("SingleTour", "Events", new { id = Tour_id });
                }
            }
            try
            {
                db.SaveChanges();

                return pay.Request(PaymentWith.Sep, groupId, Quantity.Value, cost*10, TempId.Value);
            }
            catch (Exception ex)
            {
                if (ex.Message == "error Discount")
                    TempData["error"] = " خطا اتمام کد تخفیف؟؟یا تعدادی در حال استفاده از این کد تخفیف هستند بعد از دقایقی  دوباره تلاش کنید";
                else
                    TempData["error"] = " خطا در برقرای ارتباط با در گاه بانکی لطفا دوباره تلاش کنید ";

                return RedirectToAction("error", "Home");
            }



        }


        
        [HttpGet]
        public ActionResult Ticket(int id)
        {
            Payment_user user = db.Payment_user.Find(id);
            HTMLToPDF.PDFTicket ticket = new HTMLToPDF.PDFTicket();
            ticket.Path = "~/Models/Utilities/TicketToPDF/";
            ticket.NameUser = user.name;
            ticket.NumberTicket = user.quantity.ToString();
            string mobile="";
            if (user.Tour.LeaderTel != null && user.Tour.LeaderTel != "")
            {
                mobile = user.Tour.LeaderTel;
            }
            else if (user.Tour.Profile_Tour.mobile != null && user.Tour.Profile_Tour.mobile != "")
            {
                mobile = user.Tour.Profile_Tour.mobile;
            }
            else if (user.Tour.Profile_Tour.Tel_Leader != null && user.Tour.Profile_Tour.Tel_Leader != "") {
                mobile = user.Tour.Profile_Tour.Tel_Leader;
            }
            else
            {
                mobile = "09917458060";
            }

            ticket.PhoneNumber = mobile;

            if (user.Tour.Type == "مادروکودک")
            {
                ticket.Subject1 = "نوع تور";
                ticket.Subject2 = "همراهان";
            }
            else
            {
                ticket.Subject1 = "نوع اقامت";
                ticket.Subject2 = "نوع وسیله نقلیه";
            }


            ticket.Sub1 = user.ExteraOption;
            if (user.Tour.Profile_Tour.Name_Group != null && user.Tour.Profile_Tour.Name_Group != "")
            {
                ticket.STRLogo = user.Tour.Profile_Tour.Name_Group;
            }
            else
            {
                ticket.STRLogo = "توریپس";
            }
            string url = "";
            try
            {
                string[] splits = user.Temps.First().Url.Split('/');
                url = splits[2];
            }
            catch (Exception)
            {
                url = "TOURIPS.COM";
            }
            
            ticket.TimeStart = user.Tour.Start_time;
            ticket.TitleTour = user.Tour.Title;
            ticket.TourNo = user.Tour_id.ToString();
            ticket.TransactionNo = user.refId;
            ticket.Sub2 = user.transfer;
            ticket.DateBack = CalendarMngr.JulianToPersian(user.Tour.End_Date.Value).ToString();
            ticket.DateStart = CalendarMngr.JulianToPersian(user.Tour.Start_Date.Value).ToString();
            ticket.ToureType = user.Tour.Type;
            ticket.Brand = url;
            ticket.Message = "تور خوبی رو براتون آرزو میکنه! امیدواریم کلی بهتون خوش بگذره";

            byte[] ms = ticket.Generate();
            

            return Content(Convert.ToBase64String(ms));// File(ms, "application/pdf", $"{user.Tour.Title}.pdf");

        }
        public ActionResult EndPurchase(string Status, string Authority,int id, Guid group)
        {

            Payment_user model_pay = db.Payment_user.Find(id - 7);

            if (model_pay.Authority == null)
            {
                if (Status.ToLower() == "nok") {
                    db.Tours.Find(model_pay.Tour_id).Remainder += model_pay.quantity ;
                }
                foreach (var item in db.Payment_user.Where(x => x.groupid == group))
                {

                    item.refId = Authority.TrimStart('0');
                    item.Authority = Authority;
                    var a = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                    if (Status.ToLower() == "nok")
                    {
                        var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        foreach (var item1 in lst)
                        {

                            db.Tours.Find(item1.Tour_Id).Remainder += item1.quantity;
                        }
                        db.Temps.RemoveRange(lst);
                        item.commit_pay = false;
                        TempData["status"] = "ناموفق";
                    }
                    else if (Status.ToLower() == "ok")
                    {
                        var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        db.Temps.RemoveRange(lst);
                        item.commit_pay = true;
                        model_pay.Tour.Remainder--;
                        item.Tour.Profile_Tour.Available_money += model_pay.pay;
                        TempData["status"] = "موفق";
                    }

                }
            }
            else
            {
                if (model_pay.Authority == Authority && model_pay.groupid == group)
                {

                    if (model_pay.commit_pay == true)
                    {
                        TempData["status"] = "موفق";
                    }
                    else
                    {
                        TempData["status"] = "ناموفق";
                    }
                }
                else {
                    return RedirectToAction("error", "Home");
                }
            }
            db.SaveChanges();
            TempData["Authority"] = Authority;

            return View(model_pay);
        }
        
        public ActionResult EndPurchaseAgency(string Status, string Authority, int id, Guid group)
        {
            Payment_user model_pay = db.Payment_user.Find(id - 7);
            TempData["name"] = model_pay.Tour.Profile_Tour.Name_Group;
            if (model_pay.Authority == null)
            {
                foreach (var item in db.Payment_user.Where(x => x.groupid == group))
                {
                    item.refId = Authority.TrimStart('0');
                    item.Authority = Authority;
                    var a = db.Temps.Where(x=>x.pay_user_id == item.Id).ToList();
                    if (Status.ToLower() == "nok")
                    {
                        var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        foreach (var item1 in lst)
                        {
                            
                            db.Tours.Find(item1.Tour_Id).Remainder += item1.quantity;
                        }
                        db.Temps.RemoveRange(lst);
                        item.commit_pay = false;
                        TempData["status"] = "ناموفق";
                    }
                    else if (Status.ToLower() == "ok")
                    {
                        var lst = db.Temps.Where(x => x.pay_user_id == item.Id).ToList();
                        db.Temps.RemoveRange(lst);
                        item.commit_pay = true;
                        model_pay.Tour.Remainder--;
                        item.Tour.Profile_Tour.Available_money += model_pay.pay;
                        TempData["status"] = "موفق";
                    }
                }
            }
            else
            {
                if (model_pay.Authority == Authority && model_pay.groupid == group)
                {

                    if (model_pay.commit_pay == true)
                    {
                        TempData["status"] = "موفق";
                    }
                    else
                    {
                        TempData["status"] = "ناموفق";
                    }
                }
                else
                {
                    return RedirectToAction("error", "Home");
                }
            }
            db.SaveChanges();
            TempData["Authority"] = Authority;
            return View(model_pay);
        }

        [HttpPost]
        public ActionResult PurchaseMain(Payment_user model) {
            Tour purchase_tour = new Tour();
            if (model.Id != 0)
            {
            }
                return View();
        }



        [HttpPost]
        public ActionResult UpgradeProfile(int id)
        {
            try
            {
                Payment pay = new Payment(this);
                var userid = User.Identity.GetUserId();
                Profile_Tour profile = db.Profile_Tour.Where(x => x.User_id == userid).FirstOrDefault();
                switch (id)
                {
                    case 1:
                        return pay.RequestUpAgency(PaymentWith.Sep, (int)package.simple, $"{profile.ID}-{(int)package.simple}-simple");
                    case 2:
                        return pay.RequestUpAgency(PaymentWith.Sep, (int)package.VIP, $"{profile.ID}-{(int)package.simple}-VIP");
                    default:
                        {
                            return View();
                        }
                }
            }
            catch (Exception)
            {

                return RedirectToAction("Login", "Account");
            }
           
        }

        public ActionResult UpgradeProfileConfirm(string State, string ResNum, string TRACENO,string MID,string RefNum)
        {

            try
            {
                Payments.ModelPayment modelPayment = null;
                Payment payVerify = new Payment(this);
                string[] info =  ResNum.Split('-');
                if (State == "OK")
                {
                    var userid = User.Identity.GetUserId();
                    var usermngr = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                    var user = usermngr.FindById(userid);
                    switch (info[2])
                    {
                        case "simple":
                            usermngr.AddToRoles(userid, "Admin");
                            modelPayment = payVerify.Verify(PaymentWith.Sep, MID, RefNum, ResNum, State, TRACENO, "");
                            break;
                        default:
                            break;
                    }
                    
                    usermngr.Update(user);
                    
                    TempData["TRACENO"] = TRACENO;
                    TempData["State"] = State;
                    TempData["msg"] = "پرداخت با موفقیت انجام شد.لطفا دوباره وارد شوید.";
                    return RedirectToAction("Index", "Dashboard");
                }
                else {
                    TempData["State"] = "NOK";
                    return RedirectToAction("pricing", "Dashboard");
                }

            }
            catch (Exception)
            {

                return RedirectToAction("Login", "Account");
            }
            
 
        }

        [HttpPost]
        public ActionResult Purchase(string transfer, string ExteraOption, Payment_user model, List<Payment_user> model_user, String termpolicy)
        {
            Tour purchase_tour = new Tour();
            if (model.Id != 0) { 
            if (db.Tours.Find(model.Id).Remainder < model.quantity) {
                    TempData["color"] = "style=background-color:#e3c0c0";
                    purchase_tour = db.Tours.Find(model.Id);
                    if (purchase_tour.Type == "مادروکودک")
                    {
                        return RedirectToAction("Baby", "Agency", new { id = model.Id });
                    }
                    else
                    {
                        return RedirectToAction("Single_tour", "Events", new { id = model.Id });
                    }
                }
            }


            //_________________________________________________________________________________________________________________________
            int counter = 0;
                if (transfer == null) {
                Code_meli code_Meli = new Code_meli();
                Tour tour = db.Tours.Find(model.Tour_id);
                TempData["id"] = model.Tour_id;
                TempData["cost"] = model.pay;
                int pay = model.pay;
                int quantity = (int)model_user[0].quantity; 
                TempData["destination"] = tour.Destination;
                TempData["origin"] = tour.Origin;
                TempData["enddate"] = CalendarMngr.JulianToPersian(tour.End_Date.Value.Date);
                TempData["startdate"] = CalendarMngr.JulianToPersian(tour.Start_Date.Value.Date);
                TempData["time"] = tour.Start_time.ToString();
                TempData["timeend"] = tour.Arrival_Time.ToString();
                TempData["quantity"] = model_user[0].quantity;
                TempData["placeprice"] = model_user[0].placeprice;
                TempData["transferPrice"] = model_user[0].transferPrice;
                TempData["transfer"] = model_user[0].transfer;
                TempData["ExteraOption"] = model_user[0].ExteraOption;
                foreach (var item in model_user)
                {
                    string ssn = CalendarMngr.ConvertNumbersToEnglish(item.ssn);
                    if (!code_Meli.chek(ssn)) {
                        ModelState.AddModelError($"[{counter}].ssn", "کدملی اشتباه است");
                        return View();
                    }
                    counter++;
                }
                //if (db.Temps.Where(x => x.pay == pay && x.Step == 1 && x.quantity == quantity).Count() == 0) {
                //    if (db.Tours.Find(model.Id).Remainder < model.quantity)
                //    {
                //        TempData["color"] = "style=background-color:#e3c0c0";
                //        return RedirectToAction("Single_tour", "Events", new { id = model.Id });
                //    }
                //}
                int i = 0;
                Guid g = Guid.NewGuid();
                foreach (var item in model_user)
                {
                    if (Session["stateuser"] != null)
                    {
                        if (Session["stateuser"].ToString() == "0")
                        {
                            item.State = "0";
                        }
                        if (Session["stateuser"].ToString() == "1")
                        {
                            item.State = "1";
                        }
                    }
                    else
                    {
                        item.State = "0";
                    }
                    item.Tour_id = model.Tour_id;
                    if (db.Tours.Find(model.Tour_id).Type == "مادروکودک")
                    {
                        item.pay = model.pay;
                    }
                    else {
                        item.pay = model.pay/quantity;
                    }
                    item.termpolicy = true;
                    item.Description = $"تور {model.Tour_id} با 7%تخفیف";
                    item.DateReq = DateTime.Now;
                    if (Session["stateuser"] != null)
                    {
                        if (Session["stateuser"].ToString() == "0")
                        {
                            item.State = "0";
                        }
                        if (Session["stateuser"].ToString() == "1")
                        {
                            item.State = "1";
                        }
                    }
                    else
                    {
                        item.State = "0";
                    }
                    item.groupid = g;
                    item.Born_Date = CalendarMngr.PersianToJulian(item.Born_Date_fa).Date;
                    db.Payment_user.Add(item);
                    i++;
                }

                db.SaveChanges();

                id = model_user[0].Id;
                //db.Temps.Where(x => x.pay == pay && x.Step == 1 && x.quantity == quantity).OrderByDescending(x=>x.datetime).First().pay_user_id = id;
                db.SaveChanges();
                model.Description = "توریپس";
                if (model_user[0].Tour.Type == "مادروکودک")
                {
                    model.pay = model_user[0].pay;
                }
                else {
                    model.pay = model_user[0].pay*quantity;
                }
                int id2 = id + 7;
                Guid guid = model_user[0].groupid.Value;
                try
                {
                    Zarinpal.PaymentGatewayImplementationServicePortTypeClient request1 = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
                    string autohority = string.Empty;
                    int Status = request1.PaymentRequest(_MerchantID, model.pay, model.Description, model.email, model.mobie, _CallbackURL + "?id=" + id2 + "&&group=" + guid, out autohority);
                    if (Status > 0)
                    {
                        new Thread(() =>
                        {
                            CheckPaymentStatus(autohority);
                        }).Start();

                        return Redirect(_urlpay + autohority);
                    }
                    else
                    {
                        return RedirectToAction("error", "Home");
                    }

                }
                catch (Exception)
                {
                    return null;
                }


            }
                purchase_tour = db.Tours.Find(model.Id);
                model.Description = "ok";
                TempData["id"] = model.Id;
                TempData["cost"] = model.pay;
                TempData["destination"] = purchase_tour.Destination;
                TempData["origin"] = purchase_tour.Origin;
                TempData["enddate"] = CalendarMngr.JulianToPersian(purchase_tour.End_Date.Value.Date);
                TempData["startdate"] = CalendarMngr.JulianToPersian(purchase_tour.Start_Date.Value.Date);
                TempData["time"] = purchase_tour.Start_time.ToString();
                TempData["timeend"] = purchase_tour.Arrival_Time.ToString();
                TempData["quantity"] = model.quantity;
                TempData["placeprice"] = model.placeprice;
                TempData["transferPrice"] = model.transferPrice;
                TempData["transfer"] = transfer;
                TempData["ExteraOption"] = ExteraOption;

                 purchase_tour.Remainder = purchase_tour.Remainder - model.quantity;

                 Temp temp = new Temp();
                 temp.datetime = DateTime.Now;
                 //temp.ExpireTime = DateTime.Now.AddMinutes(10);   
                 temp.quantity = (int)model.quantity;
                 //temp.pay = model.pay;
                 temp.Step = 1;
                 temp.Tour_Id = model.Id;
                 db.Temps.Add(temp);
                 db.SaveChanges();

            return View(model_user);
        }


        public ActionResult Purchase2
            ([Bind(Include = "ExteraOption,id,pay,placeprice,quantity,transfer,transferPrice")]
        string ExteraOption, int? id, int? pay, int? placeprice, int? quantity, string transfer, int? transferPrice)
        {
            PurchaseVModel model = new PurchaseVModel(quantity);
            Tour tour;
            try
            {
                if (!(id.HasValue || pay.HasValue || quantity.HasValue))
                    return RedirectToAction("error", "Home");
                if (id != 0)
                {
                    if (db.Tours.Find(id).Remainder < quantity)
                    {
                        TempData["color"] = "style=background-color:#e3c0c0";
                        tour = db.Tours.Find(id);
                        if (tour.Type == "مادروکودک")
                        {
                            return RedirectToAction("Baby", "Agency", new { id = id });
                        }
                        else
                        {
                            return RedirectToAction("Single_tour", "Events", new { id = id });
                        }
                    }
                }
                string money = new EventsController().GetMoney(transfer, "", "", quantity.Value, id.Value, null,null);
                JToken token = JObject.Parse(money);
                int intmoney = (int)token.SelectToken("per");




                tour = db.Tours.Find(id.Value);
                model.Description = "ok";
                model.Id = id;
                //model.Cost = pay;
                model.Cost = intmoney;
                model.Destination = tour.Destination;
                model.Origin = tour.Origin;
                model.Enddate = CalendarMngr.JulianToPersian(tour.End_Date.Value.Date);
                model.Startdate = CalendarMngr.JulianToPersian(tour.Start_Date.Value.Date);
                model.Time = tour.Start_time.ToString();
                model.Timeend = tour.Arrival_Time.ToString();
                model.Quantity = quantity;
                model.Transfer = transfer;
                model.ExteraOption = ExteraOption;
                tour.Remainder = tour.Remainder - model.Quantity;

                Temp temp = new Temp();
                temp.datetime = DateTime.Now;
                //temp.ExpireTime = DateTime.Now.AddMinutes(10);
                temp.quantity = (int)model.Quantity;
                //temp.pay = model.Cost;
                temp.Step = 1;
                temp.Tour_Id = model.Id;
                db.Temps.Add(temp);

                db.SaveChanges();

                return View(model);
            }
            catch
            {
                return RedirectToAction("error", "Home");
            }


        }

        public JsonResult CheckMeli()
        {
            string meli = Request.QueryString[0];
            Code_meli code_Meli = new Code_meli();
            string ssn = CalendarMngr.ConvertNumbersToEnglish(meli);
            if (!code_Meli.chek(ssn.Trim()))
            {
                return Json("کدملی اشتباه است", JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PurchaseAgency(string transfer, string ExteraOption, Payment_user model, List<Payment_user> model_user, String termpolicy, int? tempId)
        {
            Tour purchase_tour = new Tour();
            RepTemp repTemp = new RepTemp();
            if (model.Id != 0)
            {
                purchase_tour = db.Tours.Find(model.Id);
                if (db.Tours.Find(model.Id).Remainder < model.quantity)
                {
                    TempData["color"] = "style=background-color:#e3c0c0";
                    if (purchase_tour.Type == "مادروکودک")
                    {
                        return RedirectToAction("Baby", "Agency", new { id = model.Id });
                    }
                    else
                    {
                        return RedirectToAction("Single_tour", "Agency", new { id = model.Id });
                    }
                }
            }
            int counter = 0;
            if (transfer == null)
            {
                Code_meli code_Meli = new Code_meli();
                Tour tour = db.Tours.Find(model.Tour_id);
                TempData["id"] = model.Tour_id;
                if (model.pay != 0)
                {
                    TempData["cost"] = model.pay;
                }
                else if (model_user[0].pay != 0)
                {
                    TempData["cost"] = model_user[0].pay;
                }
                int pay = model.pay;
                int quantity = (int)model_user[0].quantity;
                TempData["name"] = tour.Profile_Tour.Name_Group;
                TempData["destination"] = tour.Destination;
                TempData["type"] = tour.Type;
                TempData["origin"] = tour.Origin;
                TempData["enddate"] = CalendarMngr.JulianToPersian(tour.End_Date.Value.Date);
                TempData["startdate"] = CalendarMngr.JulianToPersian(tour.Start_Date.Value.Date);
                TempData["time"] = tour.Start_time.ToString();
                TempData["timeend"] = tour.Arrival_Time.ToString();
                TempData["quantity"] = model_user[0].quantity;
                TempData["placeprice"] = model_user[0].placeprice;
                TempData["transferPrice"] = model_user[0].transferPrice;
                TempData["transfer"] = model_user[0].transfer;
                TempData["ExteraOption"] = model_user[0].ExteraOption;
                foreach (var item in model_user)
                {
                    string ssn = CalendarMngr.ConvertNumbersToEnglish(item.ssn);
                    if (!code_Meli.chek(ssn))
                    {
                        ModelState.AddModelError($"[{counter}].ssn", "کدملی اشتباه است");
                        return View();
                    }
                    counter++;
                }
                if (!tempId.HasValue)
                {
                    TempData["color"] = "style=background-color:#e3c0c0";
                    return RedirectToAction("Single_tour", "Events", new { id = model.Id });
                }
                if (db.Temps.Where(c => c.ID == tempId).Count() == 0)
                {
                    if (db.Tours.Find(model.Id).Remainder < model.quantity)
                    {
                        TempData["color"] = "style=background-color:#e3c0c0";
                        return RedirectToAction("Single_tour", "Events", new { id = model.Id });
                    }
                }
                int i = 0;
                Guid g = Guid.NewGuid();
                foreach (var item in model_user)
                {
                    item.Tour_id = model.Tour_id;
                    if (db.Tours.Find(model.Tour_id).Type == "مادروکودک")
                    {
                        item.pay = model.pay;
                    }
                    else
                    {
                        item.pay = model.pay / quantity;
                    }
                    item.termpolicy = true;
                    item.Description = $"تور {model.Tour_id}";
                    item.DateReq = DateTime.Now;
                    if (Session["stateuser"] != null)
                    {
                        if (Session["stateuser"].ToString() == "0")
                        {
                            item.State = "0";
                        }
                        if (Session["stateuser"].ToString() == "1")
                        {
                            item.State = "1";
                        }
                    }
                    else
                    {
                        item.State = "0";
                    }
                    item.groupid = g;
                    item.Born_Date = CalendarMngr.PersianToJulian(item.Born_Date_fa).Date;
                    db.Payment_user.Add(item);
                    i++;
                }

                db.SaveChanges();
                id = model_user[0].Id;

                //db.Temps.Where(x => x.pay == pay && x.Step == 1 && x.quantity == quantity).OrderByDescending(x => x.datetime).First().pay_user_id = id;
                //db.SaveChanges();
                repTemp.setStap2(tempId.Value, model_user[0], Request.Url.ToString());



                model.Description = "توریپس";
                if (model_user[0].Tour.Type == "مادروکودک")
                {
                    model.pay = model_user[0].pay;
                }
                else
                {
                    model.pay = model_user[0].pay * quantity;
                }
                int id2 = id + 7;
                Guid guid = model_user[0].groupid.Value;
                try
                {
                    Zarinpal.PaymentGatewayImplementationServicePortTypeClient request1 = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
                    string autohority = string.Empty;

                    int Status = request1.PaymentRequest(_MerchantID, model.pay, model.Description, model.email, model.mobie, _CallbackURLAgency + "?id=" + id2 + "&&group=" + guid, out autohority);
                    if (Status > 0)
                    {
                        new Thread(() =>
                        {
                            CheckPaymentStatus(autohority);
                        }).Start();

                        return Redirect(_urlpay + autohority);
                    }
                    else
                    {
                        return RedirectToAction("error", "Home");
                    }

                }
                catch (Exception)
                {
                    return null;
                }


            }
            purchase_tour = db.Tours.Find(model.Id);
            model.Description = "ok";
            TempData["name"] = purchase_tour.Profile_Tour.Name_Group;
            TempData["id"] = model.Id;
            TempData["cost"] = model.pay;
            TempData["type"] = purchase_tour.Type;
            TempData["destination"] = purchase_tour.Destination;
            TempData["origin"] = purchase_tour.Origin;
            TempData["enddate"] = CalendarMngr.JulianToPersian(purchase_tour.End_Date.Value.Date);
            TempData["startdate"] = CalendarMngr.JulianToPersian(purchase_tour.Start_Date.Value.Date);
            TempData["time"] = purchase_tour.Start_time.ToString();
            TempData["timeend"] = purchase_tour.Arrival_Time.ToString();
            TempData["quantity"] = model.quantity;
            TempData["placeprice"] = model.placeprice;
            TempData["transferPrice"] = model.transferPrice;
            TempData["transfer"] = transfer;
            TempData["ExteraOption"] = ExteraOption;

            //purchase_tour.Remainder = purchase_tour.Remainder - model.quantity;



            TempData["tempId"] = repTemp.SetStep1(model.Id, model.quantity, tempId);
            //Temp temp = new Temp();
            //temp.datetime = DateTime.Now;
            //temp.ExpireTime = DateTime.Now.AddMinutes(10);
            //temp.quantity = (int)model.quantity;
            //temp.pay = model.pay;
            //temp.Step = 1;
            //temp.Tour_Id = model.Id;
            //db.Temps.Add(temp);
            //db.SaveChanges();


            return View(model_user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]  
        public ActionResult Payment(Payment_user model, List<Payment_user> model_user,String termpolicy, int cost,int Tour_id)
        {
            
            int i = 0;
            Guid g = Guid.NewGuid();
            foreach (var item in model_user)
            {
                item.Tour_id = Tour_id;
                item.pay = cost;
                item.termpolicy = true;
                item.Description = $"{Tour_id} با 7%تخفیف ";
                item.DateReq = DateTime.Now;
                item.groupid = g;
                item.Born_Date = CalendarMngr.PersianToJulian(item.Born_Date_fa).Date;
                db.Payment_user.Add(item);
                i++;
            }

            db.SaveChanges();
            id = model_user[0].Id;
            model.Description = "توریپس";
            model.pay = cost;
            int id2 = id+7;
            Guid guid = model_user[0].groupid.Value;
            try
            {
            Zarinpal.PaymentGatewayImplementationServicePortTypeClient request1 = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
            string autohority = string.Empty;
                
            int Status = request1.PaymentRequest(_MerchantID, model.pay, model.Description, model.email, model.mobie, _CallbackURL+"?id="+id2 + "&&group="+guid, out autohority);
                if (Status > 0)
                {
                    new Thread(() =>
                    {
                        CheckPaymentStatus(autohority);
                    }).Start();
                    
                    return Redirect(_urlpay+autohority);
                }
                else
                {
                    return RedirectToAction("error", "Home");
                }

            }
            catch (Exception)
            {
                return null;
            }
            //در صورت موفق آمیز بودن درخواست، کاربر به صفحه پرداخت هدایت می شود
            //در غیر این صورت خطا نمایش داده شود
        }
        [ValidateAntiForgeryToken]
        public ActionResult PaymentAgency(Payment_user model, List<Payment_user> model_user, String termpolicy, int cost, int Tour_id)
        {
            //Code_meli code = new Code_meli();
            //if (!code.chek(model_user[0].ssn)) {
            //    ModelState.AddModelError("ssn0","error");
            //    return PurchaseAgency((int)model.Tour_id, model_user[0].pay, (int)model_user[0].quantity, (int)model_user[0].placeprice, (int)model_user[0].transferPrice, model_user[0].transfer, model_user[0].ExteraOption, model_user);
            //}
            int i = 0;
            Guid g = Guid.NewGuid();
            foreach (var item in model_user)
            {
                item.Tour_id = Tour_id;
                item.pay = cost;
                item.termpolicy = true;
                item.Description = $"تور {Tour_id}";
                item.DateReq = DateTime.Now;
                item.groupid = g;
                item.Born_Date = CalendarMngr.PersianToJulian(item.Born_Date_fa).Date;
                db.Payment_user.Add(item);
                i++;
            }

            db.SaveChanges();
            id = model_user[0].Id;
            model.Description = "توریپس";
            model.pay = cost;
            int id2 = id + 7;
            Guid guid = model_user[0].groupid.Value;
            try
            {
                Zarinpal.PaymentGatewayImplementationServicePortTypeClient request1 = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
                string autohority = string.Empty;

                int Status = request1.PaymentRequest(_MerchantID, model.pay, model.Description, model.email, model.mobie, _CallbackURLAgency + "?id=" + id2 + "&&group=" + guid, out autohority);
                if (Status > 0)
                {
                    new Thread(() =>
                    {
                        CheckPaymentStatus(autohority);
                    }).Start();

                    return Redirect(_urlpay + autohority);
                }
                else
                {
                    return RedirectToAction("error", "Home");
                }

            }
            catch (Exception)
            {
                return null;
            }
            //در صورت موفق آمیز بودن درخواست، کاربر به صفحه پرداخت هدایت می شود
            //در غیر این صورت خطا نمایش داده شود
        }
        private void CheckPaymentStatus(string autohority)
        {

            Zarinpal.PaymentGatewayImplementationServicePortTypeClient request = new Zarinpal.PaymentGatewayImplementationServicePortTypeClient();
            long refID = -1;
            bool stopit = false;
            long curtick = DateTime.Now.Ticks / TimeSpan.TicksPerSecond;
            while (true)
            {
                if (stopit) 
                    break;
                int verf = -21;
                try
                {
                    verf = request.PaymentVerification(_MerchantID, autohority,_Amount , out refID);
                    db.Payment_user.Find(id).refId = request.PaymentVerification(_MerchantID, autohority, _Amount, out refID).ToString();
                }
                catch
                {

                }
                if (verf > 0)
                {
                    stopit = true;
                    if (OnPaymentAction != null)
                    {
                        OnPaymentAction(this, new PayArgs(verf, autohority, refID));
                    }
                }
                else
                {
                    if (!stopit && verf != -21)
                    {
                        stopit = true;
                       if (OnPaymentAction != null)
                        {
                            OnPaymentAction(this, new PayArgs(verf, autohority, refID));
                        }
                    }
                }
                long curtime = DateTime.Now.Ticks / TimeSpan.TicksPerSecond;
                if ((curtime - curtick) > 1850) // 30 * 60 +- 50
                {
                    if (!stopit)
                    {
                        OnPaymentAction(this, new PayArgs(-22, autohority, refID));
                        stopit = true;
                    }
                }
            }
            
        }

        public string URL
        {
            get { return _urlpay; }
        }
        
        public class PayArgs
        {
            private int _Status;
            private string _Autohority;
            private long _RefID;
            public PayArgs(int Status, string Autohority, long RefID)
            {
                _Status = Status;
                _Autohority = Autohority;
                _RefID = RefID;
            }
            public int Status
            {
                get { return _Status; }
            }
            public string Autohority
            {
                get { return _Autohority; }
            }
            public long RefID
            {
                get { return _RefID; }
            }
            private string GetFromLastSlash(string text)
            {
                int where = text.LastIndexOf('/');
                return text.Substring(where);
            }
        }
    }
}