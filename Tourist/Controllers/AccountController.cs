﻿using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;

namespace Tourist.Controllers
{
    public class AccountController : Controller
    {
        DbTourist db = new DbTourist();
        public ApplicationUserManager usermngr
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }


        public ActionResult Logout(string name)
        {
            //User.Identity.GetUserName()
            //User.Identity.Name
            //User.Identity.GetUserId()
            
            HttpContext.GetOwinContext().Authentication.SignOut();
            if (name == null)
            {
                return RedirectToAction("index", "home");
            }
            else {
                return RedirectToAction("index", "Agency",new {name = name });
            }
        }

        public ActionResult RegisterConfirm(RegisterViewModel model)
        {
            ApplicationUser user = usermngr.FindByEmail(model.Email);//AspNetUsers
            
            if (user == null)
            {
                user = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email,
                    EmailConfirmed = true,
                };
                IdentityResult status = usermngr.Create(user, model.Password);
                if (status.Succeeded == true)
                {
                    usermngr.AddToRole(user.Id,"PreAdmin");
                    TempData["msg"] = "حساب کاربری شما با موفقیت ایجاد گردید";
                }
                else
                {
                    TempData["msg"] = "خطا در ایجاد حساب کاربری";
                    return RedirectToAction("register","Account");
                }
            }
            else
            {
                TempData["msg"] = "این حساب کاربری قبلا ثبت شده است";
                return RedirectToAction("register", "Account");
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> LoginConfirm(LoginViewModel model)
        {
            try
            {
                var user =await usermngr.FindByNameAsync(model.Email);
                if (user == null)
                {
                    TempData["msg"] = "کاربری با این نام کاربری وجود ندارد";
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    var usersigninmngr = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();

                    if (usermngr.IsInRole(user.Id, "Admin"))
                    {
                        var status = await usersigninmngr.PasswordSignInAsync(model.Email, model.Password,
                        model.RememberMe, false);
                        //User.Identity.Name
                        switch (status)
                        {
                            case SignInStatus.Success:
                                return RedirectToAction("index", "Dashboard");
                        }
                                
                    }
                    else if (usermngr.IsInRole(user.Id, "PreAdmin"))
                    {
                        var status =await usersigninmngr.PasswordSignInAsync(model.Email, model.Password,
                                           model.RememberMe, false);
                        //User.Identity.Name
                        switch (status)
                        {
                            case SignInStatus.Success:
                                return RedirectToAction("pricing", "Dashboard");
                        }
                    }
                    else
                    {

                        var status =await usersigninmngr.PasswordSignInAsync(model.Email, model.Password,
                            model.RememberMe, false);
                        //User.Identity.Name
                        if (status == SignInStatus.Success)
                        {
                            TempData["msg"] = "شما اجازه دسترسی به این قسمت سایت ندارید";
                        }
                        else
                        {
                            TempData["msg"] = "نام کاربری یا رمز شما صحیح نمی باشد";
                        }
                    }
                }
                return RedirectToAction("login", "account");
            }
            catch (Exception)
            {

               return RedirectToAction("login", "account");
            }
            
        }

        //PasswordRecovery
        public ActionResult ResetPasswordConfirm(PasswordRecoveryModel model)
        {
            string userid = Session["userid"].ToString();
            string token = Session["token"].ToString();
            Session.Remove("userid");
            Session.Remove("token");
            var result = usermngr.ResetPassword(userid, token, model.Password);
            if (result.Succeeded)
            {
                TempData["msg"] = "رمز شما با موفقیت تغییر کرد";
            }
            else
            {
                TempData["msg"] = "خطا در تغییر رمز";
            }
            return RedirectToAction("login", "account");
        }
        public ActionResult PasswordRecoveryConfirm(string userId, string token)
        {
            Session["userid"] = userId;
            Session["token"] = token;
            return RedirectToAction("NewPassword");
        }
        public ActionResult NewPassword()
        {
            return View("~/views/account/PasswordRecoveryConfirm.cshtml");
        }
        public ActionResult PasswordRecovery(string username)
        {
            try {
            var user = usermngr.FindByName(username);
            var token = usermngr.GeneratePasswordResetToken(user.Id);

            string emaillink =
                            Url.Action("PasswordRecoveryConfirm", "Account",
                                     new { userId = user.Id, token = token }
                                     , Request.Url.Scheme);
            usermngr.SendEmail(user.Id, "ایمیل تغییر رمز",
                $"<label>کاربر محترم</label><br/><b>{user.UserName}</b>" +
                  $"جهت تغییر رمز کاربری لینک زیر را کلیک نمایید<br/>" +
                  $"<a href='{emaillink}'>تغییر رمز حساب کاربری</a>"
                );

                TempData["msg"] = "لینک تغییر رمز  به ایمیل شما ارسال شد";
                return RedirectToAction("Login", "Account");
            } catch {
                TempData["msg"] = "نام کاربری وجود ندارد";
                return RedirectToAction("Login", "Account");
            }
            //var token2 = usermngr.GeneratePasswordResetToken(user.Id);
            //var token3 = usermngr.GeneratePasswordResetToken(user.Id);

            //TempData["msg"] = "لینک تغییر رمز  به ایمیل شما ارسال شد";
            //return RedirectToAction("Login", "Account");
        }

        public ActionResult RegisterAgency(Profile_Tour model, HttpPostedFileBase license_image)
        {
            try
            {
                
                    byte[] image = null;
                    if (license_image != null)
                    {
                        image = new byte[license_image.ContentLength];
                        license_image.InputStream.Read(image, 0, image.Length);
                        model.Image_License = image;
                    }
                     db.Profile_Tour.Add(model);
                     db.SaveChanges();


                //MailMessage msg = new MailMessage();
                //msg.To.Add(new MailAddress("sinadarbouy@gmail.com"));
                //msg.From = new MailAddress("mail@dor-dor.ir");
                //msg.Subject = "RegisterAgency";
                //msg.Body = $"<label>آژانس</label><br/><br>{model.Manager}</br><br>{model.Name_Agency}</br><br>{model.Tel_Agency}</br>";
                //msg.IsBodyHtml = true;

                //SmtpClient smtp = new SmtpClient("mail.dor-dor.ir", 25);
                //smtp.Credentials = new System.Net.NetworkCredential("mail@dor-dor.ir", "Sina-1376");
                ////smtp.UseDefaultCredentials = true;
                ////smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtp.EnableSsl = false;
                //smtp.Send(msg);
                //usermngr.SendEmail("sinadarbouy@gmail.com", "ایمیل تغییر رمز",
                //    $"<label>کاربر محترم</label><br/><b>{name}</b>" +
                //      $"جهت تغییر رمز کاربری لینک زیر را کلیک نمایید<br/>" 

                //    );


                SMS.Send sms = new SMS.Send();
                long[] rec = null;
                byte[] status = null;
                string[] Recipients = new string[] { "09174542538",model.mobile,"09178273067" };
                int retval = sms.SendSms("9174542538", "4398", Recipients, "20002222400065", "ثبت نام شما با موفقیت انجام شد. پس از بررسی اطلاعات وارد شده، کارشناسان ما با شما تماس خواهند گرفت", false, "", ref rec, ref status);
                TempData["msg"] = "ثبت نام شما با موفقیت انجام شد. پس از بررسی اطلاعات وارد شده، کارشناسان ما با شما تماس خواهند گرفت.";
               
                return RedirectToAction("Login", "Account");
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
        }
        


        // GET: Account
        public ActionResult Register()
        {
            //byte[] data = Encoding.UTF8.GetBytes(key.ToString());
            //byte[] result;

            //SHA512 shaM = new SHA512Managed();
            //result = shaM.ComputeHash(data);

            //if (BitConverter.ToString(result) != value) {
            //    return RedirectToAction("","");
            //}
            //Session["value"] = BitConverter.ToString(result);
            //TempData["key"] = key;

            return View();
        }
        public ActionResult SendMailToCreateUser(string name) {
            try
            {

                var id = db.Profile_Tour.Where(x => x.Name_Group == name).First().ID;

                byte[] data = Encoding.UTF8.GetBytes(id.ToString());
                byte[] result;

                SHA512 shaM = new SHA512Managed();
                result = shaM.ComputeHash(data);
                var token = BitConverter.ToString(result).Replace("-", String.Empty);

                string emaillink =
                                Url.Action("CreateUserConfirm1", "Account",
                                         new { id, token }
                                         , Request.Url.Scheme);

                TempData["Link"] = emaillink;
            }
            catch
            {
                return RedirectToAction("Login", "Account");
            }
            //var token2 = usermngr.GeneratePasswordResetToken(user.Id);
            //var token3 = usermngr.GeneratePasswordResetToken(user.Id);

            return RedirectToAction("LinkRegister", "Account");



            return View();
        }
        public ActionResult CreateUserConfirm1(string id, string token)
        {
            Session["userid"] = id;
            Session["token"] = token;
            return RedirectToAction("CreateUserConfirm2");
        }
        public ActionResult CreateUserConfirm2(string id, string token) {
            return View("~/views/account/Register.cshtml");
            
        }
        public ActionResult LinkRegister()
        {
            //byte[] data = Encoding.UTF8.GetBytes(key.ToString());
            //byte[] result;

            //SHA512 shaM = new SHA512Managed();
            //result = shaM.ComputeHash(data);

            //TempData["link"] = $"http://tourips.com/account/register/key={key}value={BitConverter.ToString(result).Substring(0,8).Replace("-",String.Empty)}";



            return View();
        }
        [Route("Login")]
        public ActionResult Login()
        {
            return View();
        }
    }
}