﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;

namespace Tourist.Utilities.Filter
{
    public class FilterLog:ActionFilterAttribute
    {
        DbTourist db;
        public FilterLog()
        {
            db = new DbTourist();
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {




            PaymentLog pay_log = new PaymentLog();

            pay_log.RefNUM = filterContext.HttpContext.Request.Params["RefNum"].ToString();
            pay_log.ResNUM = filterContext.HttpContext.Request.Params["ResNUM"].ToString();
            pay_log.State = filterContext.HttpContext.Request.Params["state"].ToString();
            pay_log.date = DateTime.Now;
            pay_log.description = "";
            db.PaymentLogs.Add(pay_log);

            db.SaveChanges();
            base.OnActionExecuting(filterContext);
        }


    }
}