﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tourist.CommonMethods;
using Tourist.Models;
using Tourist.Utilities.Discount;
using Tourist.ViewModels.PaymentViewModel;

namespace Payments
{
    public  class Payment
    {
        private IPaymentRequest payRequest;
        private Controller controller;
        private DbTourist db;
        public Payment(Controller controller)
        {
            if (controller != null)
            {
                this.controller = controller;
            }
            else
                throw new Exception();
            db = new DbTourist();
        }

       public ActionResult Request(PaymentWith with,Guid groupId,int quantity,int cost,int tempId)
        {
            List<Payment_user> payment_user = null;

            payment_user = db.Payment_user.Where(x => x.groupid == groupId).ToList();


            ///////////////////////////////////////////////////////////////////////////////select payment gateway
            if (with == PaymentWith.Sep)
                payRequest = new PayRequestSep(controller);
            
             
            ////////////////////////////////////////////////////////////////////////////////

            string description = "توریپس";
            Payment_user firestUser = payment_user.First();
            //////////////////////////////////////////////////////////////////////////////set id user_payment to temp
           
            Repositories.RepTemp repTemp = new Repositories.RepTemp();
            repTemp.setStap2(tempId,firestUser, controller.Request.Url.ToString());

            /////////////////////////////////////////////////////////////////Discount Set

                ManageDiscount manageDiscount = new ManageDiscount(db);
                manageDiscount.SetTempDiscount(firestUser, quantity);


                db.SaveChanges();
          
         
          
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            return payRequest.Request($"https://Tourips.com/Payment/endp2", description,cost,firestUser.groupid.Value.ToString());
        }
     
       public ActionResult RequestUpAgency(PaymentWith with,int price,string profileId)
        {
            
            if (with == PaymentWith.Sep)
                payRequest = new PayRequestSep(controller);
            return payRequest.Request($"http://{controller.Request.Url.Authority}/Payment/UpgradeProfileConfirm","",price, profileId);
        }

        public ModelPayment Verify(PaymentWith with, string MID, string RefNUM, string ResNUM, string state, string TraceNo, string password)
        {
            //Guid group = Guid.Parse(ResNUM);
            ////db.Payment_user.Where(c => c.refId == RefNUM && c.groupid != group).Any();
            //bool value = true;
            //if (value)
            //{
            //    IPaymentVerify payment = new SepVerify(MID, RefNUM, ResNUM, state, TraceNo, password);

            //    return payment.GetVerify();
            //}
            //throw new Exception();


            Guid group;
            if (Guid.TryParse(ResNUM, out group))
            {
                //db.Payment_user.Where(c => c.refId == RefNUM && c.groupid != group).Any();
                bool value = true;
                if (value)
                {
                    IPaymentVerify payment = new SepVerify(MID, RefNUM, ResNUM, state, TraceNo, password);
                    return payment.GetVerify();
                }
            }
            else
            {
                IPaymentVerify payment = new SepVerify(MID, RefNUM, ResNUM, state, TraceNo, password);
                return payment.GetVerify();
            }

            throw new Exception();
        }


    }
    public enum PaymentWith
    {
        Sep = 0,
        ZarinPal = 1
    }
}