﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payments
{
    public class ModelPayment
    {
        public ModelPayment()
        {
            isError = true;
            RefrenceNumber = string.Empty;
            ReservationNumber = string.Empty;
            TransactionState = string.Empty;
            errorMsg = string.Empty;
            successMsg = string.Empty;
            Description = string.Empty;
        }
       
        public string RefrenceNumber { get; set; }
        public string ReservationNumber { get; set; }
        public string TransactionState { get; set; }
        public string TraceNumber { get; set; }
        public bool  isError { get; set; }
        public string errorMsg { get; set; }
        public string successMsg { get; set; }
        public string Description { get; set; }

    }
}