﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fluentx.Mvc;
namespace Payments
{
    public  class PayRequestSep:IPaymentRequest
    {

        private  string MID = "11221931";
        private Controller control;
        //private string url;

        public  PayRequestSep(Controller control)
        {
            if(control !=null)
            {
                this.control = control;
            }else
            {
                throw new Exception();
            }
            //url = "http://" + control.Request.Url.Authority + "/Payment/endp2";
        }


        private  ActionResult Request(string  url, int amount, int segAmount1, int segAmount2, int segAmount3, int segAmount4,
          int segAmount5, int segAmount6, string data1, string data2, long wage, string resNum)
        {
           

            Tourist.ir.shaparak.sep.PaymentIFBinding initPayment = new Tourist.ir.shaparak.sep.PaymentIFBinding();
            string token = initPayment.RequestToken(MID, resNum, amount, segAmount1, segAmount2, segAmount3, segAmount4, segAmount5, segAmount6, data1, data2, wage);
            if (!string.IsNullOrEmpty(token))
            {

                Dictionary<string, object> query = new Dictionary<string, object>();
                query.Add("Token", token);
                query.Add("RedirectURL", url);
                return new RedirectAndPostActionResult("https://sep.shaparak.ir/payment.aspx", query);
            }
            return null;
        }

        
        public ActionResult Request(string url, string description, int amount,string groupId)
        {
           ////////
            return Request(url,amount,0,0, 0, 0, 0, 0, description,"",0,groupId.ToString());
        }

     
    }
}