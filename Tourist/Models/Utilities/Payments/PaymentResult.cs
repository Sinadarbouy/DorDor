﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Payments
{
    public  class PaymentResult
    {
        private  ModelPayment modelPayment;

        public  PaymentResult(string MID,string RefNUM,string ResNUM,string state,string TraceNo,string password)
        {
              if (RequestUnpack(RefNUM,ResNUM,state,TraceNo))
                {
                    if (modelPayment.TransactionState.Equals("OK"))
                    {
                        ///////////////////////////////////////////////////////////////////////////////////
                        //   *** IMPORTANT  ****   ATTENTION
                        // Here you should check refrenceNumber in your DataBase tp prevent double spending
                        ///////////////////////////////////////////////////////////////////////////////////

                        ///For Ignore SSL Error
                        
                        ServicePointManager.ServerCertificateValidationCallback =
                            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                        ///WebService Instance
                        var srv = new Tourist.ir.sep.verify.PaymentIFBinding();
                        var result = srv.verifyTransaction(modelPayment.RefrenceNumber, MID);

                        if (result > 0)
                        {
                            //if (result == amount) // چک کردن مبلغ بازگشتی از سرویس با مبلغ تراکنش
                            //{
                                modelPayment.isError = false;
                               modelPayment.Description = "بانک صحت رسيد ديجيتالي شما را تصديق نمود. فرايند خريد تکميل گشت";
                               modelPayment.Description += "<br/>" + " شماره رسید : " + modelPayment.RefrenceNumber;
                                modelPayment.successMsg = "OK";
                            //}
                            //else
                            //{
                            //    string userName = MID;//نام کاربری همان ام آی دی است
                            //    string pass = password; // رمز عبور برای شما ایمیل شده است
                            //    srv.reverseTransaction(RefNUM,MID, userName, pass);
                            //}
                        }
                        else
                        {
                            modelPayment.errorMsg = TransactionChecking((int)result);
                        }
                    }
                    else
                    {
                        Analizer(); 

                    }

                }

           
        }

        public void Analizer()
        {
            modelPayment.isError = true;
            modelPayment.errorMsg = "متاسفانه بانک خريد شما را تاييد نکرده است";

            if (modelPayment.TransactionState.Equals("Canceled By User") || modelPayment.TransactionState.Equals(string.Empty))
            {
                // Transaction was canceled by user
                modelPayment.isError = true;
                modelPayment.errorMsg = "تراكنش توسط خريدار كنسل شد";
                modelPayment.Description = "عملیات ناموفق";
            }

            else if (modelPayment.TransactionState.Equals("Invalid Merchant"))
            {
                // Amount of revers teransaction is more than teransaction
                modelPayment.isError = true;
                modelPayment.errorMsg = "پذیرنده فروشگاهی نامعتبر است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            //else if (modelPayment.TransactionState.Equals("Honour With Identification"))
            //{
            //    // Amount of revers teransaction is more than teransaction
            //    modelPayment.isError = true;
            //    modelPayment.errorMsg = "تراکنش نامعتبر است";
            //}
            else if (modelPayment.TransactionState.Equals("Invalid Transaction"))
            {
                // Can not find teransaction
                modelPayment.isError = true;
                modelPayment.errorMsg = "تراکنس نامعتبر است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Invalid Card Number"))
            {
                // Card number is wrong
                modelPayment.isError = true;
                modelPayment.errorMsg = "شماره کارت ارسالی نامعتبر است(وجود ندارد(";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("No Such Issuer"))
            {
                // Issuer can not find
                modelPayment.isError = true;
                modelPayment.errorMsg = "چنین صادر کننده کارتی وجود ندارد";
                modelPayment.Description = "کارت نامعتبر ";
            }
            else if (modelPayment.TransactionState.Equals("Expired Card Pick Up"))
            {
                // The card is expired
                modelPayment.isError = true;
                modelPayment.errorMsg = "از تاریخ اعتبار کارت کذشته است کارت دیگر معتبر نیست";
                modelPayment.Description = "کارت نامعتبر است تاریخ انقضای کارت سپری شده است";
            }
            else if (modelPayment.TransactionState.Equals("Allowable PIN Tries Exceeded Pick Up"))
            {
                // For third time user enter a wrong PIN so card become invalid
            }
            else if (modelPayment.TransactionState.Equals("Do Not Honour"))
            {
                // Pin card is wrong
                modelPayment.isError = true;
                modelPayment.errorMsg = "از انجام تراکنش صرفه نظر شده";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }

            else if (modelPayment.TransactionState.Equals("Suspected Fraud Pick Up"))
            {
                // User did not insert cvv2 & expiredate or they are wrong.
                modelPayment.isError = true;
                modelPayment.errorMsg = "";
                modelPayment.Description = "عملیات ناموفق";
            }
            //else if (modelPayment.TransactionState.Equals("Approved Update Track3"))
            //{
            //    // User did not insert cvv2 & expiredate or they are wrong.
            //    modelPayment.isError = true;
            //    modelPayment.errorMsg = "تراکنش مورد تایید است و اطلاعات شیار سوم کارت بروز رسانی شود";
            //    modelPayment.Description = "عملیات موفق";
            //}
            else if (modelPayment.TransactionState.Equals("Reenter Transaction"))
            {
                // User did not insert cvv2 & expiredate or they are wrong.
                modelPayment.isError = true;
                modelPayment.errorMsg = "تراکنش مجددا ارسال گردد";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("Suspected Fraud Pick Up"))
            {
                // User did not insert cvv2 & expiredate or they are wrong.
                modelPayment.isError = true;
                modelPayment.errorMsg = "خریدار فیلد cvv2 یا فیلد expDate اشتبا وارد کرده است یا دارنده کارت مظنون به تقلب است";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("Bank Not Supported By Switch"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "پذیرنده توسط سوییچ پشتیبانی نمی شود";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("UnAcceptable Transaction Fee"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارمزد ارسالی پذیرنده غیره قابل قبول است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Format Error"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "قالب پیام دارای اشکال است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Allowable PIN Tries Exceeded Pick Up"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "تعداد ورود رمزه غلط بیش از حد مجاز است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("No Credi Acount"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت حساب اعتباری ندارد";
                modelPayment.Description = "کارت نامعتبر است";
            }
            else if (modelPayment.TransactionState.Equals("Requested Function"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "عملیات درخواستی پشتیبانی نمی گردد";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("Lost Card"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت مفقودی می باشد";
                modelPayment.Description = "کارت مفقودی است";
            }
            else if (modelPayment.TransactionState.Equals("No Universal Account"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت حساب عمومی ندارد";
                modelPayment.Description = "کارت نامعتبر است";
            }
            else if (modelPayment.TransactionState.Equals("Stolen Card"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت مسروقه است";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("No Investment Acount"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت حساب سرمایه گذاری ندارد";
                modelPayment.Description = "کارت نامعتبراست";
            }
            else if (modelPayment.TransactionState.Equals("No Cheque Account"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت حساب جاری ندارد";
                modelPayment.Description = "کارت نامعتبر است";
            }
            else if (modelPayment.TransactionState.Equals("No Saving Account"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت حساب قرض الحسنه ندارد";
                modelPayment.Description = "کارت نامعتبراست";
            }
            else if (modelPayment.TransactionState.Equals("Expired Account"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "تاریخ انقضای کارت سپری شده است";
                modelPayment.Description = "کارت نا معتبر است (تاریخ انقضای کارت سپری شده است(";
            }
            else if (modelPayment.TransactionState.Equals("Incorrect PIN"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "خریدار رمزه کارت (pin) را اشتبا وارد کرده است";
                modelPayment.Description = "رمزه نامعتبراست";
            }

            else if (modelPayment.TransactionState.Equals("No Card Record"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت نامعتبر است";
                modelPayment.Description = "کارت نامعتبر است";
            }
            else if (modelPayment.TransactionState.Equals("Transaction Not Permitted To CardHolder"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "انجام تراکنش مربوطه توسط دارنده کارت مجاز نمی باشد";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Transaction Not Permitted To Terminal"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "انجام تراکنش مربوطه توسط پایانه انجام دهنده مجاز نمی باشد";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Exceeds Withdrawal Amount Limit"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "مبلغ تراکنش بیش از حد مجاز است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Restricted Card-Decline"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "کارت محدود شده است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Security Violation"))
            {
                // there are not suficient funds in the account
                modelPayment.isError = true;
                modelPayment.errorMsg = "تمهیدات امنیتی نقض گردیده است";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("Exceeds Withdrawal Frequency Limit"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "تراکنش در شعبه بانکی Timeout خوده است.";
                modelPayment.Description = "   تراکنش ناممعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Response Received Too Late"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "تعداد درخواست های تراکنش بیش از حد مجاز است";
                modelPayment.Description = "_";
            }
            else if (modelPayment.TransactionState.Equals("PIN Reties Exceeds-Slm"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "تعداد دفعات ورود روزه غلط بیش از حد مجاز است";
                modelPayment.Description = "   تراکنش نامعتبر است در صورت کسر وجه از حساب شما مبلغ مذکور تی 72 ساعت به حساب شما عودت داده خواهد شددر غیر این صورت به شماره تلفن 521-84585 تماس حاصل نماید  ";
            }
            else if (modelPayment.TransactionState.Equals("Invalid Amount"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "مبغ سند برگشتی از مبلغ تراکنش اصلی بیشتر است.";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("Issuer Down Slm"))
            {
                // The bank server is down
                modelPayment.isError = true;
                modelPayment.errorMsg = "سیستم بانک صادر کننده کارت خریدار در وضعیت عملیاتی نیست";
                modelPayment.Description = "عملیات ناموفق";

            }
            else if (modelPayment.TransactionState.Equals("Cut Off Is Inprogress"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "سامانه مقصد تراکنش در حال انجام عملیات پایان روز می باشد";
                modelPayment.Description = " عملیات ناموفق (سامانه مقصد تراکنش در حال انجام عملیات پایان روز می باشد) ";

            }
            else if (modelPayment.TransactionState.Equals("Transaction Cannot Be Completed"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "تراکنش Authorize شده است)شمارpinوشماره pan درست هستند (امکان سند خوردن وجود ندارد";
                modelPayment.Description = "عملیات ناموفق";
            }
            else if (modelPayment.TransactionState.Equals("TME Error"))
            {
                // unknown error occur
                modelPayment.isError = true;
                modelPayment.errorMsg = "کلیه خطاهای دیگر بانکی باعث ایجاد چنین خطایی می گردد ";
                modelPayment.Description = "عملیات ناموفق";

            }
            else if (modelPayment.TransactionState.Equals("Canceled By Issuer"))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "صادر کننده از انجام تراکنش صرفه نظر کرد";
                modelPayment.Description = "عملیات ناموفق";
            }
        }
        public ModelPayment GetResult()
        {
            return modelPayment;
        }

        private bool RequestUnpack(string RefNUM,string ResNUM,string state,string TraceNo)
        {
            modelPayment = new ModelPayment();
            if (RequestFeildIsEmpty(RefNUM,ResNUM,state,TraceNo)) return false;
          
            modelPayment.RefrenceNumber = RefNUM;
            modelPayment.ReservationNumber =ResNUM;
            modelPayment.TransactionState =state;
            modelPayment.TraceNumber = TraceNo;

            return true;
        }
        private bool RequestFeildIsEmpty(string RefNUM,string ResNUM,string state,string TraceNo)
        {

            if (ResNUM.ToString().Equals(string.Empty) && state.ToString().Equals(string.Empty))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "خطا در برقرار ارتباط با بانک";
                return true;
            }
            if (RefNUM.ToString().Equals(string.Empty) && state.ToString().Equals(string.Empty))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "فرايند انتقال وجه با موفقيت انجام شده است اما فرايند تاييد رسيد ديجيتالي با خطا مواجه گشت";
                return true;
            }

           
            if (state.ToString().Equals(string.Empty))
            {
                modelPayment.isError = true;
                modelPayment.errorMsg = "خريد شما توسط بانک تاييد شده است اما رسيد ديجيتالي شما تاييد نگشت! مشکلي در فرايند رزرو خريد شما پيش آمده است";
                return true;
            }

            return false;
        }

        private string TransactionChecking(int i)
        {
#pragma warning disable CS0219 // The variable 'isError' is assigned but its value is never used
            bool isError=false;
#pragma warning restore CS0219 // The variable 'isError' is assigned but its value is never used
            string errorMsg = "";
            switch (i)
            {

                case -1:		//TP_ERROR
                    isError = true;
                    errorMsg = "بروز خطا درهنگام بررسي صحت رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-1";
                    break;
                case -2:		//ACCOUNTS_DONT_MATCH
                    isError = true;
                    errorMsg = "بروز خطا در هنگام تاييد رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-2";
                    break;
                case -3:		//BAD_INPUT
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-3";
                    break;
                case -4:		//INVALID_PASSWORD_OR_ACCOUNT
                    isError = true;
                    errorMsg = "خطاي دروني سيستم درهنگام بررسي صحت رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-4";
                    break;
                case -5:		//DATABASE_EXCEPTION
                    isError = true;
                    errorMsg = "خطاي دروني سيستم درهنگام بررسي صحت رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-5";
                    break;
                case -7:		//ERROR_STR_NULL
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-7";
                    break;
                case -8:		//ERROR_STR_TOO_LONG
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-8";
                    break;
                case -9:		//ERROR_STR_NOT_AL_NUM
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-9";
                    break;
                case -10:	//ERROR_STR_NOT_BASE64
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-10";
                    break;
                case -11:	//ERROR_STR_TOO_SHORT
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-11";
                    break;
                case -12:		//ERROR_STR_NULL
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-12";
                    break;
                case -13:		//ERROR IN AMOUNT OF REVERS TRANSACTION AMOUNT
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-13";
                    break;
                case -14:	//INVALID TRANSACTION
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-14";
                    break;
                case -15:	//RETERNED AMOUNT IS WRONG
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-15";
                    break;
                case -16:	//INTERNAL ERROR
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-16";
                    break;
                case -17:	// REVERS TRANSACTIN FROM OTHER BANK
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-17";
                    break;
                case -18:	//INVALID IP
                    isError = true;
                    errorMsg = "خطا در پردازش رسيد ديجيتالي در نتيجه خريد شما تاييد نگرييد" + "-18";
                    break;

            }
            return errorMsg;
        }
    }
}