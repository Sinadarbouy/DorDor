﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Tourist.Models;

namespace HTMLToPDF
{
    public class Cells
    {
       
     
        public Cells()
        {

        }
        public static string Title { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }

        public string NCode { get; set; }
        public string DateGenerate { get; set; }
        public string PhoneNumber { get; set; }
        public string Amount { get; set; }
        public string Col1 { get; set; }
        public string Col2 { get; set; }
        public string Description { get; set; }
        public string Transition_no { get; set; }
        public string FatherName { get; set; }

       
    }
    public class PdfReport
    {
        string[] listHeader;
        /// <summary>
        /// تور جزيره ی رويايی هرمز
        /// </summary>
        string fontNameF = "Yekan";
        public string FontPath{ get; set; }
        public string Header { get; set; }
        /// <summary>
        /// 1397/8/15
        /// </summary>
        public string DateHeader{ get; set; }

        /// <summary>
        /// وسیله نقلیه
        /// </summary>
        public string Column1 { get; set; }
        /// <summary>
        /// اپشن
        /// </summary>
        public string Column2 { get; set; }
        /// <summary>
        /// cells.Add(new Cells()
        ///         {
        ///            Name = "لاله زارع",
        ///         Id = i.ToString(),
        ///         NCode = "2280334011",
        ///         Amount = "234234",
        ///         DateGenerate = "12-1212-2",
        ///         PhoneNumber="09175331828",
        ///         Transition_no="123123123131",
        ///         Col1= "اتوبوس وی ای پی 295000 تومان",
        ///         Col2= "۲شب اقامت در اقامتگاه محلی"
        ///         });
        /// </summary>
        public List<Cells> Rows { get; set; }

        public PdfReport()
        {
           
          
        }
        public Byte[] Generic()
        {
            listHeader = new string[] { "شماره تراکنش","توضیحات", Column2, Column1, "مبلغ", "تلفن", " تاریخ تولد", "نام پدر", "شماره ملی", " اسم", "ردیف" };
            string title = Header ;
            Document doc = new Document(PageSize.A4);
            MemoryStream ms = new MemoryStream();
            Font headerFont = GetFont(fontNameF);
            Font titleFont = GetFont(fontNameF);
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, ms);
            ITextEvents PageEvent = new ITextEvents();
            PageEvent.FontHeader = GetFont(fontNameF);
            PageEvent.Header = title;
            pdfWriter.PageEvent =PageEvent;
            doc.Open();
            /////////////////////////////////////////table title
            PdfPTable tableTitle = new PdfPTable(1);
            tableTitle.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
            tableTitle.HorizontalAlignment = Element.ALIGN_RIGHT;
            titleFont.SetStyle("font-weight:bold");
            tableTitle.WidthPercentage = 100;
            tableTitle.TotalWidth = 550f;
            tableTitle.LockedWidth = true;
            


            tableTitle.AddCell(new PdfPCell(new Phrase("", titleFont))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingBottom = 10f,
                Border = 0
            });

            doc.Add(tableTitle);
            //////////////////////////////////////////table Header
            PdfPTable pTable = new PdfPTable(11);
            pTable.TotalWidth = 586f;
            pTable.LockedWidth = true;
            pTable.SpacingAfter = 1f;
            pTable.WidthPercentage = 100;

            pTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
            float[] widths = new float[] { 3f,2.5f, 3.5f, 5f, 2f, 3.5f, 3f,2f, 3.5f, 3.3f, 1.2f };
            pTable.SetWidths(widths);
            headerFont.Size = 8f;
            headerFont.SetStyle("font-weight:bold");
            for (int i = 0; i < listHeader.Length; i++)
            {
                PdfPCell cell0 = new PdfPCell(new Phrase(listHeader[i], headerFont));
                cell0.BorderWidth = 1.15f;
                cell0.BorderColor = new BaseColor(220, 220, 220);
                cell0.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell0.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                cell0.PaddingBottom = 10f;
                cell0.PaddingRight = 2f;
                pTable.AddCell(cell0);
            }
            Font fontRows = GetFont(fontNameF);
            fontRows.Size = 8f;
            ///////////////////////////////////////////cells
            for (int i = 0; i < Rows.Count; i++)
            {

                BaseColor backgroundColor;
                if (i % 2 == 1)
                    backgroundColor = new BaseColor(220, 220, 220);
                else
                    backgroundColor = new BaseColor(255, 255, 255);
               
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Transition_no, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2f,
                    BackgroundColor = backgroundColor

                });
                /////////////////////////////////////////
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Description, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2f,
                    BackgroundColor = backgroundColor

                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Col2, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2f,
                    BackgroundColor = backgroundColor
                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Col1, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2f,
                    BackgroundColor = backgroundColor
                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Amount, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2f,
                    BackgroundColor = backgroundColor
                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].PhoneNumber, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor
                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].DateGenerate, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor

                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].FatherName, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor


                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].NCode, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor

                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Name, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor


                });
                pTable.AddCell(new PdfPCell(new Phrase(Rows[i].Id, fontRows))
                {
                    BorderWidth = 1.15f,
                    BorderColor = new BaseColor(220, 220, 220),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    RunDirection = PdfWriter.RUN_DIRECTION_LTR,
                    PaddingBottom = 10f,
                    PaddingRight = 2,
                    BackgroundColor = backgroundColor


                });

            }
            doc.Add(pTable);

            doc.Close();
            return ms.ToArray();
        }

    
        private Font GetFont(string fontName)
        {
            string fontPath = "~/HTMLToPDF/";
            if (!string.IsNullOrEmpty(FontPath))
                fontPath = FontPath;

            string fontpathAbsolute;
            if (!string.IsNullOrEmpty(fontPath))
                fontpathAbsolute = HttpContext.Current.Server.MapPath(fontPath + "fonts/" + fontName + ".ttf");
            else
                fontpathAbsolute = HttpContext.Current.Server.MapPath(fontPath+"fonts/" + fontName + ".ttf");
            BaseFont customfont = BaseFont.CreateFont(fontpathAbsolute, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(customfont);

            return font;
            //FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }
    }
 
    public class ITextEvents : PdfPageEventHelper
    {
        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;

        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;

        #region Fields
        private string _header;
        #endregion

        #region Properties
        public ITextEvents()
        {
            Header = "جهت بررسى فارسى نویسى فارسى نویس";
        }
        public Font FontHeader { get; set; }
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }
        
        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            FontHeader.SetStyle("font-weight:bold");
            Phrase p1Header = new Phrase(Header, FontHeader);
           
            //Create PdfTable object
            PdfPTable pdfTab = new PdfPTable(2);
            pdfTab.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
            pdfTab.HorizontalAlignment = Element.ALIGN_RIGHT;
            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            PdfPCell pdfCell1 = new PdfPCell();
           PdfPCell pdfCell2 = new PdfPCell(p1Header);
            pdfCell1.HorizontalAlignment = PdfWriter.RUN_DIRECTION_RTL;
            pdfCell1.RunDirection = Element.ALIGN_RIGHT;
            pdfCell2.PaddingTop = -20f;
            
            String text = "Page " + writer.PageNumber + " of ";

     
            //Add paging to footer
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            }

          

            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
          //  pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
         

            pdfCell1.Border = 0;

            pdfCell2.Border = 0;
          
            pdfCell2.PaddingRight = -125f;
            //add all three cells into PdfTable
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
           
            
         

            pdfTab.TotalWidth = document.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 70;
            //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;    

            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value

            //Move the pointer and draw line to separate header section from rest of page
            cb.MoveTo(40, document.PageSize.Height - 100);
           // cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
            cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, document.PageSize.GetBottom(50));
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 12);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 12);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber ).ToString());
            footerTemplate.EndText();
        }
    }
}