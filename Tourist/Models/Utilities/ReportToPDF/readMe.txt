﻿public ActionResult Test()
{
    PdfRepost t = new PdfRepost();
    t.DateHeader = "1397/8/15";
    t.Header = "تور جزيره ی رويايی هرمز";
    t.Column1 = "کول1";
    t.Column2 = "کول2";
    t.FontPath = "~/Utilities/HTMLToPDF/";
    List<Cells> cells = new List<Cells>();
    for (int i = 0; i < 20; i++)
    {
        cells.Add(new Cells()
        {
            Name = "لاله زارع",
            Id = i.ToString(),
            NCode = "2280334011",
            Amount = "234234",
            DateGenerate = "12-1212-2",
            PhoneNumber = "09175331828",
            Transition_no = "123123123131",
            Col1 = "کول1",
            Col2 = "کول2"
        });
    }
    t.Rows = cells;


    return File(t.Generic(), System.Net.Mime.MediaTypeNames.Application.Octet, "tesxt.pdf");
}