﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html;
using System.Web;
using static iTextSharp.text.Font;

namespace HTMLToPDF
{

    /// <summary>
    /// strLogo عنوان لوگو  ;
    /// titleTure عنوان تور  ;
    /// tourNo شماره تور;
    /// transactionNo شماره تراکنش  ;
    ///nameUser به نام ;
    ///phoneNumber شماره تلفن   ;
    ///toureType نوع تور  ;
    ///fellows همراهان  ;
    ///stayType نوع مکان  ;  
    ///carType نوع وسیله نقلیه  ;
    ///message پیام انتهای بلیت  ;
    /// </summary>
    public class PDFTicket
    {
        private string strLogo;
        private string titleTure;
        private string tourNo;
        private string transactionNo;
        private string nameUser;
        private string phoneNumber;
        private string toureType;
        private string fellows;
        private string sub1;
        private string sub2;
        private string message;
        private string fontNameF;
        private string fontNameE;
        private string imagePath;
        private string fontPath;
        
        
        public PDFTicket()
        {
            imagePath = HttpContext.Current.Server.MapPath("~/HTMLToPDF/Images/imagBackground.png");
            fontNameF = "Yekan";
            fontNameE = "Calibri";
            Subject1 = "نوع اقامت";
            Subject2 = "نوع وسیله نقلیه";
        }/// <summary>
        /// ادرس Url
        /// </summary>
        public string Brand { get; set; }
        /// <summary>
        /// نوع اقامت
        /// </summary>
        public string Subject1 { get; set; }
        /// <summary>
        /// نوع وسیله نقلیه
        /// </summary>
        public string Subject2 { get; set; }
        /// <summary>
        /// virtual path  "~/Utilities/HTMLToPDF/"
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// عنوان سفر است و بیش از 26 حرف نمی تواند باشد
        /// </summary>
        public string TitleTour
        {
            get
            {
                return titleTure;
            }
            set
            {
            
                    titleTure = value;
               
            }
        }
        /// <summary>
        /// لوگو بلیت است و بیش از 22 حرف نمی تواند باشد
        /// </summary>
        public string STRLogo
        {
            get
            {
                return strLogo;
            }
            set
            {
            
                    strLogo = value;
             
            }
        }
        /// <summary>
        /// شماره تور و حداکثر 20 کاراکتر
        /// </summary>
        public string TourNo
        {
            get
            {
                return tourNo;
            }
            set
            {
            
                    tourNo = value;
             
            }
        }
        /// <summary>
        /// به نام و حداکثر  50 کاراکتر
        /// </summary>
        public string NameUser
        {
            get
            {
                return nameUser;
            }
            set
            {
                if (value.Length <= 50)
                {
                    nameUser = value;
                }
                else
                    throw new Exception("the max length of the phrase is 50");
            }
        }
        /// <summary>
        /// شماره تراکنش حداکثر 50 کارا کتر
        /// </summary>
        public string TransactionNo
        {
            get
            {
                return transactionNo;
            }
            set
            {
                if (value.Length <= 50)
                {
                    transactionNo = value;
                }
            }
        }
        /// <summary>
        /// تاریخ شروع تور 
        /// </summary>
        public string DateStart { get; set; }
        /// <summary>
        /// زمان شروع
        /// </summary>
        public string TimeStart { get; set; }

        /// <summary>
        /// تاریخ اتمام تور 
        /// </summary>
        public string DateBack { get; set; }
        /// <summary>
        /// تعداد بلیط
        /// </summary>
        public string NumberTicket { get; set; }
        /// <summary>
        /// شماره تلفن و حداکثر 11 رقم است
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
             
                    phoneNumber = value;
              
            }
        }
        /// <summary>
        /// نوع تور و حداکثر 50 کاراکتر است
        /// </summary>
        public string ToureType
        {
            get
            {
                return toureType;
            }
            set
            {
                 toureType = value;
         
            }
        }
        /// <summary>
        /// همراهان و حداکثر 50 کاراکتر است
        /// </summary>
        public string Fellows
        {
            get
            {
                return fellows;
            }
            set
            {
              
                    fellows = value;
              
            }
        }
        /// <summary>
        /// نوع اقامت و حداکثر 50 کاراکتر است
        /// </summary>
        public string Sub1
        {
            get
            {
                return sub1;
            }
            set
            {
              
                    sub1 = value;
              
            }
        }
        /// <summary>
        ///نوع وسیله نقلیه و حد اکثر 50 کاراکتر است
        /// </summary>
        public string Sub2
        {
            get
            {
                return sub2;
            }
            set
            {
           
                    sub2 = value;
               
            }
        }
        /// <summary>
        /// پیام فوتر)  حد اکثر 50 کاراکتر است)
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
               
                    message = value;
              
            }
        }
        /// <summary>
        /// مسیر فنت انگلیسی
        /// </summary>
        public string FontPathEnglish { get; set; }
        /// <summary>
        /// مسیر فونت فارسی
        /// </summary>
        public string FontPathFarcy { get; set; }
        public byte[] Generate()
        {
            if(!string.IsNullOrEmpty(Path))
            {
                imagePath = HttpContext.Current.Server.MapPath(Path + "/Images/imagBackground.png");
                fontPath = Path;
            }
         
           


            try
            {

          
            MemoryStream ofileStream = new MemoryStream();
                
            Font docFontF = GetFont(fontNameF);

            var pageSize = new iTextSharp.text.Rectangle(430f, 500f);
            var doc = new Document(pageSize);
            //    doc.SetMargins(50f, 50f, 15f, 2f);
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, ofileStream);
            doc.Open();
            
          

            PdfContentByte canvas = pdfWriter.DirectContentUnder;
            Image image = Image.GetInstance(imagePath);
            image.SetAbsolutePosition(0, 0);
            image.ScaleAbsolute(430f,500f);
           
            PdfGState graphicsState = new PdfGState ();
            canvas.SetGState(graphicsState);
            canvas.AddImage(image);
              
                PdfPTable toptable = new PdfPTable(1)
            {
                WidthPercentage = 100,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                ExtendLastRow = false,
                SpacingBefore = 30
            };
                toptable.AddCell(new PdfPCell(new Phrase("", docFontF))
                {
                    RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0,
                    PaddingRight = 35f


                });
                docFontF.Size = 12f;

            docFontF.SetStyle(" font-weight:none");

                toptable.AddCell(new PdfPCell(new Phrase("شماره تور", docFontF))
                {
                    RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0,
                    PaddingRight = 35f,
                    PaddingTop = 3
                

            });

            //docFontF.Size = 4f;


            // FontFactory.Register(FontPathFarcy);
       
            Font font1 =GetFont(fontNameF);
            font1.Size = 9f;
                toptable.AddCell(new PdfPCell(new Phrase(TourNo, font1))
                {

                    RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0,
                    PaddingRight = 35f,
                    PaddingTop = -1

            });




            int[] topTableColumnsWidth = { 100 };
            toptable.SetWidths(topTableColumnsWidth);
            doc.Add(toptable);

            PdfPTable infoTable = new PdfPTable(1)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
                ExtendLastRow = false,
                SpacingBefore = 0
            };

           

            Font font2 = GetFont(fontNameF);

            font2.Size = 18f;
            font2.SetStyle(" font-weight:bold");
            infoTable.AddCell(new PdfPCell(new Phrase(STRLogo, font2))
            {
                //  RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                HorizontalAlignment = Element.ALIGN_CENTER,
                Border = 0,
                PaddingTop = -9,

                PaddingRight = 30f
            });
            infoTable.AddCell(new PdfPCell(new Phrase("شماره تراکنش", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = -10
            });
            infoTable.AddCell(new PdfPCell(new Phrase(TransactionNo, docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop=-2

            });

            int[] infoTableColumnWidth = { 100 };
            infoTable.SetWidths(infoTableColumnWidth);
            doc.Add(infoTable);
            PdfPTable tableMidel = new PdfPTable(1)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
                SpacingBefore = 24f
            };

         
            Font font3 =GetFont(fontNameF);
            font3.SetColor(51, 102, 34);
            font3.Size = 18f;

            font3.SetStyle("font-weight:bold");
                tableMidel.AddCell(new PdfPCell(new Phrase(TitleTour, font3))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = 0,
                    RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                    PaddingRight = 30f,
                    PaddingTop = 18

            });
            doc.Add(tableMidel);
            PdfPTable tableBody = new PdfPTable(1)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
                SpacingBefore = 40f
            };
           
          
            Font fontValue =GetFont(fontNameF);
            fontValue.Size = 12f;


            tableBody.AddCell(new PdfPCell(new Phrase("به نام", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
               

            });
            tableBody.AddCell(new PdfPCell(new Phrase(NameUser, fontValue))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 8

            });
            doc.Add(tableBody);
            PdfPTable tableBody2 = new PdfPTable(2)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
                SpacingBefore = 12f
            };
         


            tableBody2.AddCell(new PdfPCell(new Phrase("تاریخ رفت", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                 PaddingTop = 3

            });
            tableBody2.AddCell(new PdfPCell(new Phrase("ساعت", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 30f,
                 PaddingTop = 3

            });
            tableBody2.AddCell(new PdfPCell(new Phrase(DateStart, fontValue))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 3


            });
            tableBody2.AddCell(new PdfPCell(new Phrase(TimeStart, fontValue))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 3


            });

            tableBody2.AddCell(new PdfPCell(new Phrase("تاریخ برگشت", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 11


            });
            tableBody2.AddCell(new PdfPCell(new Phrase("تعداد بلیت خریداری شده", docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 30f,
                PaddingTop = 11


            });
            tableBody2.AddCell(new PdfPCell(new Phrase(DateBack, fontValue))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f


            });
            tableBody2.AddCell(new PdfPCell(new Phrase(NumberTicket, fontValue))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 38f


            });
            tableBody2.SetWidths(new int[] { 50, 50 });
            doc.Add(tableBody2);
            PdfPTable tableBody3 = new PdfPTable(1)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
                SpacingBefore = 10f
            };

            tableBody3.AddCell(new PdfPCell(new Phrase(" شماره تماس اژانس" + " : " + phoneNumber, docFontF))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 8

            });
            tableBody3.AddCell(new PdfPCell(new Phrase(" نوع تور" + " : " + ToureType, font1))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 8

            });
           
            tableBody3.AddCell(new PdfPCell(new Phrase(Subject1+ " : " + Sub1, font1))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 8

            });
            tableBody3.AddCell(new PdfPCell(new Phrase(Subject2 + " : " + Sub2, font1))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 8

            });
            doc.Add(tableBody3);
            PdfPTable tableFoter = new PdfPTable(1)
            {
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                WidthPercentage = 100,
              
            };
            Font fontE = GetFont(fontNameE);
            fontE.Size = 10f;
            fontE.SetColor(34,139,34);
            tableFoter.AddCell(new PdfPCell(new Phrase(Brand, fontE))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Border = 0,
                RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                PaddingRight = 35f,
                PaddingTop = 12

            });
                tableFoter.AddCell(new PdfPCell(new Phrase(Message, font1))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0,
                    RunDirection = PdfWriter.RUN_DIRECTION_RTL,
                    PaddingRight = 35f,
                    PaddingTop = 5
                });
               // tableFoter.SetWidths(new int[] { 84, 40 });
            doc.Add(tableFoter);
            doc.Close();
                return ofileStream.ToArray();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        
      /////////////////////////////////////////////////////////font
       
        private Font GetFont(string fontName)
        {
            string fontpathAbsolute;
            if(!string.IsNullOrEmpty(fontPath))
            fontpathAbsolute= HttpContext.Current.Server.MapPath(fontPath+"fonts/" + fontName + ".ttf");
            else
            fontpathAbsolute = HttpContext.Current.Server.MapPath(fontPath+"fonts/" + fontName + ".ttf");
            BaseFont customfont = BaseFont.CreateFont(fontpathAbsolute , BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(customfont);

            return font; 
                //FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }


    }
}
 

