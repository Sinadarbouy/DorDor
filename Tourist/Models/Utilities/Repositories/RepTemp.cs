﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tourist.Models;

namespace Repositories
{
    public class RepTemp:RepositoryBase<Temp>
    {
        RepTour repTour;
       
        public RepTemp()
        {
            repTour =new RepTour();
           
        }

        public int SetStep1(int tourId,int? quantity,int? tempId)
        {
            if (quantity.HasValue)
            {
                Temp temp=new Temp();
                if (tempId.HasValue)
                {
                    temp = Find(c=>c.Step==tempId.Value) ?? new Temp();
                    temp.Step = tempId.Value;
                }
                else
                {
                    temp.Step = 1;
                }
               
               
                temp.datetime = DateTime.Now.AddMinutes(10);
                temp.quantity = quantity;
               
                temp.isCommit = null;
                
                temp.Tour_Id = tourId;
                if (temp.ID==0)
                    Add(temp, true);
                   else
                    Update(temp, true);
            
               
                return temp.ID;
            }
            return 0;
           
        }
        public void Check(int tourId)
        {
            DateTime datetimenow = DateTime.Now;
            List<Temp> lst = Where(x => x.datetime<datetimenow && x.isCommit==null);
            List<Temp> lst2 = Where(c => c.datetime < datetimenow && c.Step == 2 && c.isCommit == false);
            
            Tour tour = repTour.FindById(tourId);
            DeleteRenge(lst);
          DeleteRenge(lst2);

            Save();

        }
        public bool  setStap2(int tempId,Payment_user payment, string uri)
        {
            Temp temp = FindById(tempId);
            Tour tour = repTour.FindById(payment.Tour_id.Value);
            bool isSuccess=false;
            if(temp!=null)
            {
                temp.datetime = DateTime.Now.AddMinutes(20);
                temp.isCommit = false;
                temp.Step = 2;
                temp.Url = uri;
                temp.pay_user_id = payment.Id;

              isSuccess=  Update(temp, true).Value;
            }else
            {
                temp = new Temp()
                {
                    datetime=DateTime.Now.AddMinutes(20),
                    isCommit=false,
                    Step=2,
                    Url = uri,
                   Tour_Id =payment.Tour_id,
                    pay_user_id=payment.Id,
                    quantity=payment.quantity
                    
                };
               isSuccess = Add(temp, true).Value;
            }
            if (!isSuccess || tour.Remainder < payment.quantity)
                return false;
            return true;
        }
        public bool Commit(Payment_user  payment,bool isError)
        {
            try
            {
            Temp temp=Find(c => c.pay_user_id == payment.Id);
            Tour tour = payment.Tour;
           if(temp != null)
            {
                temp.isCommit = true;
                Update(temp, true);
            }
           
            if (!isError)
            {
               
                    tour.Remainder -= payment.quantity;

                return true;
            }
        
            Delete(temp, true);
            return false;
            }
            catch
            {
                return false;
            }

        }
        public bool CheckQuntity(Payment_user payment)
        {
            Temp temp = Find(c => c.pay_user_id == payment.Id);
            Tour tour = payment.Tour;
            if (temp == null)
            {
                int quantity = 0;
                try
                {
                    List<Temp> temps = Where(c => c.Tour_Id == payment.Tour_id && c.datetime > DateTime.Now && c.Step == 2);
                    quantity = temps.Sum(c => c.quantity).Value;

                }
                catch
                {
                    quantity = 0;
                }
                if ((tour.Remainder - quantity) < payment.quantity)
                    return false;
                
            }
           
            return true;
        }
        public int Quantity(int tourId)
        {
            int? quantity = 0;
           List<Temp> temps = Where(c => c.Tour_Id == tourId && c.isCommit!=true);
            if (temps != null)
                quantity = temps.Sum(c=>c.quantity);
            if (quantity == null)
                quantity = 0;
            return quantity.Value;
        }
        
    }
}