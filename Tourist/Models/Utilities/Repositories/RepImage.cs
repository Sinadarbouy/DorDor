﻿using Compress;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tourist.Models;

namespace Repositories
{
 
    public struct SmalSize
    {
        public int height;
        public int width;
        private string path;
        private string name;

        public string Name
        {
            set
            {
               
                    name = value + ".jpg";
                
            }
            get
            {
                return name;
            }
        }
    
        public string Path
        {
            set
            {
                 path = value + @"Smal/";
             
            }
            get
            {
                return path;
            }
        }
        public string FullName
        {
            get
            {
                return Path + Name;
            }
        }

    }
    public struct LargSize
    {
        public int heigth;
        public int Width;
        private string path;
        private string name;
        public string Name
        {
            set
            {
             
                    name = value + ".jpg";
                
            }
            get
            {
                return name;
            }
        }

        public string Path
        {
            set
            {
               
               path = value + "Larg/";
              
            }
            get
            {
                return path;
            }
        }
        public string FullName
        {
            get
            {
                return Path + Name;
            }
        }
    }
    
    public class RepImage : RepositoryBase<image>
    {
        Controller controller;
        RepTour repTour;
        SmalSize smalSize = new SmalSize();
        LargSize largSize = new LargSize();

        public RepImage()
        {

        }
        public RepImage(Controller controller):this(controller,new DbTourist())
        {
                
        }
        public RepImage(Controller controller,DbTourist dbTourist):base(dbTourist)
        {
            this.controller = controller;

            smalSize.height = 176;
            smalSize.width = 280;

            largSize.Width = 652;
            largSize.heigth = 400;

            repTour= new RepTour(dbTourist);

        }
        public void FileSave(string path,ICollection<HttpPostedFileBase>files,Tour tour)
        {
            smalSize.Path = path;
            largSize.Path = path;
         

            Stream image;
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string name = Guid.NewGuid().ToString();
                    smalSize.Name = name;
                    largSize.Name = name;
                    image = file.InputStream;
                    Utilty.ResizeImage(image, largSize.Width, largSize.heigth,controller.Server.MapPath(largSize.FullName));
                    Utilty.ResizeImage(image, smalSize.width, smalSize.height, controller.Server.MapPath(smalSize.FullName));
                    if (System.IO.File.Exists(controller.Server.MapPath(smalSize.FullName)) && System.IO.File.Exists(controller.Server.MapPath(largSize.FullName)))
                    {
                       tour.images.Add(new image() { Path = largSize.FullName.Substring(1,largSize.FullName.Length-1)});
                       tour.images.Add(new image() { Path = smalSize.FullName.Substring(1,smalSize.FullName.Length-1)});
                    }

                }
            }
        }
        //public void Transfer()
        //{
        //    string path = "~/Content/Images/";

        //    smalSize.Path = path;
        //    largSize.Path = path;

        //    List<image>images  = GetAll().ToList();
        //    RepImage ima = new RepImage();
            
        //    foreach (image i in images)
        //    {
        //        if (i.image1 != null)
        //        {
        //            Stream stream = new MemoryStream(i.image1);


        //            string name = Guid.NewGuid().ToString();

        //            smalSize.Name = name;
        //            largSize.Name = name;
        //            try
        //            {
        //                Utilty.ResizeImage(stream, largSize.Width, largSize.heigth, controller.Server.MapPath(largSize.FullName));
        //                Utilty.ResizeImage(stream, smalSize.width, smalSize.height, controller.Server.MapPath(smalSize.FullName));
        //            }
        //            catch(Exception ex)
        //            {

        //            }

                 
                  
        //                i.Path = largSize.FullName.Substring(1, largSize.FullName.Length - 1);
        //                Update(i,true);


        //               ima.Add(new image() {Tour_id=i.Tour_id, Path = smalSize.FullName.Substring(1, smalSize.FullName.Length - 1), image1 = i.image1 },true);
                    


        //        }

        //    }
           
           
            
        //}
       
        public override bool? Delete(int Id, bool isSave = false)
        {
           image simage= FindById(Id);
            image Limage = new image();


            if (simage != null)
            {
                int lastindex = simage.Path.LastIndexOf('/');
            
                string subpath = simage.Path.Substring(lastindex);
               
               Limage = Find(c => c.Path.Contains("/Larg" + subpath));
                if (base.Delete(simage, isSave).Value)
                {
                    try
                    {
                        if (base.Delete(Limage, isSave).Value)
                        {

                            File.Delete(controller.Server.MapPath("~" + simage.Path));
                            File.Delete(controller.Server.MapPath("~" + Limage.Path));
                            return true;
                        }
                       
                    }
                    catch
                    {
                        return false;
                    }

                }
            }
               
         
            return false ;
        }

        public bool? Delete(List<image> images)
        {

            foreach (var item in images)
            {
                try
                {
                    Delete(item, false);
                    File.Delete(controller.Server.MapPath("~" + item.Path));
                }
                catch (Exception)
                {

                }
            }

            return true;
        }
    }
}

