﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

using System.Xml.Serialization;
using System.IO;
using Tourist.Models;

namespace Repositories
{
    public abstract class RepositoryBase<T> where T : class
    {
        DbTourist context;
        private IDbSet<T> dbSet;
        public RepositoryBase() : this(new DbTourist())
        {


        }
        public RepositoryBase(DbTourist dbTourist)
        {
            this.context = new DbTourist();

            this.dbSet = context.Set<T>();

        }


        public virtual bool? Add(T entity,bool isSave=false)
        {
            try
            {
                context.Entry(entity).State = EntityState.Added;
            if (isSave)
                return context.SaveChanges()>0;
            return null;
            }
            catch(Exception ex)
            {
                string exstr = ex.Message;
                return false;
            }
        }

        public virtual bool? Update(T entiry,bool isSave=false)
        {

            try
            {
              dbSet.Attach(entiry);
              context.Entry(entiry).State = EntityState.Modified;
            if (isSave)
                return Convert.ToBoolean(context.SaveChanges());

            return null;
             }
            catch(Exception ex)
            {
                string error = ex.Message;
                return false;
            }
           

        }

    public virtual bool?  Delete(int Id,bool isSave=false)
        {
            try
            {
                dbSet.Remove(FindById(Id));
            if (isSave)
                return Convert.ToBoolean(context.SaveChanges());

            return null;
             }
            catch
            {
                return false;
            }
           
        }

    public virtual bool? Delete(T entity,bool isSave=false)
        {
            try
            {
                dbSet.Remove(entity);
                if (isSave)
                  return Convert.ToBoolean(context.SaveChanges());

                return null;
            }
            catch
            {
                return false;
            }
           
        }

        public virtual T FindById(int Id)
        {
            try
            {
                return dbSet.Find(Id);
            }catch
            {
                return null;
            }
           
        }

        public virtual T Find(Expression<Func<T, bool>> Where)
        {
            try
            {
                return dbSet.Where(Where).FirstOrDefault();
            }
            catch
            {
                return null;
            }
            
        }
        public virtual List<T> Where(Expression<Func<T, bool>> Where)
        {
            try
            {
                return dbSet.Where(Where).ToList();
            }
            catch(Exception ex)
            {
                string ss = ex.Message;
                return null;
            }
          
        }


        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }
        public virtual bool? DeleteRenge(IEnumerable<T> entities,bool isSave=false)
        {
            try
            {
                if (entities != null)
                {
                    foreach (T item in entities)
                    {
                        dbSet.Attach(item);
                        context.Entry(item).State = EntityState.Deleted;
                    }
                    if (isSave)
                        return Convert.ToBoolean(context.SaveChanges());
                }else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
         
           
            return null;
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> Where)
        {
            try
            {
                return dbSet.Where(Where).ToList();
            }
            catch
            {
                return null;
            }
          
        }
        public virtual bool Exist(int id)
        {
            try
            {
                if (FindById(id) != null)
                    return true;
                return false;

            }
            catch
            {
                return false;
            }
        }
        public int Save()
        {
            try
            {
                return context.SaveChanges();
            }
            catch
            {
                return 0;
            }
           
        }
    }
}