﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tourist.Models;

namespace Tourist.Utilities.Discount
{
    public class ManageDiscount
    {
        DbTourist db;
        bool isMain = true;
        public ManageDiscount(DbTourist data, bool? isMain = true)
        {
            if (isMain != null)
                this.isMain = isMain.Value;

            if (data != null)
            {
                db = data;
            }
            else
            {
                db = new DbTourist();
            }
        }
        public string CheckDiscount(string discountCode, int? tourid)
        {
            try
            {
                Tourist.Models.Discount discount = null;
                discount = db.Discounts.First(c => c.CodeDiscount == discountCode);

                if (IsValidDiscount(discount, tourid))
                {
                    int? currentQuantity = 0;
                    int? discountRevival = 0;
                    int quan = discount.Remainder.Value;
                    if (discount.TempDiscounts.Any())
                    {


                        List<TempDiscount> tempDiscounts = discount.TempDiscounts.ToList();
                        if (tempDiscounts != null)
                        {
                            currentQuantity = tempDiscounts.Where(c => c.iscommit == false && c.time_start.Value.AddMinutes(20) > DateTime.Now).Sum(c => c.quantity);
                            discountRevival = tempDiscounts.Where(c => c.iscommit == null && c.time_start.Value.AddMinutes(10) > DateTime.Now).Sum(c => c.quantity);

                        }

                        currentQuantity = currentQuantity.HasValue ? currentQuantity.Value : 0;
                        discountRevival = discountRevival.HasValue ? discountRevival.Value : 0;

                        currentQuantity += discountRevival;
                        quan -= currentQuantity.Value;

                    }


                    return JsonConvert.SerializeObject(new { success = "true", IsPercent = discount.IsPercent, discountprice = discount.Discountprice, quantity = quan, CurrentDiscount = currentQuantity });
                }

            }
            catch
            {

            }

            return JsonConvert.SerializeObject(new { success = "false" });
        }
        public int CheckDiscount(int profileID, bool isMain)
        {
            try
            {
                ProfileDiscount profileDiscount = db.ProfileDiscounts.Where(x => x.Profile_ID == profileID).FirstOrDefault();//پیدا کردن پروفایل
                if (isMain)//تخفیف برای صفحه اصلی
                {
                    if (profileDiscount.Exp_main != null)//تاریخ انقضا دارد تخفیف یا نه
                    {
                        if (profileDiscount.Exp_main > DateTime.Now)//از زمان تخفیف نگذشته یاشد
                        {
                            return 0;//اگر گذشته باشد تخفیف اعمال نشود
                        }
                    }
                    else
                    {
                        return profileDiscount.Discount_main.Value;//تخفیف اعمال شود
                    }
                }//تخفیف برای صفحه آژانس
                else
                {
                    if (profileDiscount.Exp_Agency != null)//تاریخ انقضا دارد تخفیف یا نه
                    {
                        if (profileDiscount.Exp_Agency > DateTime.Now)//از زمان تخفیف نگذشته یاشد
                        {
                            return 0;//اگر گذشته باشد تخفیف اعمال نشود
                        }
                    }
                    else
                    {
                        return profileDiscount.Discount_Agency.Value; //تخفیف اعمال شود
                    }
                }
                return 0;//تخفیف اعمال نشود
            }
            catch
            {
                return 0;
            }

        }
        private bool IsValidDiscount(Tourist.Models.Discount discount, int? id)
        {
            try
            {
                if (!id.HasValue)
                    return false;


                Tour tour = db.Tours.First(c => c.Id == id);
                if (tour == null)
                    return false;

                return IsValidDiscount(discount, tour);
            }
            catch
            {
                return false;
            }

        }
        private bool IsValidDiscount(Tourist.Models.Discount discount, Tour tour)
        {
            try
            {



                if (discount != null && discount.ProfileID != null && discount.Start != null && discount.Exp != null && (discount.Exp > DateTime.Now && discount.Start < DateTime.Now))
                {



                    if (discount.ProfileID != tour.Profile_id)
                        return false;

                    if (!discount.IsProfile.HasValue || !discount.IsProfile.Value)
                    {
                        if (discount.TourID.HasValue && tour.Id == discount.TourID)
                            return true;
                        return false;
                    }
                    else
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }

        }
        public float CalcDiscount(float ototal, string codeDiscount, int quantity, Tour tour)
        {
            Tourist.Models.Discount discount = null;

            try
            {
                if (codeDiscount == "")
                    return CalcDiscount(ototal, quantity, tour);
                else if (codeDiscount == null)
                    return ototal;
                float CostProfileDiscount = 0;
                if (codeDiscount != "-")
                    CostProfileDiscount = CheckDiscount(tour.Profile_id.Value, isMain) * ototal / 100;

                if ((CostProfileDiscount / quantity) > 30000)
                    CostProfileDiscount = 30000 * quantity;

                if (codeDiscount != "-")
                {

                    discount = db.Discounts.Where(c => c.CodeDiscount == codeDiscount).First();
                } else
                {
                    return ototal - CostProfileDiscount;
                }

                if (IsValidDiscount(discount, tour))
                {
                    if (discount != null)
                    {

                        if (discount.Remainder >= quantity)
                        {

                            if (discount.IsPercent.Value)
                            {
                                ototal -= (ototal * discount.Discountprice.Value / 100);
                            }
                            else
                            {
                                ototal -= discount.Discountprice.Value * quantity;
                            }



                        }

                    }
                }

                return (ototal - CostProfileDiscount);
            }
            catch
            {

                return ototal;
            }
        }
        public float CalcDiscount(float ototal, int quantity, Tour tour)
        {


            try
            {


                float CostProfileDiscount = 0;

                CostProfileDiscount = CheckDiscount(tour.Profile_id.Value, isMain) * ototal / 100;

                if ((CostProfileDiscount / quantity) > 30000)
                    CostProfileDiscount = 30000 * quantity;


                return (ototal - CostProfileDiscount);
            }
            catch
            {

                return ototal;
            }
        }
        public void SetTempDiscount(Payment_user firestUser, int quantity)
        {
            try
            {
                if (firestUser.Discount_ID != null && firestUser.Discount_ID > 0)
                {

                    Tourist.Models.Discount discount = db.Discounts.First(c => c.ID == firestUser.Discount_ID);

                    if (discount != null)
                    {
                        IEnumerable<TempDiscount> tempDiscount = null;
                        if (discount.TempDiscounts != null)
                            tempDiscount = discount.TempDiscounts;

                        int? sumDisCount = 0;
                        if (tempDiscount != null && tempDiscount.Any())
                        {

                            sumDisCount = tempDiscount.Where(c => c.time_start.Value.AddMinutes(20) > DateTime.Now && c.iscommit == false).Sum(c => c.quantity);
                        }


                        if (sumDisCount != null && sumDisCount > 0)
                        {
                            sumDisCount = discount.Remainder - sumDisCount;
                        }
                        else
                        {
                            sumDisCount = discount.Remainder;
                        }


                        if (sumDisCount >= firestUser.quantity)
                        {
                            TempDiscount temp = new TempDiscount()
                            {
                                quantity = quantity,
                                discount_id = discount.ID,
                                payUser_id = firestUser.Id,
                                time_start = DateTime.Now,
                                iscommit = false
                            };
                            db.TempDiscounts.Add(temp);
                        }
                        else
                        {
                            throw new Exception("error Discount");
                        }
                    }

                }



            }
            catch
            {
                throw new Exception("Discont Error");
            }
        }
        public bool IsQuantity(Payment_user firestUser)
        {
            try
            {
                if (firestUser.Discount_ID != null && firestUser.Discount_ID > 0)
                {

                    Tourist.Models.Discount discount = db.Discounts.First(c => c.ID == firestUser.Discount_ID);

                    if (discount != null)
                    {
                        IEnumerable<TempDiscount> tempDiscount = null;
                        if (discount.TempDiscounts != null)
                            tempDiscount = discount.TempDiscounts;

                        int? sumDisCount = 0;
                        if (tempDiscount != null && tempDiscount.Any())
                        {

                            sumDisCount = tempDiscount.Where(c => c.time_start.Value.AddMinutes(20) > DateTime.Now && c.iscommit == false).Sum(c => c.quantity);
                        }

                    

                        if ( discount.Remainder>= firestUser.quantity)
                        {
                            return false;

                        }
                       
                    }

                }
                else
                {
                    return false;
                }

                return true;
            }
            catch
            {
                return true;
            }
          }
           

        public void DiscountCommit(Payment_user firestPay, bool isError)
        {

            if (firestPay.Discount_ID.HasValue)
            {
                try
                {
                    if (firestPay.Discount_ID != 0)
                    {
                       Tourist.Models.Discount discount = db.Discounts.Where(c => c.ID == firestPay.Discount_ID).First();
                        if (discount.TempDiscounts.Any())
                        {
                            TempDiscount distemp = discount.TempDiscounts.Where(c => c.payUser_id == firestPay.Id).First();
                            distemp.iscommit = true;
                           
                        }

                        if (!isError)
                        {
                            discount.Remainder -= firestPay.quantity;
                        }
                      
                    }

                }
                catch
                {

                }
            }

        }
        public int SetTemp(string codeDiscount,int quantity,Tour tour)
        {
            try
            {
                if(!string.IsNullOrEmpty(codeDiscount))
                {
                    Tourist.Models.Discount discount = db.Discounts.First(c => c.CodeDiscount == codeDiscount);

                    if (discount != null && discount.Remainder > 0 && IsValidDiscount(discount, tour))
                    {
                        if (discount != null)
                        {


                            TempDiscount tempDiscount = new TempDiscount()
                            {
                                discount_id = discount.ID,
                                iscommit = null,
                                quantity = quantity,
                                time_start = DateTime.Now
                            };
                            db.TempDiscounts.Add(tempDiscount);
                        }

                        return discount.ID;
                    }
                }
               
               
            }
            catch(Exception ex)
            {
            
            }
            return -1;
        }
        public void cleanTemp()
        {
            try
            {
                List<TempDiscount> tmps = db.TempDiscounts.ToList();
                IEnumerable<TempDiscount> tt = tmps.Where(c => c.time_start.Value.AddMinutes(20) < DateTime.Now && c.iscommit == null);
                if (tt.Count() >0)
                    db.TempDiscounts.RemoveRange(tt);
            }
            catch
            {

            }
           
        }
    }
}