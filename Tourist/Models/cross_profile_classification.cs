//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tourist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class cross_profile_classification
    {
        public int id { get; set; }
        public Nullable<int> profile_id { get; set; }
        public Nullable<int> classification_id { get; set; }
    
        public virtual Classification Classification { get; set; }
        public virtual Profile_cafe_gallery Profile_cafe_gallery { get; set; }
    }
}
