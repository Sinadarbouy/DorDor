﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tourist.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class RequestBalance
    {
        public int ID { get; set; }
        [Range(0, 100000000000000000, ErrorMessage = "لطفا قیمت را به عدد وارد کنید")]
        [Required(ErrorMessage = " لطفا قیمت را وارد کنید")]
        public Nullable<int> Amount { get; set; }
        public Nullable<int> Available { get; set; }
        public Nullable<int> Confirmation { get; set; }
        public Nullable<int> Profile_id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Decription { get; set; }
    
        public virtual Profile_Tour Profile_Tour { get; set; }
    }
}
