﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tourist.Models
{
    public class TourPlus
    {
        public Tour tour { get; set; }

        [Required(ErrorMessage = "لطفا تاریخ را انتخاب کنید")]
        public string start_date_fa { get; set; }
        [Required(ErrorMessage = "لطفا تاریخ را انتخاب کنید")]
        public string end_date_fa { get; set; }
        [Required(ErrorMessage = "لطفا تاریخ را انتخاب کنید")]
        public string deadline_fa { get; set; }
        //[RegularExpression("(^.*\\.(jpg|JPG|gif|GIF|doc|DOC|pdf|PDF)$)", ErrorMessage = "نام معتبر نمی باشد")]
        [Required(ErrorMessage = "لطفا عکس را انتخاب کنید")]
        public List<HttpPostedFileBase> beforeimage { get; set; }
    }
}