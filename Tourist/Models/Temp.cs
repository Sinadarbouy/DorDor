//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tourist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Temp
    {
        public int ID { get; set; }
        public Nullable<int> Step { get; set; }
        public Nullable<int> quantity { get; set; }
        public string Url { get; set; }
        public Nullable<System.DateTime> datetime { get; set; }
        public Nullable<int> Tour_Id { get; set; }
        public Nullable<int> pay_user_id { get; set; }
        public Nullable<bool> isCommit { get; set; }
    
        public virtual Payment_user Payment_user { get; set; }
    }
}
