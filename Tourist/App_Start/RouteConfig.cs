﻿using System.Web.Mvc;
using System.Web.Routing;
using Tourist.Routing;

namespace IdentitySample
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Home",
            //    url: "",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
            routes.Add(new SubdomainRoute());

            routes.MapRoute(
           name: "agency",
           url: "t{id}",
           defaults: new { controller = "events", action = "AgencySingletour", Id = "id" }
       );
            routes.MapRoute(
          name: "tourip",
          url: "tour{id}",
          defaults: new { controller = "events", action = "touripSingletour", Id = "id" }
      );
            routes.MapRoute(
           name: "club",
           url: "club",
           defaults: new { controller = "Agency", action = "Single_tour", Id = "1630" }
       );
            routes.MapRoute(
       name: "Login",
       url: "Login",
       defaults: new { controller = "account", action = "Login" }
                                                                 );
            routes.MapRoute(
           name: "Baby",
           url: "baby{id}",
           defaults: new { controller = "Agency", action = "Baby", Id = "id" }
       );
            routes.MapRoute(
           name: "ordibehesht",
           url: "ordibehesht",
           defaults: new { controller = "Agency", action = "Index", name = "ordibehesht" }
       );
            routes.MapRoute(
     name: "zandiyehgasht",
     url: "zandiyehgasht",
     defaults: new { controller = "Agency", action = "Index", name = "zandiyehgasht" }
 );
            routes.MapRoute(
     name: "parchin",
     url: "parchin",
     defaults: new { controller = "Agency", action = "Index", name = "parchin" }
 );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );




            //routes.MapRoute(
            // name: "tour",
            // url: "tour-{Id}",
            // defaults: new { controller = "Agency", action = "Single_Tour", Id = "Id" }
            // );

        }
    }
}