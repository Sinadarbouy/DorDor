var ComponentsBootstrapTagsinput = function() {

    var handleDemo1 = function() {
        var elt = $('#object_tagsinput');
        
        elt.tagsinput({
          itemValue: 'value',
          itemText: 'text',
        });

        $('#object_tagsinput_add').on('click', function(){
            elt.tagsinput('add', { 
                "value": $('#object_tagsinput_value').val(), 
                "text": $('#object_tagsinput_city').val(), 
                "continent": $('#object_tagsinput_continent').val()    
            });
        });

        elt.tagsinput('add', { "value": 1 , "text": "الکترونیک"   , "continent": "1"    });
        elt.tagsinput('add', { "value": 4 , "text": "درمانی"  , "continent": "2"   });
        elt.tagsinput('add', { "value": 7 , "text": "خدماتی"      , "continent": "3" });
        
    }

    var handleDemo2 = function() {

        var elt = $('#state_tagsinput');

        elt.tagsinput({
            tagClass: function(item) {
                switch (item.continent) {
                    case '1':
                        return 'label label-default';
					case '2':
                        return 'label label-primary';
                    case '3':
                        return 'label label-success';
					case '4':
                        return 'label label-warning';
					case '5':
                        return 'label label-danger label-important';
                    
                }
            },
            itemValue: 'value',
            itemText: 'text'
        });

         $('#state_tagsinput_add').on('click', function(){
            if($('#state_tagsinput_value').val()){
			elt.tagsinput('add', { 
			
                "value": $('#state_tagsinput_value').val(), 
                "text": $('#state_tagsinput_value').val(), 
                "continent": $('#state_tagsinput_continent').val()    
			    });
			}
        });
        
        elt.tagsinput('add', {
            "value": 1,
            "text": "الکترونیک",
            "continent": "1"
        });
        elt.tagsinput('add', {
            "value": 4,
            "text": "درمانی",
            "continent": "2"
        });
        elt.tagsinput('add', {
            "value": 7,
            "text": "خدماتی",
            "continent": "3"
        });
        
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo1();
            handleDemo2();
        }
    };

}();

jQuery(document).ready(function() {
    ComponentsBootstrapTagsinput.init();
});