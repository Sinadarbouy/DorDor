//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Admin_Tourist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Payment_user
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string ssn { get; set; }
        public string email { get; set; }
        public Nullable<int> pay { get; set; }
        public Nullable<int> Tour_id { get; set; }
        public Nullable<bool> commit_pay { get; set; }
        public string mobie { get; set; }
        public string Description { get; set; }
        public Nullable<bool> termpolicy { get; set; }
        public string refId { get; set; }
        public Nullable<int> quantity { get; set; }
        public Nullable<int> transferPrice { get; set; }
        public Nullable<int> placeprice { get; set; }
    
        public virtual Tour Tour { get; set; }
    }
}
