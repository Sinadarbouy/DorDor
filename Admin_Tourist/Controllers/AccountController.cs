﻿using System.Globalization;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Admin_Tourist.Models;
using System.Web.Services;

namespace IdentitySample.Controllers
{
    public class AccountController : Controller
    {
       
        public ApplicationUserManager usermngr
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }
        public ActionResult Login() {
            return View();
        }
        public ActionResult LoginConfirm(LoginViewModel model, string email, string password)
        {
            
            model.Email = email;
            model.Password = password;
            if (usermngr.FindByEmail(model.Email) == null)
            {
                TempData["msg"] = "کاربر با این نام کاربری وجود ندارد";
                return RedirectToAction("index", "home");
            }
            else
            {
                var usersigninmngr = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                var status = usersigninmngr.PasswordSignIn(model.Email, model.Password,
                    model.RememberMe, false);
                
                //User.Identity.Name
                if (status == SignInStatus.Success && User.IsInRole("Admin") )
                {
                    TempData["msg"] = "شما با موفقیت به سایت وارد شده اید";
                }
                else
                {
                    TempData["msg"] = "نام کاربری یا رمز شما صحیح نمی باشد";
                }
            }
            return RedirectToAction("index", "home");
        }
    }
    
}